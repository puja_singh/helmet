package com.bounce;

import com.bounce.config.PersistenceContext;
import com.bounce.dtos.paytm.LockAmountResponse;
import com.bounce.entity.Customer;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.CustomerRepository;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import javax.inject.Inject;


@Slf4j
@Getter
@DataJpaTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = {Application.class, PersistenceContext.class})
public class PaytmExternalApiTest {

    @Inject private CustomerRepository customerRepository;
    @Inject private PaytmRepository paytmRepository;

    private static final long testUser = 57209;
    private static final long bookingId = 51743L;
    private static final double amount = 1;

//    @Test
//    public void test_PaytmLockAmount() {
//        String paytmToken = customerRepository.findById(testUser).get().getPaytmToken();
//        LockAmountResponse lockAmountResponse = paytmRepository.lockAmount(bookingId, paytmToken, amount);
//        log.info("lockamount response {}", lockAmountResponse.getStatus());
//        Assert.assertEquals("Lock amount response status is incorrect", "TXN_SUCCESS",
//                lockAmountResponse.getStatus());
//    }
//
//    @Test
//    public void test_PaytmReleaseAmount() {
//        String paytmToken = customerRepository.findById(testUser).get().getPaytmToken();
//        paytmRepository.releaseAmount(paytmToken, "20190701111212800110168397176523887");
//    }

//    @Test
//    public void test_PaytmWithdrawAmount() throws Exception {
//        Customer customer = customerRepository.findById(testUser).get();
//        paytmRepository.withdrawForBooking(customer, bookingId, 1.1);
//    }

//    @Test
//    public void test_PaytmCaptureAmount() throws Exception {
//        Customer customer = customerRepository.findById(testUser).get();
//        paytmRepository.captureForBooking(customer, bookingId, amount, "20190701111212800110168397276482084");
//    }

    @Test
    public void test_PaytmUserBalance()  {
        Customer customer = customerRepository.findById(testUser).get();
        paytmRepository.getBalance(customer.getPaytmToken(), customer.getPaytmTokenExpiry(), customer.getId());
    }
}
