package com.bounce;

import com.bounce.cash.BounceCashApi;
import com.bounce.config.PersistenceContext;
import com.bounce.dtos.cash.BounceCashGetBalanceResponse;
import com.bounce.dtos.cash.BounceCashGetRemittableAmount;
import com.bounce.dtos.juspay.WebhookRequestDto;
import com.bounce.entity.WalletTopupOffers;
import com.bounce.repository.WalletTopupOffersRepository;
import com.bounce.utils.BillingUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

@Slf4j
@Getter
@DataJpaTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = {Application.class, PersistenceContext.class})
public class BounceCashApiTest {

    @Inject private BounceCashApi bounceCashApi;
    @Inject private WalletTopupOffersRepository walletTopupOffersRepository;

    private static final String mobileNo = "9519693052";
    private static final String CreditOrDebit = "D";


    @Value("${wallet.token:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjk4NTY4LCJpc3MiOiJodHRwOi8vcGhwdGVzdGluZy5tZXRyb2Jpa2VzLmluL2FwaS92ZXJpZnlPVFAiLCJpYXQiOjE1NDI0MDM1MjksImV4cCI6MjE0MjQwMzUyOSwibmJmIjoxNTQyNDAzNTI5LCJqdGkiOiJXd1pPYWo2UHczVWFISGRwIn0.b9ug7l0oyQYV00URGYNLI_DQEKzuyEWe1IQ-g1hgwDo}")
    private String walletToken;

    @Test
    public void test_getBalance() {
        BounceCashGetBalanceResponse response = this.bounceCashApi.getBalance(this.getWalletToken());
        Assert.assertTrue("Balance should be greater than 0", response.getResult().getData()
                .getTotalBalance() >= 0);
    }

    @Test
    public void test_getRemmitableAmount() throws Exception {
        BounceCashGetRemittableAmount response = this.bounceCashApi.getRemittableAmount(mobileNo, CreditOrDebit);
        log.info("response is: {}", response.getResult().getData().getRemittableAmount());
        Assert.assertEquals("Remittable amount", 0, (double) response.getResult().getData().getRemittableAmount(), 0.0);
    }

    @Test
    public void billing_divide(){
        log.info("value of divide and round {}",BillingUtil.divide(159998,100));
    }

    @Test
    public void test(){
        WebhookRequestDto webhookRequestDto = new WebhookRequestDto();
        webhookRequestDto.setEventName("tetsing");
        log.info("object is {}", webhookRequestDto.toString());
    }

    @Test
    public void testing(){
        WalletTopupOffers[] walletTopupOffers = walletTopupOffersRepository.findAllByActiveAndWalletValueLessThanEqualOrderByIdAsc(true, 500);
        log.info("length:{}",walletTopupOffers.length);
        log.info("first: {}", walletTopupOffers[0]);


        log.info("wallet topup offers "+walletTopupOffersRepository.findAllByActiveAndWalletValueLessThanEqualOrderByIdAsc(true, 500));
    }
}
