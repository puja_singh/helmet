package com.bounce;

import com.bounce.config.PersistenceContext;
import com.bounce.dtos.UserDeviceInfo;
import com.bounce.dtos.razorpaywebhook.RazorpayCaptureResponse;
import com.bounce.entity.User;
import com.bounce.entity.WalletTopupOffers;
import com.bounce.mbpython.MbPythonApi;
import com.bounce.repository.UserRepository;
import com.bounce.repository.WalletTopupOffersRepository;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import javax.inject.Inject;

@Slf4j
@Getter
@DataJpaTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = {Application.class, PersistenceContext.class})
public class MbPythonApiTest {

    @Inject MbPythonApi mbPythonApi;
    @Inject UserRepository userRepository;
    @Inject WalletTopupOffersRepository walletTopupOffersRepository;
    @Inject UserDeviceService userDeviceService;

    private static final long testUser = 57209;
    private static final String paymentId = "pay_CwhCHFq44vSuMU";

    @Test
    public void clearDuesCaptureRazorpayTest(){
        User user = this.userRepository.findById(testUser).orElseThrow(() -> new IllegalArgumentException("Invalid user id passed"));
        RazorpayCaptureResponse razorpayCaptureResponse = mbPythonApi.clearDues(user.getToken(),34.5,paymentId);
        log.info("status from razorpay helmet capture is : {} and data is: {}"
                ,razorpayCaptureResponse.getStatus(),razorpayCaptureResponse.getData());
    }

    @Test
    public void walletTest(){
        int amountInr = 300;
        WalletTopupOffers walletTopupOffers  = this.walletTopupOffersRepository.findFirstByPriceAndActiveOrderByCreatedAtDesc(amountInr, true);
        if(!StringUtils.isEmpty(walletTopupOffers)){
            log.info("wallet value is {} and id : {}",walletTopupOffers.getWalletValue(), walletTopupOffers.getId());
        }
    }

}
