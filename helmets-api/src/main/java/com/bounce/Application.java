package com.bounce;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;

@Slf4j
@SpringBootApplication
@ComponentScan(basePackages = "com.bounce")
public class Application {


	public static void main(String[] args) {
		log.debug("Tuborg is up!");
		String bounceUtilEnv = System.getenv("BOUNCE_UTIL_ENV");
		if(!StringUtils.isEmpty(bounceUtilEnv)){
			log.info("found BOUNCE_UTIL_ENV:: bounce-utils will work fine");
			System.setProperty("env", bounceUtilEnv);
		}else{
			log.info("could not find BOUNCE_UTIL_ENV:: bounce-utils won't work properly");
		}

	    SpringApplication.run(Application.class, args);
	}

}
