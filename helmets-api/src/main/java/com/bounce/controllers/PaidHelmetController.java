package com.bounce.controllers;

import com.bounce.PaidHelmetService;
import com.bounce.dtos.request.PaidHelmetCaptureRazorpayDto;
import com.bounce.dtos.request.PaidHelmetTrackingDto;
import com.bounce.dtos.response.legacy.*;
import com.bounce.entity.User;
import com.bounce.exceptions.CustomException;
import com.bounce.filters.AuthFilter;
import com.bounce.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/app/")
public class PaidHelmetController {

    @Inject private UserRepository userRepository;
    @Inject private PaidHelmetService paidHelmetService;

    /***
     * helmet landing page
     * @param token user token
     * @return
     */
    @RequestMapping(path = "/helmet_landing_page", method = RequestMethod.GET)
    @AuthFilter
    public LegacyBaseResponseDto helmetStatus(@RequestHeader String token) {

        try{
            User user = this.userRepository.findFirstByTokenAndActive(token, true);
            if(user == null){
                throw new CustomException("User not found", 401);
            }
            return new LegacySuccessResponseWithDataDto(paidHelmetService.getPostpaidHelmetStatus(user));
        } catch (Exception e){
            return new LegacyFailureResponseDto(e.getMessage());
        }
    }


    /***
     * To call payments api
     * @param token user token
     * @param requestDto amount, address, payment_id
     * @return
     */
    @RequestMapping(path = "/api/app/capture_payment", method = RequestMethod.POST)
    @AuthFilter
    public LegacyBaseResponseDto paidHelmetCaptureRazorpay(@RequestHeader String token, @RequestBody PaidHelmetCaptureRazorpayDto requestDto) {
         try {
             User user = this.userRepository.findFirstByTokenAndActive(token, true);
             if(user == null){
                 throw new CustomException("User not found", 401);
             }
             Double amountInr = requestDto.getAmountInr();
             String paymentId = requestDto.getPaymentId();

//             paidHelmetService.HelmetPostpaidPayment(user, amountInr, paymentId, requestDto.getAddress(),
//                     requestDto.getPinCode(), requestDto.getMethod());
             //call blowhorn api
             Map<String, Object> capturePaymentData = new HashMap<>();
             capturePaymentData.put("order_id", "some order id");
             return new LegacySuccessResponseWithDataDto(capturePaymentData);

         } catch (CustomException e){
             return new LegacyFailureResponseDto(e.getData(), 400);
         } catch (Exception e) {
             return new LegacyFailureResponseDto(e.getMessage());
         }
    }

    /***
     * to send helmet status to client
     * @param token User token
     * @return
     */
    @RequestMapping(path = "/helmet_tracking", method = RequestMethod.GET)
    @AuthFilter
    public LegacyBaseResponseDto helmetTracking(@RequestHeader String token) {
        try {

            User user = this.userRepository.findFirstByTokenAndActive(token, true);
            if(user == null) {
                throw new CustomException("User not found", 401);
            }
            return new LegacySuccessResponseWithDataDto(paidHelmetService.getTrackingDetails(user));
        } catch (CustomException e) {
            if(e.getDataMap() != null) {
                return new LegacyFailureResponseWithDataDto(e.getMessage(), e.getCode(), e.getDataMap());
            }
            return new LegacyFailureResponseDto(e.getMessage(), e.getCode());
        } catch (Exception e){
            return new LegacyFailureResponseDto(e.getMessage());
        }
    }


    /***
     * used by ops to set order/tracking status
     * @param token User token
     * @return
     */
    @RequestMapping(path = "/set_tacking_status", method = RequestMethod.POST)
    @AuthFilter
    public LegacyBaseResponseDto setTrackingStatus(@RequestHeader String token, @RequestBody PaidHelmetTrackingDto requestDto) {
        try {
            User user = this.userRepository.findFirstByTokenAndActive(token, true);
            if(user == null) {
                throw new CustomException("User not found", 401);
            }
            paidHelmetService.setTrackingDetails(requestDto);
            return new LegacySuccessResponseDto("Status updated!!");
        } catch (CustomException e) {
            if(e.getDataMap() != null) {
                return new LegacyFailureResponseWithDataDto(e.getMessage(), e.getCode(), e.getDataMap());
            }
            return new LegacyFailureResponseDto(e.getMessage(), e.getCode());
        } catch (Exception e){
            return new LegacyFailureResponseDto(e.getMessage());
        }
    }


    @RequestMapping(path = "/return_helmet", method = RequestMethod.GET)
    @AuthFilter
    public LegacyBaseResponseDto paidHelmetReturn(@RequestHeader String token) {
        try {
            User user = this.userRepository.findFirstByTokenAndActive(token, true);
            if (user == null) {
                throw new CustomException("User not found", 401);
            }
            return new LegacySuccessResponseWithDataDto(paidHelmetService.getReturnDetails(user));
        } catch (CustomException e) {
            if(e.getDataMap() != null) {
                return new LegacyFailureResponseWithDataDto(e.getMessage(), e.getCode(), e.getDataMap());
            }
            return new LegacyFailureResponseDto(e.getMessage(), e.getCode());
        } catch (Exception e){
            return new LegacyFailureResponseDto(e.getMessage());
        }
    }

    @RequestMapping(path = "/confirm_refund", method = RequestMethod.POST)
    @AuthFilter
    public LegacyBaseResponseDto paidHelmetRefund(@RequestHeader String token) {
        try {

            User user = this.userRepository.findFirstByTokenAndActive(token, true);
            if (user == null) {
                throw new CustomException("User not found", 401);
            }
            paidHelmetService.helmetPostpaidRefund(user);
            return new LegacySuccessResponseDto("Refund Initiated");
        } catch (CustomException e) {
            if(e.getDataMap() != null) {
                return new LegacyFailureResponseWithDataDto(e.getMessage(), e.getCode(), e.getDataMap());
            }
            return new LegacyFailureResponseDto(e.getMessage(), e.getCode());
        } catch (Exception e){
            return new LegacyFailureResponseDto(e.getMessage());
        }

    }

}
