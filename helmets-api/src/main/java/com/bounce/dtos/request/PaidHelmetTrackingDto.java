package com.bounce.dtos.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaidHelmetTrackingDto {

    @JsonProperty("status")
    private String status;

    @JsonProperty("awb_number")
    private String awbNumber;

    @JsonProperty("user_id")
    private Long userId;
}
