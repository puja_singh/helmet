package com.bounce.dtos.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaidHelmetCaptureRazorpayDto {

    @JsonProperty("amount_inr")
    private Double amountInr;

    @JsonProperty("payment_id")
    private String paymentId;

    @JsonProperty("address")
    private String address;

    @JsonProperty("pincode")
    private Integer pinCode;

    @JsonProperty("method")
    private String method;
}
