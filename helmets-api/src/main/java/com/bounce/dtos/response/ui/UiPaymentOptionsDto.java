package com.bounce.dtos.response.ui;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;

@Slf4j
@Getter
@Setter
public class UiPaymentOptionsDto {
    private LinkedList<WalletUiDto> wallets = new LinkedList<WalletUiDto>();
    // private   UpiUiDto upi;
//    private LinkedList<PayLaterUiDto> pay_later = new LinkedList<PayLaterUiDto>();
    private LinkedList<MorePaymentsOptionUiDto> more_options = new LinkedList<MorePaymentsOptionUiDto>();
    BounceCashUiDto bounce_cash;
}
