package com.bounce.dtos.response.legacy;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class LegacySuccessResponseWithDataDto extends LegacySuccessResponseDto {
    private Map<String, Object> data;

    public LegacySuccessResponseWithDataDto(String msg, Map<String, Object> data) {
        super(msg);
        this.data = data;
    }

    public LegacySuccessResponseWithDataDto(Map<String, Object> data) {
        this.data = data;
    }
}