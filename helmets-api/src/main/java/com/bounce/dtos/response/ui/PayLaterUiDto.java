package com.bounce.dtos.response.ui;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayLaterUiDto extends UiBaseResponse {
    private String wallet_id;
    private String token;

    public PayLaterUiDto(String name, String tag, String icon, Double balance, boolean linked, String offer,
                         boolean enabled, String gateway, String error, String wallet_id, String token) {
        super(name, tag, icon, balance, linked, offer, enabled, gateway, error);
        this.wallet_id = wallet_id;
        this.token = token;
    }
}
