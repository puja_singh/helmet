package com.bounce.dtos.response.legacy;

import com.bounce.dtos.response.BaseResponseDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public abstract class LegacyBaseResponseDto extends BaseResponseDto {
    private String msg;
    private int status;

    public LegacyBaseResponseDto(String msg, int status) {
        super(msg, status);
        this.msg = msg;
        this.status = status;
    }
}
