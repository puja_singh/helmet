package com.bounce.dtos.response.ui;

import com.bounce.dtos.response.BaseResponseDto;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UiResponseDto extends BaseResponseDto {
    private String juspay_customer_id;
    private Double amount;
    private Double nbcash_amount;
    private Double nbcash_amount_in_paisa;
    private UiPaymentOptionsDto options;

    public UiResponseDto(String message, int code) {
        super(message, code);
    }
}
