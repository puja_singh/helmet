package com.bounce.dtos.response.ui;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class UpiOptionsUiDto extends UiBaseResponse {
    private String packageName;

    public UpiOptionsUiDto(String name, String tag, String icon, Double balance, boolean linked, String offer, Boolean enabled,
                           String gateway, String error, String packageName) {
        super(name, tag, icon, balance, linked, offer, enabled, gateway, error);
        this.packageName = packageName;
    }
}
