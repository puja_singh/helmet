package com.bounce.dtos.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class FailureResponseWithDataDto extends BaseResponseDto {
    private static final String FAILURE = "failure";

    private Map<String, Object> data;

    public FailureResponseWithDataDto(String message, int code, Map<String, Object> data) {
        super(message, code);
        this.data = data;
    }
}
