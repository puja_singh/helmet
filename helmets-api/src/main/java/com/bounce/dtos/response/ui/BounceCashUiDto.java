package com.bounce.dtos.response.ui;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BounceCashUiDto extends UiBaseResponse {
    private double add_bounce_cash;

    public BounceCashUiDto(String name, String tag, String icon, double balance, boolean linked, String offer,
                           boolean enabled, String gateway, String error, double addBoucnecash) {
        super(name, tag, icon, balance, linked, offer, enabled, gateway, error);
        this.add_bounce_cash = addBoucnecash;
    }
}
