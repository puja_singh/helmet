package com.bounce.dtos.response.legacy;

import org.apache.http.HttpStatus;

public class LegacyFailureResponseDto extends LegacyBaseResponseDto {
    private static final String FAILURE = "failure";

    public LegacyFailureResponseDto(String msg, int code) {
        super(msg, code);
    }

    public LegacyFailureResponseDto(int code) {
        super(FAILURE, code);
    }

    public LegacyFailureResponseDto(String msg) {
        super(msg, HttpStatus.SC_BAD_REQUEST);
    }
}
