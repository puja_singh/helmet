package com.bounce.dtos.response;

public class FailureResponseDto extends BaseResponseDto {

    private String data;

    public FailureResponseDto(String message,  String data, int code){
        super(message, code);
        this.data = data;
    }

    public FailureResponseDto(String message, int code) {
        super(message, code);
    }

    public FailureResponseDto(int code) {
        super("failure", code);
    }

    public FailureResponseDto(String message) {
        super(message, 400);
    }
}
