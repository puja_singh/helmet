package com.bounce.dtos.response.ui;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public abstract class UiBaseResponse {
    private String name;
    private String tag;
    private String icon;
    private Double balance;
    private Boolean linked;
    private String offer;
    private Boolean enabled;
    private String gateway;
    private String error;
}
