package com.bounce.dtos.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class SuccessResponseWithDataDto extends SuccessResponseDto {
    private Map<String, Object> data;

    public SuccessResponseWithDataDto(String message, Map<String, Object> data) {
        super(message);
        this.data = data;
    }

    public SuccessResponseWithDataDto(Map<String, Object> data) {
        this.data = data;
    }
}