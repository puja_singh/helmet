package com.bounce.dtos.response.legacy;

import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpStatus;

@Getter
@Setter
public class LegacySuccessResponseDto extends LegacyBaseResponseDto {

    public LegacySuccessResponseDto(String msg) {
        super(msg, HttpStatus.SC_OK);
    }

    public LegacySuccessResponseDto() {
        super("success", HttpStatus.SC_OK);
    }
}
