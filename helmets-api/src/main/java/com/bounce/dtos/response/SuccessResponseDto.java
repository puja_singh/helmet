package com.bounce.dtos.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuccessResponseDto extends BaseResponseDto {

//    private String test;

    public SuccessResponseDto(String message) {
        super(message, 200);
    }

    public SuccessResponseDto() {
        super("success", 200);
    }
}
