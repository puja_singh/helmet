package com.bounce.dtos.response.legacy;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class LegacyFailureResponseWithDataDto extends LegacyBaseResponseDto {
    private static final String FAILURE = "failure";

    private Map<String, Object> data;

    public LegacyFailureResponseWithDataDto(String msg, int code, Map<String, Object> data) {
        super(msg, code);
        this.data = data;
    }
}
