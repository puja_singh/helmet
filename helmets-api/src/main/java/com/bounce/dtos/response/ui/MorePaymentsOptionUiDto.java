package com.bounce.dtos.response.ui;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MorePaymentsOptionUiDto extends UiBaseResponse {

    public MorePaymentsOptionUiDto(String name, String tag, String icon, Double balance, boolean linked, String offer, boolean enabled, String gateway, String error) {
        super(name, tag, icon, balance, linked, offer, enabled, gateway, error);
    }
}
