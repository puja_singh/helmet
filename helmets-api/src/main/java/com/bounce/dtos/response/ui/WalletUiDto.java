package com.bounce.dtos.response.ui;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WalletUiDto extends UiBaseResponse {
    private Double add_balance;

    public WalletUiDto(String name, String tag, String icon, Double balance, boolean linked, String offer,
                       boolean enabled, String gateway, String error, double addBalance) {
        super(name, tag, icon, balance, linked, offer, enabled, gateway, error);
        this.add_balance = addBalance;
    }
}
