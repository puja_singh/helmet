package com.bounce.dtos.response.ui;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;

@Getter
@Setter
@Slf4j
public class UpiUiDto {
    private boolean enabled = false;
    private String gateway = "Juspay";
    private String icon = "https://userdocs.bounceshare.com/Payment_Icons/small_upi.png";
    private String name = "UPI Apps";
    private LinkedList<UpiOptionsUiDto> options = new LinkedList<UpiOptionsUiDto>();
}
