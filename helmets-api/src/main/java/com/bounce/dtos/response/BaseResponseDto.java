package com.bounce.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseResponseDto {
    private String message;
    private int code;

    public BaseResponseDto(String message) {
        this.message = message;
    }
}
