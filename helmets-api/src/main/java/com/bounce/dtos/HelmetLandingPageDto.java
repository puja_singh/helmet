package com.bounce.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HelmetLandingPageDto {
    @JsonProperty("amount_inr")
    private Double amountInr;

    @JsonProperty("payment_id")
    private String paymentId;

    @JsonProperty("address")
    private String address;

    @JsonProperty("pincode")
    private Integer pinCode;

    @JsonProperty("method")
    private String method;
}
