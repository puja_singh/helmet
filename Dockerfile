# Official ubuntu image
FROM 949397323834.dkr.ecr.ap-south-1.amazonaws.com/java11:latest

EXPOSE 80

ARG PORT=80
ARG ENVIRONMENT
ARG SKIPTEST=true

RUN echo "PORT $PORT"
RUN echo "ENV $ENVIRONMENT"

ENV PORT=$PORT
ENV ENVIRONMENT=$ENVIRONMENT
ENV SKIPTEST=$SKIPTEST

# Working dir
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

RUN mvn install:install-file -Dfile=/app/PaytmChecksum.jar -DgroupId=com.paytm.pg -DartifactId=pg-checksum -Dversion=1.0 -Dpackaging=jar
RUN mvn clean package -DskipTests=$SKIPTEST
CMD PORT=$PORT ENVIRONMENT=$ENVIRONMENT /root/.rbenv/shims/foreman start