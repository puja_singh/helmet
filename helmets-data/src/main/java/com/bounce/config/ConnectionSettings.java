package com.bounce.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ConnectionSettings {

    private String username;

    private String password;

    private String host;

    private String port;

    private String databaseName;

    public static ConnectionSettings fromDatabaseUrl(String databaseUrl) {
        String strippedConnectionSettings = databaseUrl.split("//")[1];
        String leftPart = strippedConnectionSettings.split("@")[0];
        String rightPart = strippedConnectionSettings.split("@")[1];
        String hostPart = rightPart.split("/")[0];

        String database = rightPart.split("/")[1];
        String username = leftPart.split(":")[0];
        String password = leftPart.split(":")[1];
        String host = hostPart.split(":")[0];
        String port = hostPart.split(":")[1];
        return new ConnectionSettings(username, password, host, port, database);
    }

}
