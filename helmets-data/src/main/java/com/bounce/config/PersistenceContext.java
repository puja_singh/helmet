package com.bounce.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Time;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
public class PersistenceContext {

    @Inject private Environment env;

    @Inject
    @Bean(name = "dataSource", destroyMethod = "close")
    protected DataSource dataSource(HikariConfig config) {
        return new HikariDataSource(config);
    }

    @Bean
    public HikariConfig hikariConfig(Environment env) {
        HikariConfig config = new HikariConfig();
        config.setPoolName("springHikariCP");
        config.setConnectionTestQuery("SELECT 1");
        config.setDataSourceClassName(env.getRequiredProperty("db.driver"));
        config.setDataSourceProperties(dataSourceProperties(env.getRequiredProperty("db.url")));
        config.setLeakDetectionThreshold(TimeUnit.SECONDS.toMillis(10));
        config.setMaximumPoolSize(10);
        // Auto commit
        config.setAutoCommit(Boolean.parseBoolean(env.getRequiredProperty("db.autocommit")));
        return config;
    }

    private Properties dataSourceProperties(String connectionString) {
        ConnectionSettings settings = ConnectionSettings.fromDatabaseUrl(connectionString);

        Properties properties = new Properties();

        properties.setProperty("user", settings.getUsername());
        properties.setProperty("portNumber", settings.getPort());
        properties.setProperty("serverName", settings.getHost());
        properties.setProperty("password", settings.getPassword());
        properties.setProperty("databaseName", settings.getDatabaseName());

        log.info("Trying to connect. Connection details are as follows - ");
        log.info("Host - {}", settings.getHost());
        log.info("Port - {}", settings.getPort());
        log.info("Db username - {}", settings.getUsername());
        log.info("Db password - {}", settings.getPassword());
        log.info("Db name - {}", settings.getDatabaseName());
        return properties;
    }
}
