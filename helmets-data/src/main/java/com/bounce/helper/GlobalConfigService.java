package com.bounce.helper;

import com.bounce.entity.GlobalConfig;
import com.bounce.repository.GlobalConfigRepository;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
@Named
public class GlobalConfigService{
    @Inject
    private GlobalConfigRepository globalConfigRepository;

    private static ConcurrentHashMap<String, String> cache = new ConcurrentHashMap<>();
    private static ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
    {
        final int DELAY_INTERVAL = 5;
        executorService.scheduleAtFixedRate(() -> {
            try {
                log.info("GlobalConfigService:: cache Refreshed!!!");

                List<GlobalConfig> globalConfigList = globalConfigRepository.findAll();
                for (GlobalConfig globalConfig : globalConfigList) {
                    cache.put(globalConfig.getKey(), globalConfig.getValue());
                }
            } catch (Exception e) {
                log.error("Error Happend while refreshing GlobalConfig Cache", e);
            }
        }, DELAY_INTERVAL, DELAY_INTERVAL, TimeUnit.MINUTES);
    }

    private String getValue(String key) {
        if(!cache.containsKey(key)) {
            cache.put(key, globalConfigRepository.findByKey(key).getValue());
        }
        return cache.get(key);
    }

    public boolean isPaytmMandatoryInBookBike() {
        return "true".equals(getValue("BOOK_BIKE_PAYTM_MANDATORY"));
    }

    public double getPaytmWalletMinBalance() {
        return Double.valueOf(getValue("PAYTM_WALLET_MIN_BALANCE"));
    }

    public double getCyclePaytmWalletMinBalance() {
        return Double.valueOf(getValue("CYCLE_PAYTM_MIN_BALANCE"));
    }

    public double getA2bMinBaseFare() {
        return Double.valueOf(getValue("A2B_MIN_BASE_FARE"));
    }

    public boolean isBillingAmnestyOn() {
        return "true".equals(getValue("BILLING_AMNESTY_ON"));
    }

    public long getBillingAmnestyAmount() {
        return Long.valueOf(getValue("BILLING_AMNESTY_AMOUNT"));
    }

    public long getBillingAmnestyDistance() {
        return Long.valueOf(getValue("BILLING_AMNESTY_DISTANCE"));
    }

    public double getPostpaidSecurityAmount() {
        return Double.valueOf(getValue("POSTPAID_SECURITY_DEPOSIT"));
    }

    public boolean stopPaidHelmetCapture() {
        return "true".equals(getValue("STOP_PAID_HELMET_CAPTURE"));
    }

    public boolean stopNewHelmetFeature() {
        return "true".equals(getValue("STOP_PAID_HELMET_CAPTURE"));
    }

    public double getHelmetPostpaidDepositAmount() {
        return Double.valueOf(getValue("POSTPAID_HELMET_DEPOSIT"));
    }

    public int getHelmetPostpaidDeposit(){return Integer.valueOf(getValue("POSTPAID_HELMET_DEPOSIT"));}
    //TODO: Delete one of getHelmetPostpaidDepositAmount / getHelmetPostpaidDeposit and replace it's usages with one kept

    public boolean getJuspayAmazonPayEnabled() {
        return "true".equals(getValue("JUSPAY_AMZPAY_ENABLED"));
    }

    public boolean getJuspayPaytmEnabled() {
        return "true".equals(getValue("JUSPAY_PAYTM_ENABLED"));
    }

    public boolean getJuspayUpiEnabled() {
        return "true".equals(getValue("JUSPAY_UPI_ENABLED"));
    }

    public boolean getJuspayUpiGpayEnabled() {
        return "true".equals(getValue("JUSPAY_UPI_GPAY_ENABLED"));
    }
    public boolean getJuspayUpiPhonepeEnabled() {
        return "true".equals(getValue("JUSPAY_UPI_PPAY_ENABLED"));
    }
    public boolean getJuspayUpiAmazonPayEnabled() {
        return "true".equals(getValue("JUSPAY_UPI_AMZPAY_ENABLED"));
    }
    public boolean getJuspayUpiPaytmEnabled() {
        return "true".equals(getValue("JUSPAY_UPI_PAYTM_ENABLED"));
    }
    public boolean getJuspayUpiBhimEnabled() {
        return "true".equals(getValue("JUSPAY_UPI_BHIM_ENABLED"));
    }

    public boolean getJuspayOtherPaymentMethodsEnabled() {
        return "true".equals(getValue("JUSPAY_RAZORPAY_ENABLED"));
    }

}
