package com.bounce.entity;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@Table(name = "dues_ledger")
public class DuesLedger extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial)")
    private Long id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "amount", columnDefinition = "float8")
    private Double amount;

    @Column(name = "mode", length = 16)
    private String mode;

    @Column(name = "source", length = 16)
    private String source;

    @Column(name = "booking_id")
    private Integer bookingId;

    @Column(name = "payment_method", length = 16)
    private String paymentMethod;

    @Column(name = "payment_ref_id" , columnDefinition = "int4")
    private Long paymentRefId;

    @Column(name = "total_due_amount", columnDefinition = "float8")
    private Double totalDueAmount;

    @Column(name = "comments", length = 128)
    private String comments;
}
