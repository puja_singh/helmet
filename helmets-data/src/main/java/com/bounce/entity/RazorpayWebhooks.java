package com.bounce.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "razorpay_webhooks")
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class RazorpayWebhooks extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", columnDefinition = "int4")
    private Integer id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "payment_id", length = 32 )
    private String paymentId;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "mobile_no", length = 10)
    private String mobileNumber;

    @Column(name = "call_type", length = 32)
    private String callType;

    @Column(name = "capture_status", length = 32)
    private String captureStatus;

    @Column(name = "refund_status", length = 32)
    private String refundStatus;

    @Column(name = "refund_id", length = 32)
    private String refundId;

    @Column(name = "mobile_app", length = 32)
    private String mobileApp;

    @Column(name = "app_version", length = 32)
    private String appVersion;

}
