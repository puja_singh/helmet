package com.bounce.entity.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

@Getter
public enum  DistanceType {

    odometer("odometer"), google("google");

    private String code;

    private static final Map<String, DistanceType> map = new HashMap<>();

    static {
        Stream.of(DistanceType.values()).forEach(distanceType -> {
            map.put(distanceType.getCode(), distanceType);
        });
    }

    public static DistanceType fromCode(String code) {
        DistanceType retVal = map.get(code);

        if (Objects.isNull(retVal)) {
            retVal = Stream.of(DistanceType.values())
                    .filter(dType -> Objects.equals(dType.getCode(), code))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("No such enum present " + code));
        }

        return retVal;
    }

    DistanceType(String s) {
        this.code = s;
    }
}
