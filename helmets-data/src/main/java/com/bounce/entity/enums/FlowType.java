package com.bounce.entity.enums;

public enum FlowType {
    CLEAR_DUES, PAID_HELMET, POSTPAID, WALLET_TOPUP, GENERIC
}
