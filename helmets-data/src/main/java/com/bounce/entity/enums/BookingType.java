package com.bounce.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

@Getter
public enum BookingType {

    PICKUP("pick-up"), DROP("drop");

    private String code;

    private static final Map<String, BookingType> map = new HashMap<>();

    static {
        Stream.of(BookingType.values())
                .forEach(bookingType -> {
                    map.put(bookingType.getCode(), bookingType);
                });
    }

    public static BookingType fromCode(String code) {
        BookingType retVal = map.get(code);

        if (Objects.isNull(retVal)){
            for (BookingType type : BookingType.values()) {
                if (type.getCode().equals(code)) {
                    retVal = type;
                }
            }
            if (Objects.isNull(retVal)){
                throw new IllegalArgumentException("No suitable enum found for conversion. Please check!");
            }
        }

        return retVal;
    }

    BookingType(String s) {
        this.code = s;
    }

}
