package com.bounce.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PaymentMethod {
    postpaid, razorpay, paytm, juspay, bounce_cash;
}
