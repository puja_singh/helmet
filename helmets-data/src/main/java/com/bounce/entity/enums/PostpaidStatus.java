package com.bounce.entity.enums;

public enum PostpaidStatus {
    linked, not_linked, refund_initiated
}
