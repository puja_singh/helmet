package com.bounce.entity.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

@Getter
public enum BookingStatus {

    CANCELLED("cancelled"), HOLD("hold"), RESERVED("reserved"), IN_DELIVERY("in_delivery"), DELIVERED("delivered"),
    IN_TRIP("in_trip"), COMPLETED("completed");

    private static final Map<String, BookingStatus> map = new HashMap<>();

    static {
        Stream.of(BookingStatus.values())
                .forEach(bookingStatus -> {
                    map.put(bookingStatus.getCode(), bookingStatus);
                });
    }

    private String code;

    public static BookingStatus fromCode(String code) {
        BookingStatus retVal = map.get(code);

        if (Objects.isNull(retVal)) {
            for (BookingStatus type : values()) {
                if (type.getCode().equals(code)) {
                    map.put(code, type);
                    retVal = type;
                }
            }
            if (retVal == null)
                throw new IllegalArgumentException("No suitable enum found for conversion. Please check!");
        }

        return retVal;
    }

    BookingStatus(String s) {
        this.code = s;
    }
}
