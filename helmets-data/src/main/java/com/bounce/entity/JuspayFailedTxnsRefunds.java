package com.bounce.entity;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@Table(name = "juspay_failed_transactions_refunds")
@AllArgsConstructor
@NoArgsConstructor
public class JuspayFailedTxnsRefunds extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "cust_id")
    private String custId;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "refund_request_id")
    private String refundRequestId;

    @Column(name = "order_id", length = 32)
    private String orderId;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "status", length = 32)
    private String status;

    @Column(name = "remarks")
    private String remarks;


}
