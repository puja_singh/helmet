package com.bounce.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
//todo check if these two can be removed
@Table(name = "paytm_transaction")
public class PaytmTransaction extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "booking_id", columnDefinition = "int4")
    private Long bookingId;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private Customer customer;

    @Column(name = "type")
    private String type;

    @Column(name = "call_type")
    private String callType;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "paytm_token")
    private String paytmToken;

    @Column(name = "paytm_transaction_id")
    private String paytmTransactionId;

    @Column(name = "bank_transaction_id")
    private String bankTransactionId;

    @Column(name = "method", length = 16)
    private String method;

    @Column(name="trans_amount")
    private Double transAmount;

    @Column(name = "result")
    private Boolean result;

    //TODO: fix this
//    @Type( type = "jsonb-node" )
//    @Column(name = "api_response", columnDefinition = "jsonb")
//    private JsonNode apiResponse;
}
