package com.bounce.entity;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "wallet_topup")
public class WalletTopup extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "payment_id")
    private String paymentId;

    @Column(name = "offer_id", columnDefinition = "int4")
    private Long offerId;

    @Column(name = "amount_inr", columnDefinition = "float8")
    private Double amountInr;

    @Column(name = "wallet_value", columnDefinition = "float8")
    private Double walletValue;

    @Column(name = "php_response")
    private String phpResponse;

}
