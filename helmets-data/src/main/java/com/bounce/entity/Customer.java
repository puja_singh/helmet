package com.bounce.entity;

import com.bounce.entity.enums.PostpaidStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@Entity
@ToString
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int4")
    private Long id;

    @Column(name = "paytm_token")
    private String paytmToken;

    @Column(name = "paytm_token_expiry")
    private Date paytmTokenExpiry;

    @Column(name = "referral_code", length = 10)
    private String referralCode;

    @Column(name = "referral_code_used", length = 10)
    private String referralCodeUsed;

    @Column(name = "total_amount_due", columnDefinition = "float8")
    private Double totalAmountDue;

    @Column(name = "updated_on")
    private Date updatedOn;

    @Column(name = "dues_updated_on")
    private Date duesUpdatedOn;

    @Column(name = "paytm_mobile", length = 10)
    private String paytmMobile;

    @Column(name = "postpaid_deposit_amount", columnDefinition = "float8")
    private Double postpaidDepositAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "postpaid_status")
    private PostpaidStatus postpaidStatus;

}
