package com.bounce.entity;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@JsonPOJOBuilder
@Table(name = "wallet_topup_offers")
public class WalletTopupOffers extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "price", columnDefinition = "float8")
    private Double price;

    @Column(name = "discounted_price", columnDefinition = "float8")
    private Double discountedPrice;

    @Column(name = "wallet_value", columnDefinition = "float8")
    private Double walletValue;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "image", length = 32)
    private String image;

    @Column(name = "tag", length = 16)
    private String tag;
}
