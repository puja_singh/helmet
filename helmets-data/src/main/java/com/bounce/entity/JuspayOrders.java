package com.bounce.entity;

import com.bounce.entity.enums.FlowType;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@Table(name = "juspay_orders")
@AllArgsConstructor
@NoArgsConstructor
public class JuspayOrders extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "order_id", length = 32)
    private String orderId;

    @Column(name = "juspay_order_id")
    private String juspayOrderId;

    @Column(name = "cust_id")
    private String custId;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "juspay_order_status")
    private String juspayOrderStatus;

    @Column(name = "order_status")
    private String orderStatus;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Column(name = "payment_method_type")
    private String paymentMethodType;

    @Column(name = "txn_id", length = 32)
    private String txnId;

    @Column(name = "refund_status")
    private Boolean refundStatus;

    @Column(name = "user_app", length = 32)
    private String userApp;

    @Column(name = "app_version", length = 32)
    private String appVersion;

    @Enumerated(EnumType.STRING)
    @Column(name = "flow_type")
    private FlowType flowType;
}
