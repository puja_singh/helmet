package com.bounce.entity;

import com.bounce.entity.enums.PostpaidStatus;
import com.sun.xml.internal.ws.developer.UsesJAXBContext;
import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "helmet")
public class Helmet extends BaseEntity{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "amount", columnDefinition = "float8")
    private Double amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private String status;

    @Column(name = "order_date")
    private Date orderDate;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "awb_number")
    private String awbNumber;

    @Column(name = "refund_request_date")
    private Date refundRequestDate;

    @Column(name = "address", length = 255)
    private String address;

    @Column(name = "pincode")
    private Integer pincode;

}
