package com.bounce.entity;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "wallet_topup_transaction")
@Builder
public class WalletTopupTransaction extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name="amount")
    private Double amount;

    @Column(name = "payment_id")
    private String paymentId;

    @Column(name = "wallet_txn_id")
    private String walletTxnId;

    @Column(name = "result")
    private Boolean result;

}
