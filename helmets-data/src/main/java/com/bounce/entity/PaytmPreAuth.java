package com.bounce.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@Table(name = "paytm_preauth")
public class PaytmPreAuth extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "booking_id", columnDefinition = "int4")
    private Long bookingId;

    @Column(name = "order_id")
    private String orderId;
}