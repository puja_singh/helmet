package com.bounce.entity;

import com.bounce.entity.enums.PostpaidStatus;
import com.sun.xml.internal.ws.developer.UsesJAXBContext;
import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "paid_helmet")
public class PaidHelmet extends BaseEntity{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "amount", columnDefinition = "float8")
    private Double amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "postpaid_status")
    private PostpaidStatus postpaidStatus;

    @Column(name = "paid_date")
    private Date paidDate;

    @Column(name = "refund_request_date")
    private Date refundRequestDate;

    @Column(name = "address", length = 255)
    private String address;

    @Column(name = "pincode")
    private Integer pincode;

    @Column(name = "order_id")
    private String orderId;

}
