package com.bounce.entity.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Pricing implements Serializable {

    @JsonProperty("price_per_minute")
    private Double pricePerMinute;

    @JsonProperty("price_per_km")
    private Double pricePerKm;

    @JsonProperty("delivery_charge_fixed")
    private Double deliveryChargeFixed;

    @JsonProperty("delivery_charge_km")
    private Double deliveryChargeKm;

    @JsonProperty("category")
    private String category;

    @JsonProperty("city")
    private String city;
}
