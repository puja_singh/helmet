package com.bounce.entity.json.converter;

import com.bounce.entity.json.Pricing;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.SerializationException;
import org.hibernate.usertype.UserType;
import org.postgresql.util.PGobject;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

@Slf4j
public class PricingConverter implements UserType {

    private final ObjectMapper serializer = new ObjectMapper();

    @Override
    public int[] sqlTypes() {
        return new int[] {Types.JAVA_OBJECT};
    }

    @Override
    public Class returnedClass() {
        return Pricing.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return ObjectUtils.nullSafeEquals(x, y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        if (x == null) {
            return 0;
        }
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        PGobject o = (PGobject) rs.getObject(names[0]);
        if (o.getValue() != null) {
            try {
                return serializer.readValue(o.getValue(), Pricing.class);
            } catch (IOException e) {
                log.error("[PricingConverter]: Error while reading database json value. Sending default");
                log.error("[PricingConverter]: {}", e.getMessage());
                log.warn("[PricingConverter]:", e);
            }
        }

        return new Pricing();
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if (value == null) {
            st.setNull(index, Types.OTHER);
        } else {
            final Pricing pricing = (Pricing) value;
            try {
                st.setObject(index, serializer.writeValueAsString(pricing), Types.OTHER);
            } catch (JsonProcessingException e) {
                log.error("[PricingConverter]: Error while writing to database json value. Sending default");
                log.error("[PricingConverter]: {}", e.getMessage());
                log.warn("[PricingConverter]:", e);
            }
        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        if (value == null) {
            return null;
        }

        if (!(value instanceof Pricing)) {
            return null;
        }

        final Pricing resultMap = new Pricing();
        resultMap.setCity(((Pricing) value).getCity());
        resultMap.setCategory(((Pricing) value).getCategory());
        resultMap.setDeliveryChargeFixed(((Pricing) value).getDeliveryChargeFixed());
        resultMap.setDeliveryChargeKm(((Pricing) value).getDeliveryChargeKm());
        resultMap.setPricePerKm(((Pricing) value).getPricePerKm());
        resultMap.setPricePerMinute(((Pricing) value).getPricePerMinute());

        return resultMap;

    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        Object copy = deepCopy(value);

        if (copy instanceof Serializable) {
            return (Serializable) copy;
        }

        throw new SerializationException(String.format("Cannot serialize '%s', %s is not Serializable.", value, value.getClass()), null);
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return deepCopy(cached);
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return deepCopy(original);
    }
}
