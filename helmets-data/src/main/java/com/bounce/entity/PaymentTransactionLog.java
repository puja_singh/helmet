package com.bounce.entity;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "payment_transaction_log")
public class PaymentTransactionLog extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "booking_id", columnDefinition = "int4")
    private Long bookingId;

    @Column(name="amount")
    private Double amount;

    @Column(name = "type")
    private String type;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Column(name = "tracking_id")
    private String trackingId;

    @Column(name = "call_type")
    private String callType;

    @Column(name = "status")
    private String status;
}
