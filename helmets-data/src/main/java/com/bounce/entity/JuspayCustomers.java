package com.bounce.entity;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@Table(name = "juspay_customer")
@AllArgsConstructor
@NoArgsConstructor
public class JuspayCustomers extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "cust_id")
    private String custId;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "juspay_cust_id")
    private String jusypayCustId;

}
