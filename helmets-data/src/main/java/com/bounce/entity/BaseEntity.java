package com.bounce.entity;

import com.bounce.entity.enums.converter.BookingStatusConverter;
import com.bounce.entity.enums.converter.BookingTypeConverter;
import com.bounce.entity.enums.converter.DistanceTypeConverter;
import com.bounce.entity.json.converter.PricingConverter;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@TypeDefs({
        @TypeDef(name = "pricingConverter", typeClass = PricingConverter.class),
        @TypeDef(name = "bookingStatusConverter", typeClass = BookingStatusConverter.class),
        @TypeDef(name = "bookingTypeConverter", typeClass = BookingTypeConverter.class),
        @TypeDef(name = "distanceTypeConverter", typeClass = DistanceTypeConverter.class)
})
@MappedSuperclass
public class BaseEntity {

    @Column(name = "created_on")
    private Date createdAt;

    @Column(name = "updated_on")
    private Date updatedAt;

    @PrePersist
    public void postPersist() {
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    @PostUpdate
    public void postUpdate() {
        this.updatedAt = new Date();
    }
}