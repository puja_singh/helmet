package com.bounce.entity;

import com.bounce.entity.enums.BookingStatus;
import com.bounce.entity.enums.BookingType;
import com.bounce.entity.enums.DistanceType;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.entity.json.Pricing;
import com.bounce.entity.json.converter.PricingConverter;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "booking")
@TypeDef(name = "pricingConverter", typeClass = PricingConverter.class)
public class Booking extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", columnDefinition = "int4")
    private Customer customer;

    @Column(name = "bike_id", columnDefinition = "int4")
    private Long bikeId;

    @Type(type = "bookingTypeConverter")
    @Column(name = "booking_type", columnDefinition = "booking_type")
    private BookingType bookingType;

    @Type(type = "bookingStatusConverter")
    @Column(name = "status", columnDefinition = "booking_status")
    private BookingStatus bookingStatus;

    @Type(type = "pricingConverter")
    @Column(name = "pricing_json")
    private Pricing pricing;

    @Column(name = "est_cost")
    private Double estCost;

    @Column(name = "est_time")
    private Double estTime;

    @Column(name = "est_distance")
    private Double estDistance;

    @Column(name = "est_promo_used")
    private Double estRewarPointsUsed;

    @Column(name = "est_non_promo_used")
    private Double estBounceCashUsed;

    @Column(name = "est_tax")
    private Double estTax;

    @Column(name = "est_paytm_deductable", columnDefinition = "float8")
    private Double estimatedPaytmDeductable;

    @Column(name = "actual_cost")
    private Double actualCost;

    @Column(name = "actual_promo_used")
    private Double actualRewarPointsUsed;

    @Column(name = "actual_non_promo_used")
    private Double actualBounceCashUsed;

    @Column(name = "actual_tax")
    private Double actualTax;

    @Column(name = "actual_paytm_deductable")
    private Double actualPaytmDeductable;

    @Column(name = "wallet_txn_id", columnDefinition = "int4")
    private Long walletTxnId;

    @Column(name = "actual_total_amount")
    private Double actualTotalAmount;

    @Column(name = "actual_time", columnDefinition = "float8")
    private Double actualTime;

    @Column(name = "actual_distance", columnDefinition = "float8")
    private Double actualDistance;

    @Type(type = "distanceTypeConverter")
    @Column(name = "distance_type", columnDefinition = "distance_type")
    private DistanceType distanceType;

    @Column(name = "trip_placed_time")
    private Date tripPlacedTime;

    @Column(name = "trip_start_time")
    private Date tripStartTime;

    @Column(name = "trip_end_time")
    private Date tripEndTime;

    @Column(name = "amount_due")
    private Double amountDue;

    @Column(name = "pre_auth_id")
    private String preAuthId;

    @Column(name = "pre_auth_amount", columnDefinition = "float8")
    private Double preAuthAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method", length = 16)
    private PaymentMethod paymentMethod;

    @Column(name = "act_end_point_lat")
    private Double actualEndPointLat;

    @Column(name = "act_end_point_lon")
    private Double actualEndPointLon;

    @Column(name = "act_end_address")
    private String actualEndAddress;

}
