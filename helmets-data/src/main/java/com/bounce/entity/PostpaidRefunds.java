package com.bounce.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "postpaid_refunds")
public class PostpaidRefunds extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "user_id", columnDefinition = "int4")
    private Long userId;

    @Column(name = "payment_id")
    private String paymentId;

    @Column(name="payment_method", length = 32)
    private String paymentMethod;

    @Column(name = "refund_api_response")
    private String apiResponse;
}

