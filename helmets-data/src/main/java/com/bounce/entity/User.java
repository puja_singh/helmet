package com.bounce.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "ab_user")
public class User extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", columnDefinition = "int4")
    private Long id;

    @Column(name = "mobile_no")
    private String mobileNumber;

    @Column(name = "token")
    private String token;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "email")
    private String emailId;

    @Column(name = "uid", columnDefinition = "int4")
    private Long uid;
}
