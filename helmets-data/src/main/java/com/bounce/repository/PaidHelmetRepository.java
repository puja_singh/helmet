package com.bounce.repository;

import com.bounce.entity.PaidHelmet;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PaidHelmetRepository extends JpaRepository<PaidHelmet, Long> {
    PaidHelmet findByUserId(Long id);

}
