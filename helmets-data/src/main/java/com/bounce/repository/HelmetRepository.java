package com.bounce.repository;

import com.bounce.entity.Helmet;
import org.springframework.data.jpa.repository.JpaRepository;


public interface HelmetRepository extends JpaRepository<Helmet, Long> {
    Helmet findByUserId(Long id);

}
