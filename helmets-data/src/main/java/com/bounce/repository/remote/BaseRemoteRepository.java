package com.bounce.repository.remote;

public interface BaseRemoteRepository<T> {

    T findById(Long id);

    T findById(String id);

    T save(T t);

    T delete(T t);

}
