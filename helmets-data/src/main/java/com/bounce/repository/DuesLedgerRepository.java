package com.bounce.repository;

import com.bounce.entity.DuesLedger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DuesLedgerRepository extends JpaRepository<DuesLedger, Long> {
}
