package com.bounce.repository;

import com.bounce.entity.WalletTopupTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletTopupTransactionRepository extends JpaRepository<WalletTopupTransaction, Long> {
}
