package com.bounce.repository;

import com.bounce.entity.RazorpayWebhooks;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RazorpayWebhooksRepository extends JpaRepository<RazorpayWebhooks, Long> {
}
