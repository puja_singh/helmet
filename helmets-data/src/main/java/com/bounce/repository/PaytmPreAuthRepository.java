package com.bounce.repository;

import com.bounce.entity.PaytmPreAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaytmPreAuthRepository extends JpaRepository<PaytmPreAuth, Long> {
}
