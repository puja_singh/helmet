package com.bounce.repository;

import com.bounce.entity.GlobalConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GlobalConfigRepository extends JpaRepository<GlobalConfig, Long> {

    GlobalConfig findByKey(String key);

    @Override
    List<GlobalConfig> findAll();
}
