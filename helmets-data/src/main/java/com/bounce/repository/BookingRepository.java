package com.bounce.repository;

import com.bounce.entity.Booking;
import com.bounce.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    Booking findFirstByCustomerOrderByIdDesc(Customer customer);
}
