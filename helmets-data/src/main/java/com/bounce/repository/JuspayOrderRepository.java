package com.bounce.repository;

import com.bounce.entity.JuspayOrders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JuspayOrderRepository extends JpaRepository<JuspayOrders, Long> {

    JuspayOrders findFirstByOrderIdOrderByIdDesc(String orderId);
}
