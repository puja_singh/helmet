package com.bounce.repository;

import com.bounce.entity.WalletTopupOffers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletTopupOffersRepository extends JpaRepository<WalletTopupOffers, Long> {

    WalletTopupOffers findFirstByPriceAndActiveOrderByCreatedAtDesc(double amountInr, boolean active);

    WalletTopupOffers findFirstByIdAndActiveOrderByIdDesc(long Id, boolean Active);

    WalletTopupOffers[] findAllByActiveAndWalletValueLessThanEqualOrderByIdAsc(boolean Active, double walletValue);
}
