package com.bounce.repository;

import com.bounce.entity.PaymentTransactionLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentTrasactionLogRepository extends JpaRepository<PaymentTransactionLog, Long> {
}
