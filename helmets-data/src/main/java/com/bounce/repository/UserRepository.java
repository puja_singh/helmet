package com.bounce.repository;

import com.bounce.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findFirstByMobileNumberAndActiveOrderByIdAsc(String phone, boolean Active);

    User findFirstByTokenAndActive(String token, boolean Active);

}
