package com.bounce.repository;

import com.bounce.entity.JuspayCustomers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JuspayCustomerRepository extends JpaRepository<JuspayCustomers, Long> {

    JuspayCustomers findFirstByUserIdOrderByIdDesc(long userId);

}
