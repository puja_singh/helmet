package com.bounce.repository;

import com.bounce.entity.Helmet;
import com.bounce.entity.HelmetTracking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface HelmetTrackingRepository extends JpaRepository<Helmet, Long> {
    List<HelmetTracking> findByHelmetId(int id);

}
