package com.bounce.repository;

import com.bounce.entity.JuspayFailedTxnsRefunds;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JuspayFailedTxnsRefundsRepository extends JpaRepository<JuspayFailedTxnsRefunds, Long> {
}
