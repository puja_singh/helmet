package com.bounce.repository;

import com.bounce.entity.WalletTopup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletTopupRepository extends JpaRepository<WalletTopup, Long> {
}
