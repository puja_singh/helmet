package com.bounce.repository;

import com.bounce.entity.PostpaidRefunds;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostpaidRefundsRepository extends JpaRepository<PostpaidRefunds, Long> {
}
