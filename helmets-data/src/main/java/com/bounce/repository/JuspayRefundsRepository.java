package com.bounce.repository;

import com.bounce.entity.JuspayRefunds;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JuspayRefundsRepository extends JpaRepository<JuspayRefunds, Long> {
}
