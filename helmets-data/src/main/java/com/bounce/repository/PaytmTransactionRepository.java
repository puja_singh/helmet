package com.bounce.repository;

import com.bounce.entity.Customer;
import com.bounce.entity.PaytmTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface PaytmTransactionRepository extends JpaRepository<PaytmTransaction, Long> {

    PaytmTransaction findFirstByCustomerAndTypeAndCallTypeOrderByIdDesc(Customer customer, String Type, String CallType);
}
