package com.bounce.mbpython;


import com.bounce.dtos.cash.BounceCashGetRemittableAmount;
import com.bounce.dtos.razorpaywebhook.RazorpayCaptureResponse;
import com.bounce.exceptions.CustomException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Named
public class MbPythonApi {

    @Value("${mbpython.base.url}")
    private String MBPYTHON_BASE_URL;

    private static final String CLEAR_DUES = "/clear_dues";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public RazorpayCaptureResponse clearDues(String userToken, double amountInr, String paymentId){
        try {
            String apiUrl = String.format("%s%s", MBPYTHON_BASE_URL, CLEAR_DUES);
            log.info("clear dues razorpay url is: {}", apiUrl);
            Client client = ClientBuilder.newClient();

            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("amount", amountInr);
            payload.put("payment_id", paymentId);
            payload.put("method", "razorpay");

            String result = client.target(apiUrl)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("token", userToken)
                    .post(Entity.entity(payload, MediaType.APPLICATION_JSON_TYPE), String.class);
            log.info("result of clear dues capture razorpay: {}",result);
            return objectMapper.readValue(result, RazorpayCaptureResponse.class);
        } catch (Exception e) {
            log.error("Error in clearDues: ", e);
            throw new CustomException("Error in clearDues", 400);
        }
    }

}
