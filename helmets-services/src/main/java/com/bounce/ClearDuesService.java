package com.bounce;

import com.bounce.entity.User;
import org.springframework.stereotype.Component;


@Component
public interface ClearDuesService {


    void clearDuesHelper(User user, Double amountInr, String paymentId, String method) throws Exception;
}
