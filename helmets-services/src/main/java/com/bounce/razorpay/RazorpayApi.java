package com.bounce.razorpay;

import com.bounce.dtos.AnalyticsDumpDto;
import com.bounce.utils.BillingUtil;
import com.bounce.utils.TrackingDumpUtil;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Refund;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Named;

@Slf4j
@Named
public class RazorpayApi {

    @Value("${razorpay.key.id}")
    private String KEY_ID;

    @Value("${razorpay.key.secret}")
    private String KEY_SECRET;

    private RazorpayClient getRazorpayClient() {
        RazorpayClient razorpayClient = null;
        try{
            razorpayClient = new RazorpayClient(KEY_ID, KEY_SECRET);
        } catch (RazorpayException e) {
            log.error("Error in Initializing Razorpay Client");
        }
        return razorpayClient;
    }

    public String capture(String paymentId, double amountInr, long userId) throws Exception {
        int amountPaisa = (int) BillingUtil.multiply(amountInr, 100);
        log.info("amountInr: {}, amountPaisa: {}", amountInr, amountPaisa);

        RazorpayClient razorpayClient = getRazorpayClient();
        JSONObject options = new JSONObject();
        options.put("amount", amountPaisa);
        Payment payment = null;

        insertAnimalKingdom(paymentId, userId, String.valueOf(options), "request", "capture");
        try {
            payment = razorpayClient.Payments.capture(paymentId, options);
        } catch (Exception e) {
            log.error("Error happend in capture: ", e);
            throw e;
        }
        log.info("result of capture: {}", String.valueOf(payment));
        insertAnimalKingdom(paymentId, userId, String.valueOf(payment),"response", "capture");
        return String.valueOf(payment);
    }

    private void insertAnimalKingdom(String paymentId, double userId, String options, String species, String ord) {
        try {
            AnalyticsDumpDto analyticsDumpDto = AnalyticsDumpDto.builder()
                    .kingdom("mb_python")
                    .phylum("payments")
                    .family("razorpay")
                    .species(species)
                    .order(ord)
                    .genusNum(userId)
                    .className(paymentId)
                    .tribe(options)
                    .build();
            TrackingDumpUtil.dumpIntoAnimalKingdom(analyticsDumpDto);
        } catch (Exception e){
            log.info("error inserting into animal kingdom ");
        }
    }


    //Partial refund
    public String issueFullRefund(String paymentId, double amountInr, long userId) throws Exception{
        RazorpayClient razorpayClient = getRazorpayClient();
        JSONObject refundRequest = new JSONObject();
        refundRequest.put("amount", amountInr);
        JSONObject notes=new JSONObject();
        refundRequest.put("notes", notes);

        Refund refund = null;
        insertAnimalKingdom(paymentId, userId, String.valueOf(refundRequest), "request", "refund");
        try {
            refund = razorpayClient.Payments.refund(paymentId);
        } catch (Exception e) {
            log.error("Error happend in refund: ", e);
            throw e;
        }
        log.info(String.format("result of refund: %s", String.valueOf(refund)));
        insertAnimalKingdom(paymentId, userId, String.valueOf(refund),"response", "refund");
        return String.valueOf(refund);
    }

    public String statusCheck(String paymentId) throws Exception {
        RazorpayClient razorpayClient = getRazorpayClient();
        Payment payment = razorpayClient.Payments.fetch(paymentId);
        String status = payment.get("status");
        log.info("razorpay status for the payment id: {} is {}", paymentId, status);
        return status;
    }
}
