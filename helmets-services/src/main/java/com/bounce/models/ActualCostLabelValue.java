package com.bounce.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.glassfish.jersey.server.JSONP;

@Getter
@Setter
@AllArgsConstructor
public class ActualCostLabelValue {

    @JsonProperty("label")
    String label;

    @JsonProperty("value")
    Double value;
}
