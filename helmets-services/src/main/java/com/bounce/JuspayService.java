package com.bounce;

import com.bounce.dtos.juspay.CreateOrderRequest;
import com.bounce.dtos.juspay.CreateOrderResponse;
import com.bounce.dtos.juspay.WebhookRequestDto;
import com.bounce.entity.JuspayOrders;
import com.bounce.entity.User;

public interface JuspayService {

    CreateOrderResponse createOrder(User user, CreateOrderRequest createOrderRequest);


    void serverFailedJuspayTxnsRefund(String orderId, double amount, JuspayOrders juspayOrders, String remarks);

    void Juspayrefund(long userId, double amount, String orderId);

    void insertIntoQueueJuspay(WebhookRequestDto webhookRequestDto);
}
