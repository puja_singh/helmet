package com.bounce;


import com.bounce.entity.Customer;
import com.bounce.entity.User;

public interface PostpaidService extends PaymentService {

    void PostpaidPayment(long userId, double amountInr, String paymentId, String paymentMethod) throws Exception;

    void captureRazorpayPostpaidPayment(long userId, Customer customer, double amountInr, String paymentId) ;

    void refundRazorpay(User user) throws Exception;
}
