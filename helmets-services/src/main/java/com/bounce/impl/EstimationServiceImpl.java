package com.bounce.impl;

import com.bounce.EstimationService;
import com.bounce.entity.GlobalConfig;
import com.bounce.helper.GlobalConfigService;
import com.bounce.repository.BookingRepository;
import com.bounce.repository.GlobalConfigRepository;
import com.bounce.entity.Booking;
import com.bounce.utils.BillingUtil;

import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;

@Named
public class EstimationServiceImpl implements EstimationService {

    @Inject private BookingRepository bookingRepository;
    @Inject private GlobalConfigService globalConfigService;

    @Override
    public double getEstimatedBaseFare(long bookingId, double distance, double time) {

        final Booking booking = this.bookingRepository.findById(bookingId)
                .orElseThrow(() -> new RuntimeException("Invalid booking id. No booking found for given id " + bookingId));


        double distanceCost = 0.0; //Math.round(distance) * new booking.getPricing().getPricePerKm();
        double durationCost = 0.0; //Math.round(time) * booking.getPricing().getPricePerMinute();
        double deliveryCost = 0.0;

        double totalCost = BillingUtil.add(BillingUtil.add(distanceCost,  durationCost), deliveryCost);
        double minimumBaseFare = globalConfigService.getA2bMinBaseFare();

        return Math.min(totalCost, minimumBaseFare);
    }

}
