package com.bounce.impl;

import com.bounce.BounceCashService;
import com.bounce.PaymentService;
import com.bounce.PaytmService;
import com.bounce.cash.BounceCashApi;
import com.bounce.entity.Booking;
import com.bounce.entity.Customer;
import com.bounce.exceptions.CustomException;
import com.bounce.helper.GlobalConfigService;
import com.bounce.utils.BillingUtil;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class BounceCashServiceImpl implements BounceCashService {
    @Inject
    private GlobalConfigService globalConfigService;
    @Inject private BounceCashApi bounceCashRepository;
    @Override
    public boolean canProcess(Booking booking, String userToken)
            throws CustomException {
        if(booking.getEstimatedPaytmDeductable() <=0)  {
            return false; // It won't trigger paytm locking flow
        } else {
            final double bounceCashBalance = this.bounceCashRepository.getBalance(userToken).getResult()
                    .getData().getNonPromotionalBalance();
            String reason = String.format("A minimum balance of \u20B9%s is required to make this booking",
                    BillingUtil.add(bounceCashBalance, booking.getEstimatedPaytmDeductable()));
            String message = String.format("{ \"add_money\": %s, \"balance\": %s, \"reason\": %s }", booking.getEstimatedPaytmDeductable(),
                    bounceCashBalance, reason);
            throw new CustomException (message, 607);
        }
    }
}
