package com.bounce.impl;

import com.bounce.*;
import com.bounce.cash.BounceCashApi;
import com.bounce.dtos.enums.VehicleType;
import com.bounce.dtos.paytm.LockAmountResponse;
import com.bounce.entity.Booking;
import com.bounce.entity.User;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.exceptions.CustomException;
import com.bounce.helper.GlobalConfigService;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.BookingRepository;
import com.bounce.repository.UserRepository;
import com.bounce.utils.BillingUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Named
public class BookingServiceImpl implements BookingService {

    @Inject private BookingRepository bookingRepository;
    @Inject private PaytmService paytmService;
    @Inject private PaytmRepository paytmRepository;
    @Inject private GlobalConfigService globalConfigService;
    @Inject private PaymentMethodService paymentMethodService;
    @Inject private BounceCashApi bounceCashRepository;
    @Inject private PostpaidService postpaidService;
    @Inject private BounceCashService bounceCashService;
    @Inject private UserDeviceService userDeviceService;
    @Inject private UserRepository userRepository;

    @Override
    public void paytmConditionalLock(long bookingId, String userToken) {
        final Booking booking = this.bookingRepository.findById(bookingId)
                .orElseThrow(() -> new IllegalArgumentException("No booking found for the given id"));

        User user = this.userRepository.findById(booking.getCustomer().getId())
                .orElseThrow(() -> new IllegalArgumentException("No User Found"));;

        if (mustLockPaytm(booking, userToken)) {
            final double bounceCashBalance = this.bounceCashRepository.getBalance(userToken).getResult()
                    .getData().getNonPromotionalBalance();
            final double estimatedCashAount = BillingUtil.add(booking.getEstBounceCashUsed(),
                    booking.getEstimatedPaytmDeductable());

            if(booking.getEstimatedPaytmDeductable() == 0 && userDeviceService.isFirstRide(user.getUid())) {
                return;
            }

            if (BillingUtil.compareTo(bounceCashBalance,
                    BillingUtil.max(estimatedCashAount, this.globalConfigService.getPaytmWalletMinBalance())) == -1) {
                this.blockAmount(booking, VehicleType.bike);
            }
        }
    }

    /**
     * Used to happen in old firmware
     * Fraud case done by users
     * 1. Make a booking
     * 2. Cancel booking
     * 3. Otp not refreshed on bike
     * 4. Thats how users would commit fraud
     * 5. If fraud has been committed, the user's paytm amount will be locked for a specific amount
     * 5. Do not lock amount if method is postpaid
     * @param bookingId
     */
    @Override
    public void tripAfterCancellation(long bookingId) {
        final Booking booking = this.bookingRepository.findById(bookingId).orElseThrow(() -> new
                IllegalArgumentException("No booking found for the given id"));

        if (!PaymentMethod.postpaid.equals(booking.getPaymentMethod())) {
            //Check if user has sufficient balance to do the trip
            final double walletBalance = this.paytmRepository.getBalance(booking.getCustomer().getPaytmToken(),
                    booking.getCustomer().getPaytmTokenExpiry(), booking.getCustomer().getId());

            final double estimatedTripCost = booking.getEstimatedPaytmDeductable();
            final double minRequiredBalance = this.globalConfigService.getPaytmWalletMinBalance();

            final double maxWanted = Math.max(minRequiredBalance, estimatedTripCost);

            final double amountToBeLocked = Math.min(maxWanted, walletBalance);
            try {
                LockAmountResponse lockAmountResponse = paytmRepository.lockAmount(bookingId, booking.getCustomer()
                        .getPaytmToken(), amountToBeLocked, booking.getCustomer().getId());
                updateBooking(booking, amountToBeLocked, lockAmountResponse);
            } catch (Exception e) {
                log.info("Fraud Case Locking Paytm Failed because of Race Condition");
                throw e;
            }
        }
    }

    @Override
    @Transactional
    public void releaseBlockedAmount(long bookingId) throws CustomException {
        final Booking booking = bookingRepository.findById(bookingId)
                .orElseThrow(() -> new IllegalArgumentException("No booking found for the given id"));
        if(!StringUtils.isEmpty(booking.getPreAuthId())) {
            this.paytmRepository.releaseAmount(booking.getCustomer().getPaytmToken(), booking.getPreAuthId(), booking.getCustomer().getId());
        }
    }


    private void blockAmount(Booking booking, VehicleType vehicleType) {

        Assert.notNull(booking.getEstimatedPaytmDeductable(), "Booking has a null entry for estimatedPaytmDeductable field");

        if (Objects.isNull(booking.getCustomer().getPaytmToken())) {
            throw new CustomException("Paytm not linked for the user", 602);
        }

        if (Objects.isNull(booking.getCustomer().getPaytmTokenExpiry()) || booking.getCustomer()
                .getPaytmTokenExpiry().before(new Date())) {
            throw new CustomException("Paytm token expired for the user", 601);
        }



        final double preAuthAmount = this.calculatePreAuthAmount(booking, vehicleType);

        final LockAmountResponse lockResponse = this.paytmService.preAuth(booking, preAuthAmount);
        updateBooking(booking, preAuthAmount, lockResponse);
    }

    @Transactional
    void updateBooking(Booking booking, double preAuthAmount, LockAmountResponse lockResponse) {
        booking.setPreAuthId(lockResponse.getPreauthId());
        booking.setPreAuthAmount(preAuthAmount);
        this.bookingRepository.save(booking);
    }

    private double calculatePreAuthAmount(Booking booking, VehicleType vehicleType) {
        double retVal;
        switch (vehicleType){
            case bike:
                retVal = calculatePreAuthAmountForBike(booking);
                break;
            case cycle:
                retVal = calculatePreAuthAmountForCycle(booking);
                break;

                default:
                    throw new IllegalArgumentException("Invalid vehicle type provided");
        }
        return retVal;
    }

    private double calculatePreAuthAmountForBike(Booking booking) {
//        final double currentBalance = paytmRepository.getBalance(booking.getCustomer().getPaytmToken(),
//                booking.getCustomer().getPaytmTokenExpiry(), booking.getCustomer().getId());
        final double minRequiredBalance = this.globalConfigService.getPaytmWalletMinBalance();
//        final double tripCostDeficit = BillingUtil.max(
//                BillingUtil.subtract(booking.getEstimatedPaytmDeductable(), currentBalance), 0);
//        boolean isBalanceLow = BillingUtil.compareTo(currentBalance, minRequiredBalance) < 0;

//        if(true) {
//            final double rechargeAmount = BillingUtil.max(BillingUtil.subtract(minRequiredBalance, currentBalance), tripCostDeficit);
//            if(BillingUtil.compareTo(tripCostDeficit, rechargeAmount) ==0 ) {
//                isBalanceLow = false;
//            }
//            this.paytmService.raisePaytmLowBalanceCustomException(rechargeAmount, currentBalance, isBalanceLow);
//        }
        return BillingUtil.max(minRequiredBalance, booking.getEstimatedPaytmDeductable());
    }

    private double calculatePreAuthAmountForCycle(Booking booking) {
        return this.globalConfigService.getCyclePaytmWalletMinBalance();
    }

    private boolean mustLockPaytm(Booking booking, String userToken) {
        if(booking.getPaymentMethod() == PaymentMethod.postpaid) {
            return this.postpaidService.canProcess(booking, userToken);
        } else if(booking.getPaymentMethod() == PaymentMethod.bounce_cash) {
            return this.bounceCashService.canProcess(booking, userToken);
        } else {
            return this.paytmService.canProcess(booking, userToken);
        }
    }
}
