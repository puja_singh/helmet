package com.bounce.impl;

import com.bounce.ClearDuesService;
import com.bounce.PaytmService;
import com.bounce.RazorpayService;
import com.bounce.cash.BounceCashApi;
import com.bounce.dtos.cash.BounceCashDeductionInternalData;
import com.bounce.dtos.cash.BounceCashDeductionResponse;
import com.bounce.dtos.cash.BounceCashGetRemittableAmount;
import com.bounce.entity.Customer;
import com.bounce.entity.DuesLedger;
import com.bounce.entity.PaytmTransaction;
import com.bounce.entity.User;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.exceptions.CustomException;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.CustomerRepository;
import com.bounce.repository.DuesLedgerRepository;
import com.bounce.repository.PaymentTrasactionLogRepository;
import com.bounce.repository.PaytmTransactionRepository;
import com.bounce.utils.BillingUtil;
import com.bounce.entity.PaymentTransactionLog;
import com.bounce.utils.LoggingUtil;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.Date;

@Slf4j
@Named
public class ClearDuesServiceImpl implements ClearDuesService {


    private String RAZORPAY = "razorpay";
    private long orderId = 2792;

    @Inject private CustomerRepository customerRepository;
    @Inject private DuesLedgerRepository duesLedgerRepository;
    @Inject private PaytmRepository paytmRepository;
    @Inject private PaytmTransactionRepository paytmTransactionRepository;
    @Inject private PaytmService paytmService;
    @Inject private BounceCashApi bounceCashApi;
    @Inject private RazorpayService razorpayService;
    @Inject private PaymentTrasactionLogRepository paymentTrasactionLogRepository;

    @Override
    public void clearDuesHelper(User user, Double amountInr, String paymentId, String method) throws Exception {
        Customer customer = this.customerRepository.findById(user.getId())
                .orElseThrow(()->new IllegalArgumentException("Invalid Customer"));
        this.viaBounceWallet(user, customer);
        double amount = customer.getTotalAmountDue();

        if(BillingUtil.compareTo(amount, 0) == 0){
            return;
        }
        if(!StringUtils.isEmpty(method) && RAZORPAY.equals(method)){
            this.viaRazorpay(amountInr, paymentId, customer);

        } else {
            this.viaPaytm(customer);
        }

    }

    private void viaPaytm(Customer customer) throws Exception {
        double duesAmount = customer.getTotalAmountDue();
        double paytmBalance = this.paytmService.getWalletBalance(customer);
        log.debug("paytm balance {}", paytmBalance);
        if (BillingUtil.compareTo(paytmBalance ,duesAmount) == -1){
            throw new CustomException(String.format("please add %s to paytm", BillingUtil.subtract(duesAmount, paytmBalance)), 400);
        }
        //todo withdraw amount from paytm.
        PaytmTransaction paytmTransaction = this.paytmRepository.withdrawForBooking(customer, orderId, duesAmount, "WITHDRAW", "dues");
        customer.setTotalAmountDue(0.0);
        customer.setDuesUpdatedOn(new Date());
        this.customerRepository.save(customer);
        logDuesLedger(customer.getId(), duesAmount, customer.getTotalAmountDue(),
                 "paytm", paytmTransaction.getId());

    }

    private void viaRazorpay(double amountInr, String paymentId, Customer customer) {

        if(StringUtils.isEmpty(paymentId) || amountInr == 0.0){
            throw new CustomException("amount or payment_id missing", 400);
        }

        clearDuesViaRazorpay(amountInr, paymentId, customer);
    }

    private void clearDuesViaRazorpay(double amountInr, String paymentId, Customer customer) {
        PaymentTransactionLog paymentTransactionLog = null;
        long orderId = 2792;
        String captureResponse = null;
        boolean result = false;
        try{
            //todo round amountInr
            double amountPaisa = BillingUtil.multiply(amountInr, 100);
            if(BillingUtil.compareTo(amountInr, customer.getTotalAmountDue()) != 0){
                throw new CustomException("Please try again later", 400);
            }
            paymentTransactionLog = LoggingUtil.insertIntoPaymentTransactionLog(paymentTrasactionLogRepository,
                    customer.getId(), orderId, amountInr, paymentId, PaymentMethod.razorpay.toString(), "dues", "CAPTURE");
            captureResponse = razorpayService.capture(paymentId, amountInr, customer.getId());
            result = true;
            customer.setTotalAmountDue(0.0);
            customer.setDuesUpdatedOn(new Date());
            this.customerRepository.save(customer);

        }catch (Exception e){
            log.info("error in capturing payment: {}", e.getMessage());
            captureResponse = e.getMessage();
            result = false;
        }
        log.info("capture response: {}",captureResponse);
        String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
        PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId,
                customer, "WITHDRAW", "dues", orderIdRand, paymentId, PaymentMethod.razorpay.toString(), amountInr, result, captureResponse);

        if(paytmTransaction == null){
            log.info("saving to transaction failed.");
        }
        if(paymentTransactionLog != null){
            paymentTransactionLog.setStatus(String.valueOf(result));
            this.paymentTrasactionLogRepository.save(paymentTransactionLog);
        }else{
            log.info("saving to payment transaction log failed.");
        }
        log.debug("verdict {}", paytmTransaction);
        if(!result){
            throw new CustomException("Failed to Capture", 400);
        } else {
            //todo check null pointer for paytmtransaction
            if(paytmTransaction != null) {
                logDuesLedger(customer.getId(), amountInr, customer.getTotalAmountDue(), PaymentMethod.razorpay.toString(), paytmTransaction.getId());
            }else{
                logDuesLedger(customer.getId(), amountInr, customer.getTotalAmountDue(), PaymentMethod.razorpay.toString(), null);
            }
        }
    }


    private void viaBounceWallet(User user, Customer customer){

        double dues = customer.getTotalAmountDue();
        double remmitableAmount = 0;
        String exceededLimit = null;
        log.info("dues is {}",dues);
        try {
            BounceCashGetRemittableAmount bounceCashGetRemittableAmount = bounceCashApi.getRemittableAmount(user.getMobileNumber(), "D");
            remmitableAmount = bounceCashGetRemittableAmount.getResult().getData().getRemittableAmount();
        } catch (Exception e){
            log.info("error in getting remittable amount", e);
        }
        double clearAmount = BillingUtil.min(dues, remmitableAmount);

        if(clearAmount > 0){
            log.info("clearAmount :{}", clearAmount);
            BounceCashDeductionResponse bounceCashDeductionResponse = bounceCashApi
                    .deductNonPromotionalBalance(user.getToken(), clearAmount);
            BounceCashDeductionInternalData bounceWallet = bounceCashDeductionResponse.getResult().getData()[0];
            long paymentRefId = bounceWallet.getWalletTxnId();

            customer.setTotalAmountDue(BillingUtil.subtract(dues, clearAmount)); //todo check this statement
            customer.setDuesUpdatedOn(new Date());
            customerRepository.save(customer);

            String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
            PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId,
                    customer, "WITHDRAW", "dues", orderIdRand, String.valueOf(paymentRefId), "wallet", clearAmount, true, String.valueOf(bounceCashDeductionResponse));

            logDuesLedger(user.getId(), clearAmount, customer.getTotalAmountDue(), "wallet", paytmTransaction.getId());
        }

    }


    @Transactional
    void logDuesLedger(long userId, double clearAmount, double totalAmountDue, String paymentMethod, Long paymentRefId){
        DuesLedger duesLedger = DuesLedger.builder()
                .userId(userId)
                .amount(clearAmount)
                .mode("Settle")
                .source("Clear Dues Flow")
                .paymentMethod(paymentMethod)
                .paymentRefId(paymentRefId)
                .totalDueAmount(totalAmountDue)
                .build();
        this.duesLedgerRepository.save(duesLedger);
    }

}
