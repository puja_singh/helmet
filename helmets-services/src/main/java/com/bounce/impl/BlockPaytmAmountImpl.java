package com.bounce.impl;

import com.bounce.entity.Booking;
import com.bounce.helper.GlobalConfigService;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.BookingRepository;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;

@Slf4j
@Named
public class BlockPaytmAmountImpl {

    @Inject private BookingRepository bookingRepository;
    @Inject private GlobalConfigService globalConfigHelper;
    @Inject private PaytmRepository paytmRepository;



    public void cycleBlockPaytm(long bookingId) {
        Booking booking = bookingRepository.findById(bookingId).orElse(null);
        if(booking != null) {

        } else {
            throw new RuntimeException(String.format("Booking %s not found", bookingId));
        }
    }

}
