package com.bounce.impl;

import com.bounce.PaytmService;
import com.bounce.RazorpayService;
import com.bounce.WalletTopupService;
import com.bounce.cash.BounceCashApi;
import com.bounce.dtos.cash.BounceCashGetRemittableAmount;
import com.bounce.entity.*;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.exceptions.CustomException;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.*;
import com.bounce.utils.BillingUtil;
import com.bounce.utils.LoggingUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@Slf4j
public class WalletTopupServiceImpl implements WalletTopupService {

    private final static String monthly = "monthly";
    private long orderId = 2794;

    @Inject private WalletTopupOffersRepository walletTopupOffersRepository;
    @Inject private RazorpayService razorpayService;
    @Inject private BounceCashApi bounceCashApi;
    @Inject private CustomerRepository customerRepository;
    @Inject private PaytmService paytmService;
    @Inject private PaytmRepository paytmRepository;
    @Inject private WalletTopupRepository walletTopupRepository;
    @Inject private PaytmTransactionRepository paytmTransactionRepository;
    @Inject private PaymentTrasactionLogRepository paymentTrasactionLogRepository;

    @Override
    public void walletTopupHelper(User user, double amountInr, String paymentId, long offerId, String paymentMethod) throws Exception {

        Customer customer = this.customerRepository.findById(user.getId())
                .orElseThrow(()->new IllegalArgumentException("Invalid Customer"));

        double walletTopupValue = getWalletTopupValue(amountInr, offerId);

        WalletTopup walletTopup = insertIntoWalletTopup(user.getId(), amountInr, offerId, paymentId, walletTopupValue);


        if(!StringUtils.isEmpty(paymentMethod) && PaymentMethod.paytm.toString().equals(paymentMethod)){
            this.viaPaytm(customer, amountInr);

        } else {
            this.captureWalletTopupPaymentRazorpay(amountInr, paymentId, customer);
        }

        String phpResponse = bounceCashApi.triggerWalletTopupApi(walletTopupValue, user.getMobileNumber(), paymentId, user.getId());
        log.info("php response: {}", phpResponse);

        updateWalletTopup(walletTopup, phpResponse);
    }

    @Override
    public double getWalletTopupValue(double amountInr, long offerId) {
        JSONObject offer = getActiveOffer(offerId);

        if(offer == null){
            throw new CustomException("Offer No More Active", 400);
        }
        double priceToPay;

        if(offer.has("discounted_price") && offer.getDouble("discounted_price") != 0){
            priceToPay = offer.getDouble("discounted_price");
        } else {
            priceToPay = offer.getDouble("price");
        }
        //todo check above statement.

        log.info("price to pay :{} , amountInr:{}", priceToPay, amountInr);
        if(BillingUtil.compareTo(amountInr, priceToPay) != 0){
            throw new CustomException("Attemp of Fraud", 400);
        }

        return offer.getDouble("wallet_value");
    }

    @Override
    public List<Map<String, Object>> getActiveOffers(User user) {
        try {
            BounceCashGetRemittableAmount bounceCashGetRemittableAmount = bounceCashApi.getRemittableAmount(user.getMobileNumber(), "C");
            double remittableAmount = bounceCashGetRemittableAmount.getResult().getData().getRemittableAmount();
            String exceededLimit = bounceCashGetRemittableAmount.getResult().getData().getExceededLimit();

            if (!StringUtils.isEmpty(exceededLimit)) {
                String duration = null;
                if (monthly.equals(exceededLimit)) {
                    duration = "month";
                } else {
                    duration = "year";
                }
                throw new CustomException("Sorry, your account has reached the maximum limit as per RBI guidelines.|You will be able to add Bounce Cash again next month.", 621);
            } else {
                WalletTopupOffers[] walletTopupOffers = this.walletTopupOffersRepository.findAllByActiveAndWalletValueLessThanEqualOrderByIdAsc(true, remittableAmount);
                log.info("offers : {}", walletTopupOffers);
                if (walletTopupOffers.length == 0) {
                    throw new CustomException("Sorry, your account has reached the maximum limit as per RBI guidelines.|You will be able to add Bounce Cash again next month.", 622);
                }

                List<Map<String, Object>> offers = new ArrayList<>();
                for(int i=0;i<walletTopupOffers.length; i++){
                    Map<String, Object> offer = new HashMap<>();
                    offer.put("id", walletTopupOffers[i].getId());
                    offer.put("price", walletTopupOffers[i].getPrice());
                    offer.put("wallet_value", walletTopupOffers[i].getWalletValue());
                    offer.put("image", walletTopupOffers[i].getImage());
                    offer.put("tag", walletTopupOffers[i].getTag());

                    if(walletTopupOffers[i].getDiscountedPrice() != null){
                        offer.put("discounted_price", walletTopupOffers[i].getDiscountedPrice());
                    }
                    offers.add(offer);
                }
                return offers;
            }
        } catch (Exception e) {
            log.info("error in getActiveOffers", e);
            throw new CustomException("Error finding the active offers");
        }
    }

    private JSONObject getActiveOffer(long offerId){
        WalletTopupOffers walletTopupOffers = this.walletTopupOffersRepository
                .findFirstByIdAndActiveOrderByIdDesc(offerId, true);

        JSONObject offer = new JSONObject();

        if(walletTopupOffers != null){


            offer.put("id", walletTopupOffers.getId());
            offer.put("price", walletTopupOffers.getPrice());
            offer.put("wallet_value", walletTopupOffers.getWalletValue());
            offer.put("image", walletTopupOffers.getImage());
            offer.put("tag", walletTopupOffers.getTag());

            if(walletTopupOffers.getDiscountedPrice() != null){
                offer.put("discounted_price", walletTopupOffers.getDiscountedPrice()); //todo round discounted price
            } else {
                offer.put("discounted_price", 0); //todo keep null value.
            }
        }

        return offer;
    }

    private boolean captureWalletTopupPaymentRazorpay(double amountInr, String paymentId, Customer customer){

        boolean result = false;
        PaymentTransactionLog paymentTransactionLog = null;
        String captureResponse = null;
        try{

            double amountPaisa = BillingUtil.multiply(amountInr, 100);

            //todo insert into payment transaction log
            paymentTransactionLog = LoggingUtil.insertIntoPaymentTransactionLog(paymentTrasactionLogRepository,
                    customer.getId(), orderId, amountInr, paymentId, PaymentMethod.razorpay.toString(), "wallet_topup", "CAPTURE");
            captureResponse = razorpayService.capture(paymentId, amountInr, customer.getId());
            result = true;

        } catch (Exception e){
            JSONObject response = new JSONObject();
            response.put("response", e.getMessage());
            captureResponse = String.valueOf(response);
        }

        String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());

        try {
            PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId,
                    customer, "DEPOSIT", "wallet_topup", orderIdRand, paymentId, PaymentMethod.razorpay.toString(), amountInr, result, captureResponse);
        } catch (Exception e){
            log.info("saving to transaction failed.");
        }

        if(paymentTransactionLog != null){
            paymentTransactionLog.setStatus(String.valueOf(result));
            this.paymentTrasactionLogRepository.save(paymentTransactionLog);
        }else{
            log.info("saving to payment transaction log failed.");
        }

        if(!result){
            throw new CustomException("Failed to Capture", 401);
        }
        return result;
    }


    private void viaPaytm(Customer customer, double amountInr) throws Exception {
        double paytmBalance = this.paytmService.getWalletBalance(customer);
        log.debug("paytm balance {}", paytmBalance);
        if (BillingUtil.compareTo(paytmBalance ,amountInr) == -1){
            throw new CustomException(String.format("please add %s to paytm", BillingUtil.subtract(amountInr, paytmBalance)), 400);
        }
        //todo withdraw amount from paytm.
        this.paytmRepository.withdrawForBooking(customer, orderId, amountInr, "DEPOSIT", "wallet_topup");

    }


    @Transactional
    public WalletTopup insertIntoWalletTopup(Long userId, double amountInr, Long offerId, String paymentId, double walletTopupValue){

        WalletTopup walletTopup = WalletTopup.builder()
                .userId(userId)
                .amountInr(amountInr)
                .offerId(offerId)
                .paymentId(paymentId)
                .walletValue(walletTopupValue)
                .build();
        this.walletTopupRepository.save(walletTopup);
        return walletTopup;
    }

    @Transactional
    public void updateWalletTopup(WalletTopup walletTopup, String phpResponse){

        walletTopup.setPhpResponse(phpResponse);
        this.walletTopupRepository.save(walletTopup);

    }

}
