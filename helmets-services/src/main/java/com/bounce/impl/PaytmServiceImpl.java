package com.bounce.impl;

import com.bounce.PaytmService;
import com.bounce.dtos.enums.VehicleType;
import com.bounce.dtos.paytm.LockAmountResponse;
import com.bounce.dtos.paytm.PaytmWalletDetails;
import com.bounce.entity.Booking;
import com.bounce.entity.Customer;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.exceptions.CustomException;
import com.bounce.helper.GlobalConfigService;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.BookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Named
public class PaytmServiceImpl implements PaytmService {

    private static final String LINKED = "linked";
    private static final String NOT_LINKED = "not_linked";
    private static final String REFUND_INITIATED = "refund_initiated";
    private static final String POSTPAID = "postpaid";

    @Inject private PaytmRepository paytmRepository;
    @Inject private BlockPaytmAmountImpl blockPaytmAmountService;
    @Inject private BookingRepository bookingRepository;
    @Inject private GlobalConfigService globalConfigService;

    @Override
    public boolean isTokenValidFor(Customer customer) {
        return !StringUtils.isEmpty(customer.getPaytmToken()) && customer.getPaytmTokenExpiry().after(new Date());
    }

    @Override
    public double getWalletBalance(Customer customer) {
        return this.paytmRepository.getBalance(customer.getPaytmToken(), customer.getPaytmTokenExpiry(), customer.getId());
    }

    @Override
    public PaytmWalletDetails getWalletDetails(Customer customer) {
        Assert.notNull(customer, "Invalid customer found");

        final PaytmWalletDetails retVal = PaytmWalletDetails.builder()
                .customer(customer)
                .paymentMethod(PaymentMethod.paytm)
                .build();

        if (!isTokenValidFor(customer)) {
            log.info("[PaytmWalletDetails]: Invalid token for customer {}", customer.toString());
            retVal.setIsTokenValid(Boolean.FALSE);
        }
        else {
            log.info("[PaytmWalletDetails]: Token valid for customer {}", customer.toString());
            retVal.setIsTokenValid(Boolean.TRUE);
        }

        return retVal;
    }

    public LockAmountResponse preAuth(Booking booking, double amount) {
        return this.paytmRepository.lockAmount(booking.getId(), booking.getCustomer().getPaytmToken(), amount, booking.getCustomer().getId());
    }

    public void raisePaytmLowBalanceCustomException(double addMoney, double currentBalance, boolean isBalanceLow)
            throws CustomException{
        String reason;

        if(isBalanceLow) {
            reason = String.format("A minimum balance of \u20B9%s is required to make this booking",
                    this.globalConfigService.getPaytmWalletMinBalance());
        }
        else {
            reason = "You do not have sufficient balance for this booking";
        }
        String message = String.format("{ \"add_money\": %s, \"balance\": %s, \"reason\": %s }", addMoney,
                currentBalance, reason);
        throw new CustomException (message, 607);
    }

    @Override
    public boolean canProcess(Booking booking, String userToken) {
        return this.globalConfigService.isPaytmMandatoryInBookBike();
    }
}
