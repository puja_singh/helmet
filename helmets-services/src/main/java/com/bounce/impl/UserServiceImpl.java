package com.bounce.impl;

import com.bounce.PaytmService;
import com.bounce.UserService;
import com.bounce.entity.Booking;
import com.bounce.entity.Customer;
import com.bounce.repository.CustomerRepository;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UserServiceImpl implements UserService {
    @Inject PaytmService paytmService;
    @Inject CustomerRepository customerRepository;

    @Override
    public double getPaytmWalletBalance(long customerId) {
        final Customer customer = this.customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("No Customer found for the given id"));
        return this.paytmService.getWalletBalance(customer);
    }
}
