package com.bounce.impl;

import com.bounce.cash.BounceCashApi;
import com.bounce.dtos.cash.BounceCashDeductionInternalData;
import com.bounce.dtos.cash.BounceCashDeductionResponse;
import com.bounce.entity.Invoice;
import com.bounce.entity.Booking;
import com.bounce.entity.enums.DistanceType;
import com.bounce.entity.json.Pricing;
import com.bounce.exceptions.CustomException;
import com.bounce.helper.GlobalConfigService;
import com.bounce.models.ActualCostLabelValue;
import com.bounce.repository.InvoiceRepository;
import com.bounce.repository.BookingRepository;
import com.bounce.repository.UserRepository;
import com.bounce.utils.BillingUtil;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class ActualBillingImpl {

    @Inject private BookingRepository bookingRepository;
    @Inject private UserRepository userRepository;
    @Inject private InvoiceRepository invoiceRepository;
    @Inject private GlobalConfigService globalConfigHelper;
    @Inject private BounceCashApi bounceCashApi;

    public void generateBilling(long bookingId, double distance, double time) throws CustomException, Exception {
        Booking booking = bookingRepository.findById(bookingId).get();
        if(booking == null) {
            throw new Exception("Booking not found");
        }

        Pricing pricing = new Pricing(); //booking.getPricing();
        double roundedDistance = BillingUtil.roundDistance(distance);
        double distanceCost = BillingUtil.multiply(roundedDistance, pricing.getPricePerKm());

        long roundedTime = BillingUtil.roundTime(time);
        double durationCost = BillingUtil.multiply(roundedTime, pricing.getPricePerMinute());

        double oldCost = BillingUtil.add(distanceCost, durationCost);
        double cost = oldCost;

        if(distance <= 500) {
            if(BillingUtil.isBillingAmnestyApplicable(distance, oldCost)) {
                cost = 0;
                //TODO move_bike_to_oos_amnesty_conditional(booking, False)
            } else {
                cost = oldCost;
                //TODO move_bike_to_oos_amnesty_conditional(booking, True)
            }
        } else {
            double a2bMinBaseFare = globalConfigHelper.getA2bMinBaseFare();
            cost = Math.max(oldCost, a2bMinBaseFare);
            //TODO move_bike_to_oos_amnesty_conditional(booking, True)
        }

        booking.setActualCost(cost);
        booking.setActualTime(time);
        booking.setActualDistance(distance);
        booking.setDistanceType(DistanceType.odometer);

        if(booking.getActualEndPointLat() != null && booking.getActualEndPointLon() != null) {
            log.info("getting end address by google maps API for booking id {}", (booking.getId()));
            //TODO: fix this using googlemap api
            //booking.setActualEndAddress();
        }
        bookingRepository.save(booking);
        log.info("saved cost of the trip into booking object");

        log.info("Fetching bounceCash api");
        long customerId = booking.getCustomer().getId();
        String userToken = userRepository.findById(customerId).get().getToken();
        BounceCashDeductionResponse bounceCashDeductionResponse = bounceCashApi.getActualDeduction(userToken, cost, bookingId);
        BounceCashDeductionInternalData bounceWallet = bounceCashDeductionResponse.getResult().getData()[0];
        double nonPromotionCreditUsed = bounceWallet.getNonPromotionalCreditUsed();
        double promotionalCreditUsed = bounceWallet.getPromotionalCreditUsed();
        double taxAmount = bounceWallet.getTotalTaxAmount();
        double actualPaytmDeductable = bounceWallet.getPriceWithTax();

        double totalAmount = BillingUtil.add(cost, taxAmount);
        double subTotal = BillingUtil.subtract(cost, promotionalCreditUsed);

        ActualCostLabelValue[] costDetails = populateActualCostDetails(cost, promotionalCreditUsed, subTotal,
                        taxAmount, nonPromotionCreditUsed, actualPaytmDeductable);

        booking.setWalletTxnId(bounceWallet.getWalletTxnId());
        booking.setActualRewarPointsUsed(promotionalCreditUsed);
        booking.setActualBounceCashUsed(nonPromotionCreditUsed);
        booking.setActualTax(taxAmount);
        booking.setActualPaytmDeductable(actualPaytmDeductable);
        booking.setActualTotalAmount(totalAmount);
        bookingRepository.save(booking);

        try {
            // TODO  act_data = str({"act_promo_used": booking.actual_promo_used, "act_non_promo_used": booking.actual_non_promo_used, ' \
            //                                         '"act_tax": booking.actual_tax,"amount_due":booking.amount_due})
            //            log_analytics(kingdom=Kingdom.MB_PYTHON.value, phylum=Phylum.BOOKING.value, fam='trip_payment_actual', ord='success',
            //                          cls=booking.user_id,
            //                          num_genus=booking.id, ts=timelib.time() * 1000, num1=booking.wallet_txn_id,
            //                          num2=booking.actual_paytm_deductable,num3=booking.actual_time,str1=act_data,str2=booking.actual_distance,
            //                          str3=booking.actual_cost)
        } catch (Exception e) {
            log.info("error occured at bill logging {}", e.getMessage());
        }

        generateInvoice(bookingId);

        //TODO        bookingWithdrawLogic(Booking booking, double actualPaytmDeductable, )


    }

    private String generateInvoice(long bookingId) {
        Invoice invoice = Invoice.builder().build();
        invoice.setBookingId(bookingId);
        invoice.setUuid(Generators.timeBasedGenerator().toString());
        invoice.setCreatedAt(new Date());
        invoice.setUpdatedAt(new Date());
        invoiceRepository.save(invoice);
        return invoice.getUuid();
    }

    private ActualCostLabelValue[] populateActualCostDetails(double cost, double promotionalCreditUsed, double subTotal,
                                                             double taxAmount, double nonPromotionCreditUsed,
                                                             double actualPaytmDeductable) {
        List<ActualCostLabelValue> list = new ArrayList<>();
        list.add(new ActualCostLabelValue("Base Price", cost));
        list.add(new ActualCostLabelValue("Reward Points Discount", promotionalCreditUsed));
        list.add(new ActualCostLabelValue("Sub Total", subTotal));
        list.add(new ActualCostLabelValue("Tax", taxAmount));
        list.add(new ActualCostLabelValue("Paid via Bounce Wallet", nonPromotionCreditUsed));
        list.add(new ActualCostLabelValue("Paid via Paytm Wallet", actualPaytmDeductable));
        list.add(new ActualCostLabelValue("Net Amount", BillingUtil.add(subTotal, taxAmount)));

        ActualCostLabelValue[] costDetails = new ActualCostLabelValue[list.size()];
        return list.toArray(costDetails);
    }

}
