package com.bounce.impl;

import com.bounce.PaidHelmetService;
import com.bounce.PaytmService;
import com.bounce.RazorpayService;
import com.bounce.dtos.AnalyticsDumpDto;
import com.bounce.dtos.request.PaidHelmetTrackingDto;
import com.bounce.entity.*;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.entity.enums.PostpaidStatus;
import com.bounce.exceptions.CustomException;
import com.bounce.helper.GlobalConfigService;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.*;
import com.bounce.utils.BillingUtil;
import com.bounce.utils.LoggingUtil;
import com.bounce.utils.TrackingDumpUtil;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.*;

@Named
@Slf4j
public class PaidHelmetServiceImpl implements PaidHelmetService {

    private long orderId = 2795;

    @Inject private PaidHelmetRepository paidHelmetRepository;
    @Inject private HelmetRepository helmetRepository;
    @Inject private HelmetTrackingRepository helmetTrackingRepository;
    @Inject private GlobalConfigService globalConfigService;
    @Inject private CustomerRepository customerRepository;
    @Inject private PaytmTransactionRepository paytmTransactionRepository;
    @Inject private PaytmService paytmService;
    @Inject private PaytmRepository paytmRepository;
    @Inject private RazorpayService razorpayService;
    @Inject private PaymentTrasactionLogRepository paymentTrasactionLogRepository;

    @Override
    public void HelmetPostpaidPayment(User user, double amountInr, String paymentId, String address, Integer pincode, String method) throws Exception {
        Customer customer = this.customerRepository.findById(user.getId())
                .orElseThrow(()->new IllegalArgumentException("Invalid Customer"));

        if (globalConfigService.stopPaidHelmetCapture()) {
            throw new CustomException("Sorry, there's high demand for these helmets. We are working hard to get new helmets. Please check again soon. If money has been deducted it will be refunded within few working days.", 200);
        }

        Helmet helmet = this.helmetRepository.findByUserId(user.getId());
        if (helmet != null) {
            if (PostpaidStatus.linked.equals(helmet.getStatus())) {
                throw new CustomException("You already have a helmet");
            } else if (PostpaidStatus.refund_initiated.equals(helmet.getStatus())) {
                throw new CustomException("Refund is in progress");
            } else {
                helmet.setAmount(amountInr);
                helmet.setAddress(address);
                helmet.setPincode(pincode);
                this.helmetRepository.save(helmet);
            }
        } else {
            helmet = insertIntoHelmet(customer.getId(), amountInr, pincode, address, "not_linked");
        }

        if(!StringUtils.isEmpty(method) && PaymentMethod.paytm.toString().equals(method)){
            this.captureHelmetPaytm(customer, amountInr);
        } else {
            this.captureHelmetRazorpay(customer, amountInr, paymentId, address, pincode);
        }

        if(BillingUtil.compareTo(globalConfigService.getHelmetPostpaidDepositAmount(), amountInr) >= 0) {
            helmet.setStatus("linked");
            helmet.setOrderDate(new Date());
            this.helmetRepository.save(helmet);
            paidHelmetAnalyticsDump(user, amountInr, customer, method, PostpaidStatus.linked.toString());
        }

    }

    private void paidHelmetAnalyticsDump(User user, double amountInr, Customer customer, String method, String status) {
        try {
            AnalyticsDumpDto analyticsDumpDto = AnalyticsDumpDto.builder()
                    .kingdom("mb_python")
                    .phylum("deposit_payments")
                    .family("helmet_deposit")
                    .genusNum(Double.valueOf(user.getId()))
                    .species(method)
                    .num1(amountInr)
                    .str1(status)
                    .str2(String.valueOf(customer.getPaytmTokenExpiry()))
                    .str3(customer.getPaytmToken())
                    .build();
            TrackingDumpUtil.dumpIntoAnimalKingdom(analyticsDumpDto);
        } catch (Exception e){
            log.info("error in inserting into animal kingdom in pai helmet");
        }
    }

    private void captureHelmetPaytm(Customer customer, double amountInr) throws Exception {
        double paytmBalance = this.paytmService.getWalletBalance(customer);
        log.debug("paytm balance {}", paytmBalance);
        if (BillingUtil.compareTo(paytmBalance ,amountInr) == -1){
            throw new CustomException(String.format("please add %s to paytm", BillingUtil.subtract(amountInr, paytmBalance)), 400);
        }
        //todo withdraw amount from paytm.
        this.paytmRepository.withdrawForBooking(customer, orderId, amountInr, "DEPOSIT", "postpaid_helmet");
    }

    private void captureHelmetRazorpay(Customer customer, double amountInr, String paymentId, String address, Integer pincode){

        boolean result = false;
        String captureResponse = null;
        PaymentTransactionLog paymentTransactionLog = null;
        try {

            paymentTransactionLog = LoggingUtil.insertIntoPaymentTransactionLog(paymentTrasactionLogRepository,
                    customer.getId(), orderId, amountInr, paymentId, PaymentMethod.razorpay.toString(), "postpaid_helmet", "CAPTURE");

            captureResponse = razorpayService.capture(paymentId, amountInr, customer.getId());
            result = true;


        } catch (Exception e){
            log.info("error in capturing payment: {}", e.getMessage());
            captureResponse = e.getMessage();
            result = false;
        }
        String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
        //todo update paymentTransactionLog
        if(paymentTransactionLog != null){
            paymentTransactionLog.setStatus(String.valueOf(result));
            this.paymentTrasactionLogRepository.save(paymentTransactionLog);
        }else{
            log.info("saving to payment transaction log failed.");
        }

        PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId, customer,
                "DEPOSIT", "postpaid_helmet", orderIdRand, paymentId, PaymentMethod.razorpay.toString(), amountInr, result, captureResponse);
        if(paytmTransaction == null){
            log.info("saving to transaction failed.");
        }

        if(!result){
            throw new CustomException("Failed to Capture");
        }
    }

    public boolean refundValidity (Helmet helmet) {
        Date orderDate = helmet.getOrderDate();
        Date todayDate = new Date();
        Calendar c1 = DateUtils.toCalendar(todayDate);
        Calendar c2 = DateUtils.toCalendar(orderDate);
        c2.add(Calendar.MONTH, 3);
        return !c1.before(c2);
    }

    public Map<Object, Object> getBuyDetails() {
        Map<Object, Object> buyDetails = new HashMap<>();
        buyDetails.put("price", globalConfigService.getHelmetPostpaidDepositAmount());
        buyDetails.put("price_text", "");
        return buyDetails;
    }

    public Map<Object, Object> getReturnDetails(Helmet helmet) {
        Map<Object, Object> returnDetails = new HashMap<>();
        returnDetails.put("order_id", "Order Id: " + helmet.getOrderId());
        returnDetails.put("delivered_on", "Delivered on " + helmet.getOrderDate());
        returnDetails.put("amount", "Amount paid " + helmet.getAmount());
        returnDetails.put("text", refundValidity(helmet)? "Return Helmet" : "The item is no longer eligible for return.");
        return returnDetails;
    }

    public List<Object> setLandingPageText(boolean buy) {
        List<Object> landingPageDetails = new ArrayList<>();
        Map<Object, Object> textDetails = new HashMap<>();
        if(buy) {
            textDetails.put("heading", "Get ISI marked personal helmet");
            textDetails.put("text", "at security deposit of Rs. " + globalConfigService.getHelmetPostpaidDepositAmount() +" delivered to your doorstep.");
            textDetails.put("terms", null);
            landingPageDetails.add(textDetails);

            textDetails.replace("heading", "Delivered in 2-4 days");
            textDetails.replace("text", null);
            textDetails.replace("terms", null);
            landingPageDetails.add(textDetails);

            textDetails.replace("heading", "Easy returns");
            textDetails.replace("text", "Helmets found in damaged conditions not eligible for return");
            textDetails.replace("terms", null);
            landingPageDetails.add(textDetails);

            textDetails.replace("heading", "Size");
            textDetails.replace("text", "One size");
            textDetails.replace("terms", null);
            landingPageDetails.add(textDetails);

            textDetails.replace("heading", "Size");
            textDetails.replace("text", "One size");
            textDetails.replace("terms", null);
            landingPageDetails.add(textDetails);

            textDetails.replace("heading", "Product Details");
            textDetails.replace("text", "Pay deposit to get a Bounce helmet \n" +
                    "Use it like it’s your own! \n" +
                    "Get the full deposit when you return it \n" +
                    "Delivered only in Bengaluru \n");
            textDetails.replace("terms", null);
            landingPageDetails.add(textDetails);

            textDetails.replace("heading", "Easy Returns");
            textDetails.replace("text", "Return the helmet in good consition within 3 months and get your deposit back");
            textDetails.replace("terms", "T&C");
            landingPageDetails.add(textDetails);

        }
        else {
            //add data for text details
            textDetails.put("heading", "Have a safe journey!");
            textDetails.put("text", null);
            textDetails.put("terms", null);
            landingPageDetails.add(textDetails);
        }
        return landingPageDetails;
    }

    @Override
    public Map<String, Object> getPostpaidHelmetStatus(User user){

        Helmet helmet = this.helmetRepository.findByUserId(user.getId());

        if(globalConfigService.stopNewHelmetFeature()) {
            if((helmet == null || helmet.getStatus() == null
                    || "not_linked".equals(helmet.getStatus()))){
                throw new CustomException("Sorry, there's high demand for these helmets. We are working hard to get new helmets. Please check again soon.", 805);

            }
        }

        Map<String, Object> status = new HashMap<>();
        List<Object> buyHelmetText = null;
        if(helmet != null){
            if ("not_linked".equals(helmet.getStatus())) {
                Map<Object, Object> buyDetails = getBuyDetails();
                status.put("buy_info", buyDetails);
                buyHelmetText = setLandingPageText(true);
            }
            else if ("linked".equals(helmet.getStatus())){
                Map<Object, Object> returnDetails = getReturnDetails(helmet);
                status.put("return_info", returnDetails);
                buyHelmetText = setLandingPageText(false);
            }
            status.put("type", (helmet.getStatus()!= null)? helmet.getStatus(): "not_linked");

        } else {
            Map<Object, Object> buyDetails = getBuyDetails();
            status.put("buy_info", buyDetails);
            buyHelmetText = setLandingPageText(true);
            status.put("type", "not_linked");
        }
        status.put("section", buyHelmetText);
        status.put("amount", globalConfigService.getHelmetPostpaidDepositAmount());
        status.put("images", "list of images");

        log.info("snackbar data: " + status);

        return status;
    }

    @Override
    public void helmetPostpaidRefund(User user){

        Customer customer = this.customerRepository.findById(user.getId())
                .orElseThrow(()-> new IllegalArgumentException("Invalid Customer"));

        if(BillingUtil.compareTo(customer.getTotalAmountDue(),0) != 0){
            Map<String, Object> data = new HashMap<>();
            data.put("dues_amount", customer.getTotalAmountDue());
            throw new CustomException("Please clear your dues to withdraw the deposit", 803,  data);
        }

        Helmet helmet = this.helmetRepository.findByUserId(user.getId());

        if(helmet ==  null){
            throw new CustomException("User never took helmet", 801);
        } else if("linked".equals(helmet.getStatus())){

            helmet.setStatus("refund_initiated");
            helmet.setRefundRequestDate(new Date());
            this.helmetRepository.save(helmet);

            paidHelmetAnalyticsDump(user, helmet.getAmount(), customer, "", PostpaidStatus.refund_initiated.toString());

        } else if(PostpaidStatus.refund_initiated.equals(helmet.getStatus())){
            throw new CustomException("Refund is already in progress", 802);

        }else{
            throw new CustomException("User never took helmet", 801);
        }
    }

    @Transactional
    public Helmet insertIntoHelmet(long userId, double amountInr, Integer pincode, String address, String status){

        Helmet helmet = Helmet.builder()
                .userId(userId)
                .amount(amountInr)
                .address(address)
                .pincode(pincode)
                .status(status)
                .orderDate(new Date())
                .build();
        this.helmetRepository.save(helmet);
        return helmet;
    }


    @Override
    public Map<String, Object> getReturnDetails(User user) {

        Map<String, Object> returnDetails = new HashMap<>();
        Helmet helmet = this.helmetRepository.findByUserId(user.getId());
        if (helmet != null) {
            returnDetails.put("amount", helmet.getAmount());
            returnDetails.put("address", helmet.getAddress());
            returnDetails.put("contact_number", user.getMobileNumber());

            Map<String, Object> returnSection = new HashMap<>();
            returnSection.put("title", "I agree to return all the items in original condition");
            returnSection.put("info", "Helmet will go through a thorough quality check & refund will be initiated post that.");
            returnDetails.put("return_info", returnSection);

            returnDetails.put("button_text", "Confirm your Return");

            Date orderDate = helmet.getOrderDate();
            Calendar c1 = DateUtils.toCalendar(orderDate);
            c1.add(Calendar.MONTH, 3);

            returnDetails.put("return_period", "Return by " + c1.toString());

            log.info("return details: " + returnDetails);
            return returnDetails;
        }
        else {
            throw new CustomException("User never took helmet", 801);
        }
    }


    @Override
    public Map<String, Object> getTrackingDetails(User user) {

        Map<String, Object> trackingDetails = new HashMap<>();
        Map<Object, Object> trackingData = new HashMap<>();
        Helmet helmet = this.helmetRepository.findByUserId(user.getId());
        List<HelmetTracking> helmetTrackingList = this.helmetTrackingRepository.findByHelmetId(helmet.getId());

        for (HelmetTracking helmetTracking: helmetTrackingList) {
            trackingData.put(helmetTracking.getStatus(), helmetTracking.getStatusUpdateTime());
        }

        trackingData.put("awb_number", helmet.getAwbNumber());
        trackingData.put("order_id", helmet.getOrderId());

        if (helmet != null) {
            String status = null;
            if ("not_linked".equals(helmet.getStatus())){
                status = "buy";
            }
            if ("refund_initiated".equals(helmet.getStatus())){
                status = "return";
            }
            trackingDetails.put("type", status);
            trackingDetails.put("order_id", "id");
            trackingDetails.put("tracking data", trackingData);
            return trackingDetails;
        }
        else {
            throw new CustomException("User never took helmet", 801);
        }
    }


    @Override
    public void setTrackingDetails(PaidHelmetTrackingDto paidHelmetTrackingDto) {
        Helmet helmet = this.helmetRepository.findByUserId(paidHelmetTrackingDto.getUserId());

        helmet.setAwbNumber(paidHelmetTrackingDto.getAwbNumber());
        helmet.setStatus(paidHelmetTrackingDto.getStatus());
        this.helmetRepository.save(helmet);

    }
}
