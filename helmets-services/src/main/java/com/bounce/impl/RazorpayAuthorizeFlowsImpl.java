package com.bounce.impl;


import com.bounce.RazorpayAuthorizeFlowsService;
import com.bounce.RazorpayService;
import com.bounce.cash.BounceCashApi;
import com.bounce.entity.*;
import com.bounce.mbpython.MbPythonApi;
import com.bounce.repository.*;
import com.bounce.utils.BillingUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.transaction.Transactional;

@Slf4j
@Component
public class RazorpayAuthorizeFlowsImpl implements RazorpayAuthorizeFlowsService {


    @Inject private MbPythonApi mbPythonApi;
    @Inject private CustomerRepository customerRepository;
    @Inject private PaidHelmetRepository paidHelmetRepository;
    @Inject private WalletTopupOffersRepository walletTopupOffersRepository;
    @Inject private RazorpayWebhooksRepository razorpayWebhooksRepository;
    @Inject private BounceCashApi bounceCashApi;
    @Inject private RazorpayService razorpayService;

    private static final String AUTHORIZED = "authorized";
    private static final String CAPTURED = "captured";
    private static final String CLEAR_DUES = "CLEAR_DUES";

    public void clearDues(Customer customer, String userToken, String paymentId, double amountInr, String mobileNumber, RazorpayWebhooks razorpayWebhooks) throws Exception {
        double dueAmount = customer.getTotalAmountDue();
        try {
            mbPythonApi.clearDues(userToken, amountInr, paymentId);
            this.updateRazorpayWebhooksClearDues(razorpayWebhooks, CAPTURED);
        } catch (Exception e) {
            String currentTxnStatus = razorpayService.statusCheck(paymentId);
            if(AUTHORIZED.equals(currentTxnStatus)) {
                captureAndReleaseDubiousPayments(customer, paymentId, amountInr, CLEAR_DUES, mobileNumber, razorpayWebhooks);
            } else { // already captured
                customer = this.customerRepository
                        .findById(customer.getId()).orElseThrow(() -> new IllegalArgumentException("Invalid customer object passed"));
                double latestDueAmount = customer.getTotalAmountDue();
                if(BillingUtil.compareTo(dueAmount, latestDueAmount) == 0) {
                    String refund = razorpayService.issueFullRefund(paymentId, amountInr, customer.getId());
                    JSONObject refundObject = new JSONObject(refund);
                    String refundId = BillingUtil.getValue(refundObject,"id");
                    this.updateRazorpayWebhooks(razorpayWebhooks, CAPTURED, refundId);
                }
            }
        }
    }

    public void captureAndReleaseDubiousPayments(Customer customer, String paymentId, double amountInr, String callType, String mobileNo, RazorpayWebhooks razorpayWebhooks) throws Exception {
        // capture and refund.
        String capture = razorpayService.capture(paymentId, amountInr, customer.getId());
        JSONObject captureObject  = new JSONObject(capture);
        String captureStatus = BillingUtil.getValue(captureObject, "status");

        String refund = razorpayService.issueFullRefund(paymentId, amountInr, customer.getId());
        JSONObject refundObject = new JSONObject(refund);
        String refundId = BillingUtil.getValue(refundObject, "id");
        this.updateRazorpayWebhooks(razorpayWebhooks, captureStatus, refundId);
    }

    @Transactional
    public RazorpayWebhooks insertIntoRazorpayWebhooks(RazorpayWebhooks razorpayWebhooks) {
        return razorpayWebhooksRepository.save(razorpayWebhooks);
    }

    @Transactional
    public void updateRazorpayWebhooks(RazorpayWebhooks razorpayWebhooks, String captureStatus, String refundId){
        razorpayWebhooks.setCaptureStatus(captureStatus);
        razorpayWebhooks.setRefundId(refundId);
        razorpayWebhooksRepository.save(razorpayWebhooks);
    }

    @Transactional
    public void updateRazorpayWebhooksClearDues(RazorpayWebhooks razorpayWebhooks, String captureStatus){
        razorpayWebhooks.setCaptureStatus(captureStatus);
        razorpayWebhooksRepository.save(razorpayWebhooks);
    }
}