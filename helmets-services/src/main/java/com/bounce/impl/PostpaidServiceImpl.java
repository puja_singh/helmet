package com.bounce.impl;

import com.bounce.JuspayService;
import com.bounce.PaytmService;
import com.bounce.PostpaidService;
import com.bounce.RazorpayService;
import com.bounce.dtos.AnalyticsDumpDto;
import com.bounce.entity.*;
import com.bounce.entity.enums.BookingStatus;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.entity.enums.PostpaidStatus;
import com.bounce.exceptions.CustomException;
import com.bounce.helper.GlobalConfigService;
import com.bounce.paytm.PaytmRepository;
import com.bounce.repository.*;
import com.bounce.utils.BillingUtil;
import com.bounce.utils.Log;
import com.bounce.utils.LoggingUtil;
import com.bounce.utils.TrackingDumpUtil;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Named
@Slf4j
public class PostpaidServiceImpl implements PostpaidService {

    private String RAZORPAY = "razorpay";
    private long orderId = 2793;

    @Inject private RazorpayService razorpayService;
    @Inject private GlobalConfigService globalConfigService;
    @Inject private CustomerRepository customerRepository;
    @Inject private PaytmTransactionRepository paytmTransactionRepository;
    @Inject private PaymentTrasactionLogRepository paymentTrasactionLogRepository;
    @Inject private BookingRepository bookingRepository;
    @Inject private PaytmRepository paytmRepository;
    @Inject private PaytmService paytmService;
    @Inject private JuspayService juspayService;
    @Inject private PostpaidRefundsRepository postpaidRefundsRepository;

    @Override
    public boolean canProcess(Booking booking, String userToken) {
        return !(Objects.equals(PaymentMethod.postpaid, booking.getPaymentMethod()) && isPostpaidActive(booking.getCustomer()));
    }

    private boolean isPostpaidActive(Customer customer) {
        return Objects.equals(PostpaidStatus.linked, customer.getPostpaidStatus());
    }

    @Override
    public void PostpaidPayment(long userId , double amountInr, String paymentId, String paymentMethod) throws Exception {

        Customer customer = this.customerRepository.findById(userId)
                .orElseThrow(()->new IllegalArgumentException("No customer with given id"));

        if(!StringUtils.isEmpty(paymentMethod) && PaymentMethod.paytm.toString().equals(paymentMethod)){
            this.viaPaytm(customer, amountInr);

        } else {
            this.captureRazorpayPostpaidPayment(userId, customer, amountInr, paymentId);
        }
    }

    private void viaPaytm(Customer customer, double amountInr) throws Exception {
        double paytmBalance = this.paytmService.getWalletBalance(customer);
        log.debug("paytm balance {}", paytmBalance);
        if (BillingUtil.compareTo(paytmBalance ,amountInr) == -1){
            throw new CustomException(String.format("please add %s to paytm", BillingUtil.subtract(amountInr, paytmBalance)), 400);
        }
        //todo withdraw amount from paytm.
        this.paytmRepository.withdrawForBooking(customer, orderId, amountInr, "DEPOSIT", "postpaid");
        double postpaidDepositAmount = amountInr;

        if (postpaidDepositAmount >= globalConfigService.getPostpaidSecurityAmount()){
            customer.setPostpaidDepositAmount(postpaidDepositAmount);
            customer.setPostpaidStatus(PostpaidStatus.linked);
            log.info("postpaidDepositAmount: {} in",postpaidDepositAmount);
            this.customerRepository.save(customer);
            postpaidAnalyticsDump(customer.getId(), customer, PostpaidStatus.linked.toString(), PaymentMethod.paytm.toString());
        }
    }


    @Override
    public void captureRazorpayPostpaidPayment(long userId, Customer customer, double amountInr, String paymentId) {
        PaymentTransactionLog paymentTransactionLog = null;
        String captureResponse = null;
        boolean result = false;
        try{
            //todo round amountInr
            double amountPaisa = BillingUtil.multiply(amountInr, 100);
            paymentTransactionLog = LoggingUtil.insertIntoPaymentTransactionLog(paymentTrasactionLogRepository,
                    userId, orderId, amountInr, paymentId, PaymentMethod.razorpay.toString(), "postpaid", "CAPTURE");
            captureResponse = razorpayService.capture(paymentId, amountInr, customer.getId());
            result = true;
            //todo change rounding logic
            double postpaidDepositAmount = amountInr;
            if (postpaidDepositAmount >= globalConfigService.getPostpaidSecurityAmount()){
                customer.setPostpaidDepositAmount(postpaidDepositAmount);
                customer.setPostpaidStatus(PostpaidStatus.linked);
                log.info("postpaidDepositAmount: {} in",postpaidDepositAmount);
                this.customerRepository.save(customer);
                postpaidAnalyticsDump(userId, customer, PostpaidStatus.linked.toString(), PaymentMethod.razorpay.toString());

            }
        }catch (Exception e){
            log.info("error in capturing payment: {}", e.getMessage());
            captureResponse = e.getMessage();
            result = false;
        }
        log.info("capture response: {}",captureResponse);
        String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
        try {
            PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId,
                    customer, "DEPOSIT", "postpaid", orderIdRand, paymentId, PaymentMethod.razorpay.toString(), amountInr, result, captureResponse);
        } catch (Exception e){
            log.info("saving to transaction failed.");
        }
        if(paymentTransactionLog != null){
            paymentTransactionLog.setStatus(String.valueOf(result));
            this.paymentTrasactionLogRepository.save(paymentTransactionLog);
        }else{
            log.info("saving to payment transaction log failed.");
        }
        if(!result){
            throw new CustomException("Failed to Capture");
        }
    }

    private void postpaidAnalyticsDump(long userId, Customer customer, String status, String method) {
        try {
            AnalyticsDumpDto analyticsDumpDto = AnalyticsDumpDto.builder()
                    .kingdom("mb_python")
                    .phylum("deposit_payments")
                    .family("postpaid_deposit")
                    .genusNum(Double.valueOf(userId))
                    .num1(customer.getPostpaidDepositAmount())
                    .species(method)
                    .str1(status)
                    .str2(String.valueOf(customer.getPaytmTokenExpiry())) //todo check for null pointer.
                    .str3(customer.getPaytmToken())
                    .build();
            TrackingDumpUtil.dumpIntoAnimalKingdom(analyticsDumpDto);
        } catch (Exception e){
            log.info("error inserting in animal kingdom in postpaid");
        }
    }


    @Override
    public void refundRazorpay(User user) {

        Customer customer = this.customerRepository.findById(user.getId())
                .orElseThrow(()-> new IllegalArgumentException("customer doesn't exits"));
        Booking latest_booking = this.bookingRepository.findFirstByCustomerOrderByIdDesc(customer);

        //todo : see if this can be optimised.
        if(latest_booking != null) {
            if (BookingStatus.HOLD.equals(latest_booking.getBookingStatus()) ||
                    BookingStatus.IN_TRIP.equals(latest_booking.getBookingStatus()) ||
                    BookingStatus.RESERVED.equals(latest_booking.getBookingStatus())) {
                throw new CustomException("You cannot withdraw your deposit during a ride", 804);
            }
        }

        if(customer.getTotalAmountDue() > 0){
            Map<String, Object> data = new HashMap<>();
            data.put("dues_amount", customer.getTotalAmountDue());
            throw new CustomException("Please clear your dues to withdraw the deposit", 803, data);
        }

        if(PostpaidStatus.linked.equals(customer.getPostpaidStatus())) {
            customer.setPostpaidStatus(PostpaidStatus.refund_initiated);
            this.customerRepository.save(customer);

            postpaidAnalyticsDump(user.getId(), customer, PostpaidStatus.refund_initiated.toString(), PaymentMethod.razorpay.toString());

            PaytmTransaction paytmTransaction = paytmTransactionRepository
                    .findFirstByCustomerAndTypeAndCallTypeOrderByIdDesc(customer,"DEPOSIT", "postpaid");
            //todo null check for paytmTransaction
            if(paytmTransaction == null) {
                throw new CustomException("No transaction with the transaction Id");
            }

            log.info("paymentId :{}, id:{}, paymentMethod:{}", paytmTransaction.getPaytmTransactionId(), paytmTransaction.getId(), paytmTransaction.getMethod());
            String paymentId = paytmTransaction.getPaytmTransactionId();
            double depositAmount = customer.getPostpaidDepositAmount();
            if(PaymentMethod.razorpay.toString().equals(paytmTransaction.getMethod())){
                String refund = razorpayService.issueFullRefund(paymentId ,depositAmount, user.getId());
                PostpaidRefunds postpaidRefunds = LoggingUtil.insertIntoPostpaidRefunds(postpaidRefundsRepository,
                        user.getId(), paytmTransaction.getMethod(), paytmTransaction.getPaytmTransactionId(), refund);
            }  else if (PaymentMethod.juspay.toString().equals(paytmTransaction.getMethod())) {
                juspayService.Juspayrefund(user.getId(), depositAmount, paytmTransaction.getPaytmTransactionId());
            } else if (paytmTransaction.getMethod() == null || PaymentMethod.paytm.toString().equals(paytmTransaction.getMethod())){
                String result = paytmRepository.refundPaytm(paytmTransaction.getOrderId(), depositAmount, paytmTransaction.getPaytmTransactionId(), user.getId());
                PostpaidRefunds postpaidRefunds = LoggingUtil.insertIntoPostpaidRefunds(postpaidRefundsRepository,
                        user.getId(), paytmTransaction.getMethod(), paytmTransaction.getPaytmTransactionId(), result);
            }
            customer.setPostpaidStatus(PostpaidStatus.not_linked);
            customer.setPostpaidDepositAmount(0.0);
            this.customerRepository.save(customer);
            postpaidAnalyticsDump(user.getId(), customer, PostpaidStatus.not_linked.toString(), PaymentMethod.razorpay.toString());
        }else if(PostpaidStatus.refund_initiated.equals(customer.getPostpaidStatus())){
            throw new CustomException("Refund is already in progress", 802);
        }else{
            throw new CustomException("Pay After Trip is not activated", 801);
        }
    }

}
