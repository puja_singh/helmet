package com.bounce.impl;

import com.bounce.SQSQueueService;
import com.bounce.dtos.razorpaywebhook.RazorpayWebhookRequestDto;
import com.bounce.juspay.JuspayQueuePolling;
import com.bounce.utils.SQS.SQSQueue;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;

@Named
@Slf4j
public class SQSQueueServiceImpl implements SQSQueueService {


    @Inject private SQSQueue sqsQueue;
    @Inject private JuspayQueuePolling juspayQueuePolling;

    @Override
    public void insertIntoQueue(RazorpayWebhookRequestDto requestDto) {
        this.sqsQueue.insertIntoQueue(requestDto);
    }

    @Override
    public boolean setupLongPolling() {
        try {
            this.sqsQueue.setupLongPolling();
            log.info("Long polling is set up ");
            return true;
        } catch (Exception e){
            return false;
        }

    }
    @Override
    public boolean setupJuspayLongPolling() {
        try {
            this.juspayQueuePolling.longPollingForJuspay();
            log.info("Long polling is set up  for juspay");
            return true;
        } catch (Exception e){
            return false;
        }

    }
}
