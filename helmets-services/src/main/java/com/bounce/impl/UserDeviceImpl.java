package com.bounce.impl;

import com.bounce.UserDeviceService;
import com.bounce.utils.JedisCommands;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Named;

@Named
@Slf4j
public class UserDeviceImpl implements UserDeviceService {

    private static final int REDIS_DB = 0;
    private static final String PLATFORM = "bounce.user.app.platform.%s";
    private static final String APPVERSION = "bounce.user.app.version.%s";
    private static final String FIRST_RIDE = "bounce.user.app.firstridedone.%s";

    @Override
    public String getUserDevicePlatform(long userId) {
        try {

            String platformKey = String.format(PLATFORM, userId);
            log.info("platform key :{}",platformKey);
            return JedisCommands.get(platformKey);
        }catch (Exception e){
            log.error("error in finding userdeviceplatform",e);
        }
        return null;
    }

    @Override
    public String getUserDeviceAppVersion(long userId) {
        try {
            String appVersionKey = String.format(APPVERSION, userId);
            return JedisCommands.get(appVersionKey);
        }catch (Exception e){
            log.error("error in finding userdeviceplatform",e);
        }
        return null;

    }

    @Override
    public boolean isFirstRide(long userId) {
        try{
            String isFirstRide = String.format(FIRST_RIDE, userId);
            if(JedisCommands.exists(isFirstRide)){
                return false;
            }
        } catch (Exception e) {
            log.info("error in finding isFirstRide", e);
            return true;
        }
        return true;
    }
}
