package com.bounce.impl;

import com.bounce.PaymentMethodService;
import com.bounce.PaytmService;
import com.bounce.cash.BounceCashApi;
import com.bounce.dtos.*;
import com.bounce.dtos.cash.ClientWalletEstimateResponseDto;
import com.bounce.dtos.enums.LinkedStatus;
import com.bounce.dtos.paytm.PaytmWalletDetails;
import com.bounce.entity.Customer;
import com.bounce.entity.User;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.entity.enums.PostpaidStatus;
import com.bounce.helper.GlobalConfigService;
import com.bounce.repository.CustomerRepository;

import com.bounce.utils.BillingUtil;
import lombok.extern.slf4j.Slf4j;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;

@Named
@Slf4j
public class PaymentMethodServiceImpl implements PaymentMethodService {

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private PaytmService paytmService;

    @Inject
    private GlobalConfigService globalConfigService;

    @Inject
    private BounceCashApi bounceCashRepository;

    @Override
    public List<PaymentMethodDetailsV1> getPaymentMethodDetails(long customerId) {
        final List<PaymentMethodDetailsV1> paymentMethodsForCustomer = new ArrayList<>();

        final Customer customer = this.customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid customer object passed"));

        final PaymentMethodDetailsV1 paytm = this.forPaytm(customer);
        final PaymentMethodDetailsV1 postpaid = this.forPostpaid(customer);

        //if postpaid is linked postpaid should be at index 0
        if (postpaid.getLinkedStatus() == LinkedStatus.linked) {
            paymentMethodsForCustomer.add(postpaid);
            paymentMethodsForCustomer.add(paytm);
        }
        else {
            //else normal flow
            paymentMethodsForCustomer.add(paytm);
            paymentMethodsForCustomer.add(postpaid);
        }
        return paymentMethodsForCustomer;
    }

    @Override
    public PaymentMethodDetailsResponse RefreshPaymentDetails(User user, double estNetFare) {
        final Customer customer = this.customerRepository.findById(user.getId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid customer object passed"));
        ClientWalletEstimateResponseDto clientWalletEstimateResponseDto =
                bounceCashRepository.clientWalletEstimate(estNetFare, user.getToken());
        double bounceWalletBalance = clientWalletEstimateResponseDto.getResult().getData()[0].getWalletBalance();

        final PaymentMethodDetailsV2 paytm = getPaytmDetails(customer);
        final PaymentMethodDetailsV2 postpaid = getParDetails(customer);
        final PaymentMethodDetailsV2 bounceCash = getBounceCashDetails(customer, user.getToken(), bounceWalletBalance);

        final List<PaymentMethodDetailsV2> paymentMethodsForCustomer = new ArrayList<>();
        paymentMethodsForCustomer.add(bounceCash);
        paymentMethodsForCustomer.add(postpaid);
        paymentMethodsForCustomer.add(paytm);

        PaymentMethod defaultMethod = getDefaultPaymentMethod(estNetFare, paytm, postpaid, bounceCash);

        return new PaymentMethodDetailsResponse(defaultMethod, paymentMethodsForCustomer,
                clientWalletEstimateResponseDto.getResult().getData()[0].getRewardUsedText(),
                clientWalletEstimateResponseDto.getResult().getData()[0].getBounceCashText(),
                clientWalletEstimateResponseDto.getResult().getData()[0].getBounceCashSubText(),
                clientWalletEstimateResponseDto.getResult().getData()[0].getStatus(),
                clientWalletEstimateResponseDto.getResult().getData()[0].getMessage(),
                clientWalletEstimateResponseDto.getResult().getData()[0].getStrike_cost());
    }

    private PaymentMethod getDefaultPaymentMethod(double estNetFare, PaymentMethodDetailsV2 paytm,
                                           PaymentMethodDetailsV2 postpaid, PaymentMethodDetailsV2 bounceCash) {
        PaymentMethod defaultMethod = null;
        if(BillingUtil.compareTo(estNetFare, 0d) > 0 && bounceCash.getBalance() >= estNetFare){
            defaultMethod = PaymentMethod.bounce_cash;
        } else if(LinkedStatus.linked.equals(postpaid.getLinkedStatus())){
            defaultMethod = PaymentMethod.postpaid;
        } else if(LinkedStatus.linked.equals(paytm.getLinkedStatus())){
            defaultMethod = PaymentMethod.paytm;
        }
        return defaultMethod;
    }

    private PaymentMethodDetailsV2 getBounceCashDetails(Customer customer, String token, double walletBalance) {
        PaymentMethodDetailsV1 bounceCashDetails = new PaymentMethodDetailsV1();
        bounceCashDetails.setDepositAmount(0d);
        bounceCashDetails.setLinkedStatus(LinkedStatus.linked);
        bounceCashDetails.setPaymentMethod(PaymentMethod.bounce_cash);
        return new PaymentMethodDetailsV2(bounceCashDetails, new BounceCashDisplayDetails(), walletBalance);
    }

    private PaymentMethodDetailsV2 getPaytmDetails(Customer customer) {
        final PaymentMethodDetailsV1 paytmDetailsV1 = this.forPaytm(customer);
        PaymentDisplayDetails paytmDisplayDetails = new PaytmDisplayDetails();
        paytmDisplayDetails.setMainString(String.format(paytmDisplayDetails.getMainString(),
                globalConfigService.getPaytmWalletMinBalance()));
        if(LinkedStatus.linked.equals(paytmDetailsV1.getLinkedStatus())) {
            paytmDisplayDetails.setCtaText(null);
        }
        return new PaymentMethodDetailsV2(paytmDetailsV1, paytmDisplayDetails, 0d);
    }

    private PaymentMethodDetailsV2 getParDetails(Customer customer) {
        final PaymentMethodDetailsV1 postpaidv1 = this.forPostpaid(customer);

        PaymentDisplayDetails postpaidDisplayDetails = new PostpaidDisplayDetails();
        if(LinkedStatus.linked.equals(postpaidv1.getLinkedStatus())){
            postpaidDisplayDetails.setCtaText(null);
        }
        return new PaymentMethodDetailsV2(postpaidv1, postpaidDisplayDetails, 0d);
    }

    private PaymentMethodDetailsV1 forPaytm(Customer customer) {
        final PaytmWalletDetails paytmWalletDetails = this.paytmService.getWalletDetails(customer);

        final PaymentMethodDetailsV1 retVal = new PaymentMethodDetailsV1();
        retVal.setCustomer(customer);
        retVal.setLinkedStatus(paytmWalletDetails.getIsTokenValid() ? LinkedStatus.linked : LinkedStatus.not_linked);
        retVal.setPaymentMethod(PaymentMethod.paytm);
        retVal.setDepositAmount(0.0);
        return retVal;
    }

    private PaymentMethodDetailsV1 forPostpaid(Customer customer) {
        final PaymentMethodDetailsV1 retVal = new PaymentMethodDetailsV1();
        retVal.setCustomer(customer);
        retVal.setLinkedStatus(Objects.equals(customer.getPostpaidStatus(), PostpaidStatus.linked) ? LinkedStatus.linked
                : LinkedStatus.not_linked);
        retVal.setPaymentMethod(PaymentMethod.postpaid);
        retVal.setDepositAmount(this.globalConfigService.getPostpaidSecurityAmount());
        return retVal;
    }
}
