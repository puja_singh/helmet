package com.bounce.impl;

import com.bounce.JuspayService;
import com.bounce.UserDeviceService;
import com.bounce.dtos.AnalyticsDumpDto;
import com.bounce.dtos.juspay.*;
import com.bounce.entity.*;
import com.bounce.entity.enums.FlowType;
import com.bounce.entity.enums.PostpaidStatus;
import com.bounce.exceptions.CustomException;
import com.bounce.juspay.JuspayApi;
import com.bounce.repository.JuspayCustomerRepository;
import com.bounce.repository.JuspayFailedTxnsRefundsRepository;
import com.bounce.repository.JuspayOrderRepository;
import com.bounce.repository.JuspayRefundsRepository;
import com.bounce.utils.RealtimeData;
import com.bounce.utils.SQS.SQSQueueTxns;
import com.bounce.utils.TrackingDumpUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.uuid.Generators;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Named
@Slf4j
public class JuspayServiceImpl implements JuspayService {

    @Value("${juspay.prefix}")
    private String juspayPrefix ;

    @Value("${juspay.fifoqueue}")
    private String JUSPAY_QUEUE_NAME;;

    @Inject private JuspayOrderRepository juspayOrderRepository;
    @Inject private JuspayCustomerRepository juspayCustomerRepository;
    @Inject private JuspayApi juspayApi;
    @Inject private JuspayFailedTxnsRefundsRepository juspayFailedTxnsRefundsRepository;
    @Inject private UserDeviceService userDeviceService;
    @Inject private JuspayRefundsRepository juspayRefundsRepository;

    @Override
    public CreateOrderResponse createOrder(User user, CreateOrderRequest createOrderRequest){

        JuspayOrders juspayOrders = insertIntoOrders(user.getId(), createOrderRequest);
        String orderId = null;
        if(!juspayPrefix.equals("prod")){
            orderId = String.format("%s_bounce_%s", juspayPrefix, juspayOrders.getId());
        } else {
            orderId = String.format("bounce_%s", juspayOrders.getId());
        }
        Map<String, Object> udf = getUdfObject(user, createOrderRequest);
        CreateOrderResponse createOrderResponse = juspayApi.createOrder(user.getId(), orderId, createOrderRequest.getAmount(), createOrderRequest.getCustId(), udf);

        juspayOrders = updateOrders(juspayOrders, createOrderResponse);
        return createOrderResponse;
    }

    private Map<String, Object> getUdfObject(User user, CreateOrderRequest createOrderRequest) {
        Map<String, Object> udf = new HashMap<>();
        udf.put("flow_type", createOrderRequest.getFlowType());
        udf.put("user_id", user.getId());
        udf.put("bounce_cash_used", createOrderRequest.getBounceCashUsed());
        if(FlowType.WALLET_TOPUP.equals(createOrderRequest.getFlowType())){
            udf.put("offer_id", createOrderRequest.getOfferId());
        }
        if(FlowType.PAID_HELMET.equals(createOrderRequest.getFlowType())){
            udf.put("pincode", createOrderRequest.getPinCode());
            String address = String.format("'%s'",createOrderRequest.getAddress());
            udf.put("address", address);
        }
        return udf;
    }

    public String customer(User user){
        JuspayCustomers juspayCustomers = this.juspayCustomerRepository.findFirstByUserIdOrderByIdDesc(user.getId());

        if(juspayCustomers == null){

            String custId;
            if(!juspayPrefix.equals("prod")){
                custId = String.format("%s_bounce_%s",juspayPrefix, user.getId());
            } else {
                custId = String.format("bounce_%s",user.getId());
            }
            CreateCustomerResponse createCustomerResponse = juspayApi.createCustomer(user, custId);

            String juspayCustId = createCustomerResponse.getId();
            JuspayCustomers juspaynewCustomers = insertIntoCustomers(custId, user.getId(), juspayCustId);
            return juspaynewCustomers.getCustId();
        }
        return juspayCustomers.getCustId();
    }

    @Override
    public void serverFailedJuspayTxnsRefund(String orderId, double amount, JuspayOrders juspayOrders, String remarks){
        JuspayFailedTxnsRefunds juspayFailedRefunds = insertIntoRefunds(amount, juspayOrders.getCustId(), orderId, juspayOrders.getUserId(), remarks);
        String uniqueRequestId;
        if(!juspayPrefix.equals("prod")){
            uniqueRequestId = String.format("%s_bounce_refund_%s", juspayPrefix, juspayFailedRefunds.getId());
        } else {
            uniqueRequestId = String.format("bounce_refund_%s", juspayFailedRefunds.getId());
        }
        log.info("unique_request_id : {}", uniqueRequestId);
        JuspayRefundResponseDto juspayRefundResponseDto = juspayApi.refundOrder(amount, orderId, uniqueRequestId, juspayOrders.getUserId());
        //todo update JuspayFailedRefunds after seeing the refundOrder response.
        juspayFailedRefunds = updateJuspayFailedTxnsRefunds (juspayFailedRefunds, uniqueRequestId, juspayRefundResponseDto.getRefunds()[0].getStatus());

    }

    @Override
    public void Juspayrefund(long userId, double amount, String orderId){
        JuspayOrders juspayOrders = this.juspayOrderRepository.findFirstByOrderIdOrderByIdDesc(orderId);

        if(juspayOrders == null){
            throw new CustomException("No order present with the given orderId");
        }

        String uniqueRequestId;
        JuspayRefunds juspayRefunds = insertIntoJuspayRefunds(userId, amount, juspayOrders.getCustId(), orderId, juspayOrders.getFlowType());
        if(!juspayPrefix.equals("prod")){
            uniqueRequestId = String.format("%s_bounce_refund_%s", juspayPrefix, juspayRefunds.getId());
        } else {
            uniqueRequestId = String.format("bounce_refund_%s", juspayRefunds.getId());
        }
        JuspayRefundResponseDto juspayRefundResponseDto = juspayApi.refundOrder(amount, orderId, uniqueRequestId, userId);
        updateJuspayRefunds(juspayRefunds,  uniqueRequestId, juspayRefundResponseDto.getRefunds()[0].getStatus());

    }

    @Transactional
    public void updateJuspayRefunds(JuspayRefunds juspayRefunds, String refundRequestId, String status){
        juspayRefunds.setRefundRequestId(refundRequestId);
        juspayRefunds.setStatus(status);
        juspayRefundsRepository.save(juspayRefunds);
    }

    @Transactional
    public JuspayRefunds insertIntoJuspayRefunds(long userId, double amount, String custId, String orderId, FlowType flowType){
        JuspayRefunds juspayRefunds = JuspayRefunds.builder()
                .amount(amount)
                .custId(custId)
                .flowType(flowType.toString())
                .orderId(orderId)
                .status(PostpaidStatus.refund_initiated.toString())
                .userId(userId)
                .build();
        juspayRefundsRepository.save(juspayRefunds);
        //todo save.
        return juspayRefunds;

    }

    @Transactional
    private JuspayFailedTxnsRefunds insertIntoRefunds(double amount, String custId, String orderId, long userId, String remarks){
        JuspayFailedTxnsRefunds juspayFailedRefunds = JuspayFailedTxnsRefunds.builder()
                .amount(amount)
                .custId(custId)
                .orderId(orderId)
                .userId(userId)
                .status("INITIATED")
                .remarks(remarks)
                .build();
        juspayFailedTxnsRefundsRepository.save(juspayFailedRefunds);
        return juspayFailedRefunds;
    }

    @Transactional
    private JuspayFailedTxnsRefunds updateJuspayFailedTxnsRefunds (JuspayFailedTxnsRefunds juspayFailedTxnsRefunds, String refundRequestId, String status){
        juspayFailedTxnsRefunds.setRefundRequestId(refundRequestId);
        juspayFailedTxnsRefunds.setStatus(status);
        juspayFailedTxnsRefundsRepository.save(juspayFailedTxnsRefunds);
        return juspayFailedTxnsRefunds;

    }

    @Transactional
    private JuspayCustomers insertIntoCustomers(String custId, long userId, String juspayCustId){
        JuspayCustomers juspayCustomers = JuspayCustomers.builder()
                .custId(custId)
                .userId(userId)
                .jusypayCustId(juspayCustId)
                .build();
        this.juspayCustomerRepository.save(juspayCustomers);
        return juspayCustomers;
    }

    @Transactional
    private JuspayOrders insertIntoOrders(long userId,  CreateOrderRequest createOrderRequest){
//
        String userDevicePlatform = this.userDeviceService.getUserDevicePlatform(userId);
        String userAppVersion = this.userDeviceService.getUserDeviceAppVersion(userId);
        JuspayOrders juspayOrders = JuspayOrders.builder()
                .amount(createOrderRequest.getAmount())
                .custId(createOrderRequest.getCustId())
                .userId(userId)
                .flowType(createOrderRequest.getFlowType())
                .orderStatus("Created")
                .refundStatus(false)
                .userApp(userDevicePlatform)
                .appVersion(userAppVersion)
                .paymentMethod(createOrderRequest.getPaymentMethod())
                .paymentMethodType(createOrderRequest.getPaymentMethodType())
                .build();
        this.juspayOrderRepository.save(juspayOrders);
        return juspayOrders;

    }

    @Transactional
    private JuspayOrders updateOrders(JuspayOrders juspayOrders, CreateOrderResponse createOrderResponse) {

        juspayOrders.setOrderId(createOrderResponse.getOrderId());
        juspayOrders.setJuspayOrderStatus(createOrderResponse.getStatus());
        juspayOrders.setJuspayOrderId(createOrderResponse.getJuspayOrderId());
        this.juspayOrderRepository.save(juspayOrders);
        return juspayOrders;
    }

    @Override
    public void insertIntoQueueJuspay(WebhookRequestDto webhookRequestDto){

        try{
            AnalyticsDumpDto analyticsDumpDto = AnalyticsDumpDto.builder()
                    .kingdom("mb_python")
                    .phylum("payments")
                    .family("juspay")
                    .order("juspayCallback")
                    .className(webhookRequestDto.getContent().getOrder().getOrderId())
                    .species("request")
                    .str1(webhookRequestDto.getContent().getOrder().getCustId())
                    .str2(webhookRequestDto.getEventName())
                    .str3(webhookRequestDto.getContent().getOrder().getUdf())
                    .build();
            TrackingDumpUtil.dumpIntoAnimalKingdom(analyticsDumpDto);
        } catch (Exception e){
            log.info("error in logging the webhook");
        }

        RealtimeData.get().getScheduledExecutorService().schedule(() -> {
            try{
                insertIntoQueueJuspayHelper(webhookRequestDto);
            }catch (Exception e){
                log.error("Error while inserting into queue", e);
            }
        }, 0, TimeUnit.MILLISECONDS);
    }

    private void insertIntoQueueJuspayHelper(WebhookRequestDto webhookRequestDto) throws Exception {

        JSONObject requestObject = new JSONObject();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode().pojoNode(webhookRequestDto);
        String jsonString = "";
        jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
        requestObject.put("request", jsonString);
        String dedupId = Generators.timeBasedGenerator().generate().toString(); //todo change this.
        SQSQueueTxns.SqsInsertion(requestObject, dedupId, JUSPAY_QUEUE_NAME);
    }

}
