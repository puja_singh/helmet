package com.bounce.impl;

import com.bounce.RazorpayService;
import com.bounce.exceptions.CustomException;
import com.bounce.razorpay.RazorpayApi;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;

@Slf4j
@Named
public class RazorpayServiceImpl implements RazorpayService {

    @Inject private RazorpayApi razorpayApi;

    @Override
    public String statusCheck(String paymentId) {
        try {
            return this.razorpayApi.statusCheck(paymentId);
        } catch (Exception e) {
            log.info("error in razorpay statusCheck: ", e);
            return "{}";
        }
    }

    @Override
    public String capture(String paymentId, double amountInr, long userId) {
        try {
            return this.razorpayApi.capture(paymentId, amountInr, userId);
        } catch (Exception e) {
            //todo check if it throws an error
            log.info("error in razorpay capture: ", e);
            return "{}";
        }
    }

    @Override
    public String issueFullRefund(String paymentId, double amountInr, long userId) {
        try {
            return this.razorpayApi.issueFullRefund(paymentId, amountInr, userId);
        } catch (Exception e) {
            log.info("error in razorpay statusCheck: ", e);
            throw new CustomException("");
        }
    }
}
