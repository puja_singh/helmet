package com.bounce.exceptions;

import lombok.Getter;

import java.util.Map;

@Getter
public class CustomException extends RuntimeException {
    private int code = 500;
    private String data = "";
    private Map<String, Object> dataMap = null;



    public CustomException(String message, int code) {
        super(message);
        this.code = code;
    }

    public CustomException(String message, int code, String data) {
        this(message, code);
        this.data = data;
    }

    public CustomException(String message, int code, Map<String, Object> dataMap) {
        this(message, code);
        this.dataMap = dataMap;
    }

    public CustomException(String message) {
        this(message, 500);
    }

    @Override
    public String toString() {
        return String.format("%s with code= %s and data= %s", super.toString(), this.code, this.data);
    }
}
