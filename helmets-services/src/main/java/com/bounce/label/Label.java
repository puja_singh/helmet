package com.bounce.label;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Label {

    TRIP_FARE("Trip Fare"), MIN_TRIP_FARE("Minimum fare applied"), REWARD_POINTS("Reward Points Discount"),
    SUB_TOTAL("Subtotal"), TAX("Tax"), TOTAL_FARE("Total Fare"), BOUNCE_WALLET("Bounce Wallet"),
    PAYTM("To Pay"), DUES("Dues"), DISCLAIMER("Actual fare may differ based on time & distance covered by you");

    private String code;

}
