package com.bounce.label;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseStyle {

    private StyleDetails label;

    private StyleDetails value;
}
