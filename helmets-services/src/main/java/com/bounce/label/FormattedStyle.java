package com.bounce.label;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormattedStyle extends BaseStyle {

    private String type;

    private String mtop;

    private String color;

}
