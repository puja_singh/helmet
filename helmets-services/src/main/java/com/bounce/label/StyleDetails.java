package com.bounce.label;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class StyleDetails {

    @JsonProperty("size")
    private String size;

    @JsonProperty("color")
    private String color;

    @JsonProperty("content")
    private String content;

    @JsonProperty("alignment")
    private String alignment;

    @JsonProperty("weight")
    private String weight;

    @JsonProperty("image_url")
    private String imageUrl;

    @JsonProperty("strike")
    private Boolean strike;

    @JsonProperty("image_dim")
    private String imageDim;

    @JsonProperty("align")
    private String align;

}
