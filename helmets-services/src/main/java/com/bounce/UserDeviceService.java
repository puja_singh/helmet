package com.bounce;

import com.bounce.dtos.UserDeviceInfo;

public interface UserDeviceService {

    public String getUserDevicePlatform(long userId);

    public String getUserDeviceAppVersion(long userId);

    boolean isFirstRide(long userId);
}
