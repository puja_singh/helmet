package com.bounce;

import com.bounce.dtos.razorpaywebhook.RazorpayWebhookRequestDto;

public interface SQSQueueService {


    void insertIntoQueue(RazorpayWebhookRequestDto requestDto);

    boolean setupLongPolling();


    boolean setupJuspayLongPolling();
}
