package com.bounce;

import com.bounce.dtos.paytm.LockAmountResponse;
import com.bounce.dtos.paytm.PaytmWalletDetails;
import com.bounce.entity.Booking;
import com.bounce.entity.Customer;

public interface PaytmService extends PaymentService {

    boolean isTokenValidFor(Customer customer);

    double getWalletBalance(Customer customer);

    PaytmWalletDetails getWalletDetails(Customer customer);

    void raisePaytmLowBalanceCustomException(double rechargeAmount, double currentBalance, boolean isBalanceLow);

    LockAmountResponse preAuth(Booking booking, double amountToBeLocked);
}
