package com.bounce.paytm;

import com.bounce.dtos.AnalyticsDumpDto;
import com.bounce.dtos.paytm.LockAmountResponse;
import com.bounce.dtos.paytm.ReleaseAmountResponse;
import com.bounce.dtos.paytm.WithdrawAmountResponse;
import com.bounce.dtos.paytm.WalletBalanceResponse;
import com.bounce.entity.PaymentTransactionLog;
import com.bounce.entity.PaytmPreAuth;
import com.bounce.entity.enums.PaymentMethod;
import com.bounce.exceptions.CustomException;
import com.bounce.entity.Customer;
import com.bounce.entity.PaytmTransaction;
import com.bounce.repository.PaymentTrasactionLogRepository;
import com.bounce.repository.PaytmPreAuthRepository;
import com.bounce.repository.PaytmTransactionRepository;
import com.bounce.repository.UserRepository;
import com.bounce.utils.ClientUtil;
import com.bounce.utils.LoggingUtil;
import com.bounce.utils.TrackingDumpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.uuid.Generators;
import com.paytm.pg.merchant.CheckSumServiceHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.jooq.tools.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.TreeMap;

@Slf4j
@Named
public class PaytmRepository {
    //merchant credentials
    private static final String PAYTM_CURRENCY = "INR";
    private static final String PAYTM_INDUSTRY_TYPE_ID = "Retail110";
    private static final String MERCHANT_KEY = "V!@4zPvEUMMQ7g_t";
    private static final String MID = "Wicked31644082492327";
    private static final String PAYTM_MID = "Wicked31644082492327";
    private static final String PAYTM_MERCHANT_KEY = "V!@4zPvEUMMQ7g_t";
    private static final String PAYTM_CLIENT_SECRET = "799a5cee-ca80-4940-a79a-b7375d2c50ff";

    //base urls
    private static final String PAYTM_TXN_URL = "https://securegw.paytm.in";

    //api path
    private static final String PAYTM_WITHDRAW_URL = "/order/directPay";
    private static final String PAYTM_CAPTURE_URL = "/order/capture";
    private static final String PAYTM_CHECK_STATUS_URL = "/order/status";
    private static final String PAYTM_PRE_AUTH_URL = "/order/preAuth";
    private static final String PAYTM_RELEASE_URL = "/order/release";
    private static final String PAYTM_WALLET_BALANCE = "/service/checkUserBalance";
    private static final String PAYTM_REFUND = "/refund/apply";

    //response satus
    private static final String TXN_SUCCESS = "TXN_SUCCESS";
    private static final String TXN_FAILURE = "TXN_FAILURE";
    private static final String PENDING = "PENDING";

    //withdrawScw requestTypes
    private static final String WITHDRAW = "WITHDRAW";
    private static final String CAPTURE = "CAPTURE";
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final CheckSumServiceHelper checkSumServiceHelper = CheckSumServiceHelper.getCheckSumServiceHelper();

    @Value("${paytm.autorelease.time:12}")
    private String paytmAutoReleaseTime;

    @Inject private UserRepository userRepository;
    @Inject private PaytmTransactionRepository paytmTransactionRepository;
    @Inject private PaytmPreAuthRepository paytmPreAuthRepository;
    @Inject private PaymentTrasactionLogRepository paymentTrasactionLogRepository;

    private String generateChecksum(TreeMap<String, String> params) throws Exception {
        return checkSumServiceHelper.genrateCheckSum(MERCHANT_KEY, params);
    }

    public LockAmountResponse lockAmount(long bookingId, String token, double amount, long userId) throws CustomException {
        TreeMap<String, String> params = new TreeMap<>();
        log.info("[PaytmRepository:PaytmLockAmountApi] paytmAutoReleaseTime {}", this.paytmAutoReleaseTime);
        try {
            params.put("MID", MID);
            String orderId = String.format("%s_%s", bookingId, Generators.timeBasedGenerator().generate());

            PaytmPreAuth paytmPreAuth = PaytmPreAuth.builder().build();
            paytmPreAuth.setBookingId(bookingId);
            paytmPreAuth.setOrderId(orderId);
            paytmPreAuthRepository.save(paytmPreAuth);

            params.put("ORDER_ID", orderId);
            params.put("TOKEN", token);
            params.put("TXN_AMOUNT", String.valueOf(amount));
            params.put("DURATIONHRS", paytmAutoReleaseTime);
            params.put("CHECKSUM", generateChecksum(params));

            String url = String.format("%s%s", PAYTM_TXN_URL, PAYTM_PRE_AUTH_URL);
            URIBuilder ub = new URIBuilder(url);
            String paramsString = objectMapper.writeValueAsString(params);
            log.info("paramsString: {}", paramsString);
            ub.addParameter("JsonData", paramsString);
            String apiUrl = String.valueOf(ub.build());
            Client client = ClientUtil.getClient();
            try {
                insertAnimalKingdom(orderId, userId, paramsString, "request", "lockAmount", apiUrl);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in lockAmount request");
            }
            String result = client.target(apiUrl)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);
            log.info("lockAmountResponse[raw] for bookingId: {} is {}", bookingId, result);
            LockAmountResponse lockAmountResponse = objectMapper.readValue(result, LockAmountResponse.class);
            log.info("lockAmountResponse for bookingId: {} is {}", bookingId, lockAmountResponse);
            try {
                insertAnimalKingdom(orderId, userId, String.valueOf(lockAmountResponse), "response", "lockAmount", apiUrl);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in lockAmount response");
            }
            if (TXN_SUCCESS.equals(lockAmountResponse.getStatus())) {
                log.info("Paytm: amount locked successfully for orderId {} , amount {}, preauthId {}", orderId, amount, lockAmountResponse.getPreauthId());
                return lockAmountResponse;
            }
            else {
                log.info("Paytm: locking amount failed: {}", lockAmountResponse);
                throw new Exception("locking amount failed");
            }
        } catch (Exception e) {
            log.error("Error happend in lockAmount: ", e);
            throw new CustomException("Oops, we see an issue with your Paytm account. Use 'Pay After Ride' option to continue with your booking.", 631);
        }
    }

    public void releaseAmount(String token, String preAuthId, long userId) throws CustomException {
        try {
            TreeMap<String, String> params = new TreeMap<>();
            params.put("MID", MID);
            params.put("TOKEN", token);
            params.put("PREAUTH_ID", preAuthId);
            params.put("CHECKSUM", generateChecksum(params));

            String url = String.format("%s%s", PAYTM_TXN_URL, PAYTM_RELEASE_URL);
            URIBuilder ub = new URIBuilder(url);
            String paramsString = objectMapper.writeValueAsString(params);
            ub.addParameter("JsonData", paramsString);
            log.info("paramsString: {}", paramsString);
            String apiUrl = String.valueOf(ub.build());
            Client client = ClientUtil.getClient();
            try {
                insertAnimalKingdom(preAuthId, userId, paramsString, "request", "releaseAmount", apiUrl);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in lockAmount request");
            }
            String result = client.target(apiUrl)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get(String.class);
            log.info("releaseAmountResponse[raw] for preAuthId: {} is {}", preAuthId, result);
            ReleaseAmountResponse releaseAmountResponse =
                    objectMapper.readValue(result, ReleaseAmountResponse.class);
            log.info("releaseAmountResponse for preAuthId: {} is: {}", preAuthId, releaseAmountResponse);
            try {
                insertAnimalKingdom(preAuthId, userId, String.valueOf(releaseAmountResponse), "response", "releaseAmount", apiUrl);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in releaseAmount response");
            }
            if (TXN_SUCCESS.equals(releaseAmountResponse.getStatus())) {
                log.info("Paytm: releaseAmount successful for preAuthId: {}", preAuthId);
            } else {
                log.info("Paytm: releaseAmount failed for preAuthId: {} message: {}", preAuthId, releaseAmountResponse.getMessage());
                throw new Exception("release amount failed");
            }
        } catch (Exception e) {
            throw new CustomException("release amount failed", 600);
        }
    }

    private String withdrawApi(Customer customer, String orderId, double amount, String reqType, String preAuthId) throws Exception {
        String paytmToken = customer.getPaytmToken();
        if (paytmToken == null) {
            throw new CustomException("Paytm not linked for the user", 602);
        } else if (customer.getPaytmTokenExpiry().before(new Date()) && WITHDRAW.equals(reqType)) {
            // CAPTURE works even for expired tokens if preauth is successful
            // REF: Paytm documentation
            throw new CustomException("Paytm token expired for the user", 601);
        }

        TreeMap<String, String> params = new TreeMap<>();
        params.put("MID", MID);
        params.put("ReqType", reqType);
        params.put("TxnAmount", String.valueOf(amount));
        params.put("OrderId", orderId);
        params.put("Currency", PAYTM_CURRENCY);
        params.put("DeviceId", userRepository.findById(customer.getId()).get().getMobileNumber());
        params.put("SSOToken", paytmToken);
        params.put("PaymentMode", "PPI");
        params.put("CustId", String.valueOf(customer.getId()));
        params.put("IndustryType", PAYTM_INDUSTRY_TYPE_ID);
        params.put("Channel", "WEB");
        params.put("AuthMode", "USRPWD");
        String apiPath = PAYTM_WITHDRAW_URL;
        if ("CAPTURE".equals(reqType)) {
            apiPath = PAYTM_CAPTURE_URL;
            params.put("PREAUTH_ID", String.valueOf(preAuthId));
        }

        params.put("CheckSum", generateChecksum(params));
        String url = String.format("%s%s", PAYTM_TXN_URL, apiPath);
        URIBuilder ub = new URIBuilder(url);
        String paramsString = objectMapper.writeValueAsString(params);
        log.info("paramsString: {}", paramsString);
        ub.addParameter("JsonData", paramsString);
        String apiUrl = String.valueOf(ub.build());
        Client client = ClientUtil.getClient();
        String result = null;
        try {
            insertAnimalKingdom(orderId, customer.getId(), paramsString, "request", "withdrawApi", apiUrl);

        } catch (Exception e){
            log.info("Error occured while inserting in animal kingdom in withdrawApi request");
        }

        try {
            result = client.target(apiUrl)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);
            log.info("witdrawApi Response[raw] for orderId: {} is {}", orderId, result);
            try {
                insertAnimalKingdom(orderId, customer.getId(), result, "response", "withdrawApi", apiUrl);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in withdrawApi response");
            }
            return result;
        } catch (Exception e) {
            log.error("Error happend in withdraw: ", e);
            throw e;
        }
    }


    public String refundPaytm(String orderId, double amount, String TxnId, long userId){
        try {
            JSONObject body = new JSONObject();
            body.put("mid", MID);
            body.put("txnType", "REFUND");
            body.put("orderId", orderId);
            body.put("txnId", TxnId);
            body.put("refId", Generators.timeBasedGenerator().generate().toString());
            body.put("refundAmount", String.valueOf(amount));

            JSONObject head = new JSONObject();
            String checksum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(MERCHANT_KEY, body.toString());
            head.put("clientId", "C11");
            head.put("signature", checksum);
            JSONObject params = new JSONObject();
            params.put("body", body);
            params.put("head", head);

            String url = String.format("%s%s", PAYTM_TXN_URL, PAYTM_REFUND);
            URIBuilder ub = new URIBuilder(url);
            Client client = ClientUtil.getClient();
            String apiUrl = String.valueOf(ub.build());
            try {
                insertAnimalKingdom(orderId, userId, String.valueOf(params), "request", "refundPaytm", url);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in refundPaytm request");
            }
            String result = client.target(apiUrl)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("Content-Type", "application/json")
                    .post(Entity.json(params), String.class);
            log.info("apiUrl :{}, params: {}", apiUrl, params);
            log.info("result of refund api paytm: {}", result);
            try {
                insertAnimalKingdom(orderId, userId, result, "response", "refundPaytm", url);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in refundPaytm response");
            }
            return result;
        } catch (Exception e) {
            throw new CustomException("refund amount failed", 600);
        }

    }
    private PaytmTransaction withdrawForBookingWrapper(Customer customer, long bookingId, String type, double amount, String preAuthId, String callType) throws Exception {
//        String callType = "A2B";
        String orderId = String.format("%s_%s", bookingId, Generators.timeBasedGenerator().generate());
        String apiResponse = "{}";
        PaymentTransactionLog paymentTransactionLog = LoggingUtil.insertIntoPaymentTransactionLog(paymentTrasactionLogRepository, customer.getId(), bookingId, amount, orderId, PaymentMethod.paytm.toString(), callType, type);
        try {
            apiResponse = withdrawApi(customer, orderId, amount, "WITHDRAW", preAuthId);
        } catch (Exception e) {
            log.error("Exception in withdrawForBooking", e);
            throw new Exception("Exception in withdrawForBooking");
        } finally {
            WithdrawAmountResponse withdrawAmountResponse =
                    objectMapper.readValue(apiResponse, WithdrawAmountResponse.class);
            //todo change calltype and method
            PaytmTransaction paytmTransaction = insertIntoPaytmTransaction(bookingId, customer, type, callType, orderId, amount, apiResponse, withdrawAmountResponse);
            String responseCode = withdrawAmountResponse.getResponseCode();
            String responseStatus = withdrawAmountResponse.getStatus();
            updatePaymentTransactionLog(paymentTransactionLog, responseStatus);
            if (responseCode != null && !responseCode.isEmpty()) {
                if ("1101".equals(responseCode)) {
                    throw new CustomException("Currently Paytm servers are down, please try after sometime", 604);
                } else if ("235".equals(responseCode)) {
                    throw new CustomException("Insufficient balance in paytm account. Please add money", 603);
                } else if ("343".equals(responseCode)) {
                    throw new CustomException("Paytm token expired for the user", 601);
                }
            }

            if (responseStatus == null) {
                throw new CustomException("Paytm: Internal Error", 600);
            } else if (TXN_FAILURE.equals(responseStatus)) {
                log.info("txn failed: {}", withdrawAmountResponse);
                throw new CustomException("Paytm withdrawal transaction failed", 600);
            }
            return paytmTransaction;
        }
    }

    private void updatePaymentTransactionLog(PaymentTransactionLog paymentTransactionLog, String responseStatus) {
        if(TXN_SUCCESS.equals(responseStatus)){
            paymentTransactionLog.setStatus("success");
        } else {
            paymentTransactionLog.setStatus("failed");
        }
        paymentTrasactionLogRepository.save(paymentTransactionLog);
    }

    public void captureForBooking(Customer customer, long bookingId, double amount, String preAuthId) throws Exception {
        withdrawForBookingWrapper(customer, bookingId, CAPTURE, amount, preAuthId, "A2B");
    }

    public PaytmTransaction withdrawForBooking(Customer customer, long bookingId, double amount, String type, String callType) throws Exception {
        return withdrawForBookingWrapper(customer, bookingId, type, amount, null, callType);
    }

    @Transactional
    public PaytmTransaction insertIntoPaytmTransaction(long bookingId, Customer customer, String type, String callType,
                                            String orderId, double transAmount, String apiResponse,
                                            WithdrawAmountResponse withdrawAmountResponse) {
        boolean result = false;
        if (TXN_SUCCESS.equals(withdrawAmountResponse.getStatus())) {
            result = true;
        }

        final PaytmTransaction paytmTransaction = PaytmTransaction.builder().build();
        paytmTransaction.setBookingId(bookingId);
        paytmTransaction.setCustomer(customer);
        paytmTransaction.setType(type);
        paytmTransaction.setCallType(callType);
        paytmTransaction.setOrderId(orderId);
        paytmTransaction.setPaytmToken(customer.getPaytmToken());
        paytmTransaction.setPaytmTransactionId(withdrawAmountResponse.getPaytmTransactionId());
        paytmTransaction.setBankTransactionId(withdrawAmountResponse.getBankTransactionId());
        paytmTransaction.setMethod("paytm");
        paytmTransaction.setTransAmount(transAmount);
        paytmTransaction.setResult(result);
        //TODO: fix this
//        paytmTransaction.setApiResponse(apiResponse);
        paytmTransaction.setCreatedAt(new Date());
        paytmTransaction.setUpdatedAt(new Date());
        paytmTransactionRepository.save(paytmTransaction);
        return paytmTransaction;
    }

    public double getBalance(String paytmToken, Date paytmTokenExpiry, long userId) throws CustomException {
        if (paytmToken == null) {
            throw new CustomException("Paytm not linked for the user", 602);
        }
        if(paytmTokenExpiry.before(new Date())) {
            throw new CustomException("Paytm token expired for the user", 601);
        }


        String url = String.format("%s%s", PAYTM_TXN_URL, PAYTM_WALLET_BALANCE);

        try {
            URIBuilder ub = new URIBuilder(url);
            String apiUrl = String.valueOf(ub.build());

            Client client = ClientUtil.getClientBasedOnContext();

            String payload = String.format("{\"merchantGuid\":\"%s\", \"mid\":\"%s\"}", PAYTM_CLIENT_SECRET, PAYTM_MID);

            try {
                insertAnimalKingdom(paytmToken, userId, payload, "request", "getBalance", apiUrl);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in balance request");
            }
            log.info("paytm getBalance apiUrl: {}", apiUrl);
            String result = client.target(apiUrl)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("ssotoken", paytmToken)
                    .post(Entity.entity(payload, MediaType.APPLICATION_JSON_TYPE), String.class);
            log.info("paytm getBalanceResponse[raw]: {}", result);
            WalletBalanceResponse response =
                    objectMapper.readValue(result, WalletBalanceResponse.class);
            try {
                insertAnimalKingdom(paytmToken, userId, String.valueOf(response), "response", "getBalance", apiUrl);

            } catch (Exception e){
                log.info("Error occured while inserting in animal kingdom in balance response");
            }
            return response.getResponse().getPaytmWalletBalance();
        } catch (ForbiddenException e) {
            log.error("[PaytmApi]: Get Wallet Balance error. Forbidden Exception", e);
            throw new CustomException("Paytm token expired for the user", 601);
        } catch (Exception e) {
            log.error("error happend", e);
            throw new CustomException("Paytm Internal Error: Wallet balance check failed", 600);
        }
    }

    private void insertAnimalKingdom(String paymentId, double userId, String options, String species, String ord, String url) {
        AnalyticsDumpDto analyticsDumpDto = AnalyticsDumpDto.builder()
                .kingdom("mb_python")
                .phylum("payments")
                .family("paytm")
                .species(species)
                .order(ord)
                .valstring(url)
                .genusNum(userId)
                .className(paymentId)
                .tribe(options)
                .build();
        TrackingDumpUtil.dumpIntoAnimalKingdom(analyticsDumpDto);
    }
}
