package com.bounce.juspay;


import com.bounce.dtos.AnalyticsDumpDto;
import com.bounce.dtos.juspay.CreateCustomerResponse;
import com.bounce.dtos.juspay.CreateOrderResponse;
import com.bounce.dtos.juspay.JuspayRefundResponseDto;
import com.bounce.dtos.juspay.OrderStatusResponseDto;
import com.bounce.entity.User;
import com.bounce.exceptions.CustomException;
import com.bounce.utils.TrackingDumpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@Named
public class JuspayApi {

    @Value("${juspay.api.key}")
    private String JUSYPAY_API_KEY;

    @Value("${juspay.base.url}")
    private String JUSPAY_BASE_URL;

    private static final String CUSTOMER = "/customers";
    private static final String WALLETS = "/wallets";
    private static final String ORDER = "/orders";
    private static final String REFUND = "/refunds";
    private static final String JUSPAY_API_VERSION = "2018-10-25";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public CreateCustomerResponse createCustomer(User user, String custId){
        try {
            String apiUrl = String.format("%s%s", JUSPAY_BASE_URL, CUSTOMER);
            Client client = ClientBuilder.newClient();
            log.info("apiUrl : {}",apiUrl);

            Form form = new Form();
            form.param("object_reference_id", custId);
            form.param("mobile_number", user.getMobileNumber());
            form.param("email_address", user.getEmailId());

            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");

            String result = client.target(apiUrl)
                    .register(feature)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .post(Entity.form(form), String.class);

            log.info("result from createCustomer juspay: {}", result);
            return objectMapper.readValue(result, CreateCustomerResponse.class);
        } catch (Exception e){
            log.info("error in createCustomer: ",e);
            throw new CustomException("error in createCustomer");
        }
    }

    public String listWallet(String custId){
        try {

            String apiUrl = String.format("%s%s/%s%s", JUSPAY_BASE_URL, CUSTOMER, custId, WALLETS);
            Client client = ClientBuilder.newClient();
            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");

            String result = client.target(apiUrl)
                    .register(feature)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);

            log.info("result of listWallets: {}", result);
            return result;
        } catch (Exception e){
            log.info("error in listWallets: ",e);
            throw new CustomException("error in listWallets");

        }
    }

    public String refreshWallet(String walletId){
        try{

            String apiUrl = String.format("%s%s/%s", JUSPAY_BASE_URL, WALLETS, walletId);
            Client client = ClientBuilder.newClient();

            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");

            String result = client.target(apiUrl)
                    .register(feature)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);
            log.info("result of refreshWallet: {}", result);
            return result;

        } catch (Exception e){
            log.info("error in refreshWallet: ",e);
            throw new CustomException("error in refreshWallet");
        }
    }

    public CreateOrderResponse createOrder(long userId, String orderId, double amount, String custId, Map<String, Object> udf){
        try{
            String apiUrl = String.format("%s%s", JUSPAY_BASE_URL, ORDER);
            log.info("apiUrl is: {}",apiUrl);

            Client client = ClientBuilder.newClient();
            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");


            Map<String, Object> params = new HashMap<String, Object>();
            params.put("order_id", orderId);
            params.put("amount", amount);
            params.put("customer_id", custId);
            params.put("options.get_client_auth_token", true);
            params.put("udf6", udf.toString());

            DumpAnimalKingdom(userId,"request", orderId, custId, apiUrl, String.valueOf(params), "create_order");

            String result = client.target(apiUrl)
                    .register(feature)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("version", JUSPAY_API_VERSION)
                    .post(Entity.json(params), String.class);
            log.info("result from createOrder juspay: {}", result);
            DumpAnimalKingdom(userId, "response", orderId, custId, apiUrl, result, "create_order");
            return objectMapper.readValue(result, CreateOrderResponse.class);
        }catch (Exception e){
            log.info("error in createOrder juspay: ",e);
            throw new CustomException("Error creating order");
        }
    }

    private void DumpAnimalKingdom(long userId, String species, String orderId, String custId, String apiUrl, String tribe, String method) {
        try {
            AnalyticsDumpDto analyticsDumpDto = AnalyticsDumpDto.builder()
                .kingdom("mb_python")
                .phylum("payments")
                .family("juspay")
                .species(species)
                .order(method)
                .genusNum(Double.valueOf(userId))
                .className(orderId)
                .tribe(tribe)
                .valstring(apiUrl)
                .str1(custId)
                .build();
            TrackingDumpUtil.dumpIntoAnimalKingdom(analyticsDumpDto);
        } catch (Exception e){
            log.info("error in inserting into analytics");
        }
    }

    public void createWallet(String custId, String gateway){

        String apiUrl = String.format("%s%s/%s%s", JUSPAY_BASE_URL, CUSTOMER, custId, WALLETS);
        log.info("apiUrl: {}",apiUrl);
        Client client = ClientBuilder.newClient();
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");

        Map<String, Object> params = new LinkedHashMap();
        params.put("gateway", gateway);

        String result = client.target(apiUrl)
                .register(feature)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(Entity.json(params), String.class);
        log.info("result from createWallet juspay: {}", result);
    }

    public JuspayRefundResponseDto refundOrder(double amount, String orderId, String uniqueRequestId, long userId) {
        try {

            String apiUrl = String.format("%s%s/%s%s", JUSPAY_BASE_URL, ORDER, orderId, REFUND);
            log.info("apiUrl: {}", apiUrl);
            Client client = ClientBuilder.newClient();
            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");

            Form form = new Form();
            form.param("unique_request_id", uniqueRequestId);
            form.param("amount", String.valueOf(amount));

            DumpAnimalKingdom(userId, "request", orderId, null, apiUrl, String.valueOf(form), "refund_order");
            String result = client.target(apiUrl)
                    .register(feature)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("version", JUSPAY_API_VERSION)
                    .post(Entity.form(form), String.class);
            log.info("result from refundOrder juspay: {}", result);
            DumpAnimalKingdom(userId, "response", orderId, null, apiUrl, result, "refund_order");
            return objectMapper.readValue(result, JuspayRefundResponseDto.class);
        } catch (Exception e){
            log.info("error in refunding juspay: ",e);
            throw new CustomException("Error refunding order");
        }

    }

    public OrderStatusResponseDto orderStatus(String orderId){
        try {

            String apiUrl = String.format("%s%s/%s", JUSPAY_BASE_URL, ORDER, orderId);
            log.info("apiUrl: {}", apiUrl);
            Client client = ClientBuilder.newClient();

            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");
            String result = client.target(apiUrl)
                    .register(feature)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);
            log.info("result from orderStatus juspay: {}", result);
            return objectMapper.readValue(result, OrderStatusResponseDto.class);
        } catch (Exception e){
            log.info("error in orderStatus juspay: ",e);
            throw new CustomException("Error getting Status");
        }
    }

    public void linkWallet(String command, Integer otp, String walletId) {
        try {
            String apiUrl = String.format("%s%s/%s", JUSPAY_BASE_URL, WALLETS, walletId);
            log.info("apiUrl: {}", apiUrl);
            Client client = ClientBuilder.newClient();

            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(JUSYPAY_API_KEY, "");

            Map<String, Object> params = new LinkedHashMap();
            params.put("command", command);
            if(otp != null){
                params.put("otp", otp);
            }
            String result = client.target(apiUrl)
                    .register(feature)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .post(Entity.json(params), String.class);
            log.info("result from linkWallet: {}", result);
        } catch (Exception e) {
            log.info("error in linkWallet: ",e);
            throw new CustomException("error in linkWallet");
        }
    }

}
