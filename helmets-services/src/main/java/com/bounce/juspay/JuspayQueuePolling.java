package com.bounce.juspay;

import com.bounce.JuspayService;
import com.bounce.WalletTopupService;
import com.bounce.cash.BounceCashApi;
import com.bounce.dtos.cash.BounceCashDeductionInternalData;
import com.bounce.dtos.cash.BounceCashDeductionResponse;
import com.bounce.dtos.cash.BounceCashGetRemittableAmount;
import com.bounce.dtos.juspay.JuspayInternalOrderDto;
import com.bounce.dtos.juspay.JuspayInternalUdfNotes;
import com.bounce.dtos.juspay.WebhookRequestDto;
import com.bounce.entity.*;
import com.bounce.entity.enums.FlowType;
import com.bounce.entity.enums.PostpaidStatus;
import com.bounce.exceptions.CustomException;
import com.bounce.helper.GlobalConfigService;
import com.bounce.repository.*;
import com.bounce.utils.BillingUtil;
import com.bounce.utils.LoggingUtil;
import com.bounce.utils.SQS.SQSQueueTxns;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.uuid.Generators;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Date;

@Named
@Slf4j
public class JuspayQueuePolling {
    private final String CHARGED = "CHARGED";
    private final String ORDER_FAILED = "ORDER_FAILED";
    private final String TXN_CREATED = "TXN_CREATED";

    @Value("${juspay.fifoqueue}")
    private String JUSPAY_QUEUE_NAME;


    @Inject private JuspayOrderRepository juspayOrderRepository;
    @Inject private CustomerRepository customerRepository;
    @Inject private UserRepository userRepository;
    @Inject private BounceCashApi bounceCashApi;
    @Inject private GlobalConfigService globalConfigService;
    @Inject private PaidHelmetRepository paidHelmetRepository;
    @Inject private WalletTopupService walletTopupService;
    @Inject private WalletTopupRepository walletTopupRepository;
    @Inject private PaytmTransactionRepository paytmTransactionRepository;
    @Inject private DuesLedgerRepository duesLedgerRepository;
    @Inject private JuspayService juspayService;
    @Inject private SQSQueueTxns sqsQueueTxns;


    public void longPollingForJuspay(){

        sqsQueueTxns.setupLongPolling(JUSPAY_QUEUE_NAME);
    }


    public void processJuspayTransactions(WebhookRequestDto webhookRequestDto){

        ObjectMapper objectMapper = new ObjectMapper(); //todo make this object mapper global
        JsonObject udfNotesJson = new JsonParser().parse(webhookRequestDto.getContent().getOrder().getUdf()).getAsJsonObject();

        try {
            JuspayInternalUdfNotes juspayInternalUdfNotes = objectMapper
                    .readValue(udfNotesJson.toString(), JuspayInternalUdfNotes.class);

            webhookRequestDto.getContent().getOrder().setUdfNotes(juspayInternalUdfNotes);

        } catch (IOException e) {
            log.info("error in object mapping");
        }

        log.info("user_id :{}", webhookRequestDto.getContent().getOrder().getUdfNotes().getUserId());
        JuspayInternalOrderDto orderDetails = webhookRequestDto.getContent().getOrder();
        String orderId = orderDetails.getOrderId();
        JuspayOrders juspayOrders= this.juspayOrderRepository.findFirstByOrderIdOrderByIdDesc(orderId);

        //todo update the juspay_orders  after not null.

        if(juspayOrders == null){
            throw new CustomException("no order present with the order id");
        }

        User user = this.userRepository.findById(orderDetails.getUdfNotes().getUserId())
                .orElseThrow(()-> new IllegalArgumentException("No user present for the userId"));

        Customer customer = this.customerRepository.findById(orderDetails.getUdfNotes().getUserId())
                .orElseThrow(()-> new IllegalArgumentException("No user present for the userId"));

        if(CHARGED.equals(orderDetails.getTxnDetails().getJuspayOrderStatus())){
            juspayOrders = updateJuspayOrders(orderDetails, juspayOrders);
            FlowType flowType = orderDetails.getUdfNotes().getFlowType();

            if(FlowType.CLEAR_DUES.equals(flowType)){
                if(canClearDues(orderDetails.getAmount(), orderDetails.getUdfNotes().getBounceCashUsed(),customer, user)){

                    clearDues(customer, orderDetails.getUdfNotes().getBounceCashUsed(), juspayOrders, orderDetails.getAmount(), user);

                } else {
                    //todo refund amount log reason
                    juspayService.serverFailedJuspayTxnsRefund(orderId, orderDetails.getAmount(), juspayOrders, "Not eligible to clear dues");
                    juspayOrders.setOrderStatus("Failed");
                    juspayOrders.setRefundStatus(true);
                    this.juspayOrderRepository.save(juspayOrders);

                }
            } else if(FlowType.POSTPAID.equals(flowType)){
                if(customer.getPostpaidStatus() == null || PostpaidStatus.not_linked.equals(customer.getPostpaidStatus())){
                    linkPostpaid(customer, orderDetails.getAmount(), juspayOrders);
                } else {
                    //todo refund amount.
                    String remarks = String.format("failed because postpaid status :%s",customer.getPostpaidStatus());
                    juspayService.serverFailedJuspayTxnsRefund(orderId, orderDetails.getAmount(), juspayOrders, remarks);
                    juspayOrders.setOrderStatus("Failed");
                    juspayOrders.setRefundStatus(true);
                    this.juspayOrderRepository.save(juspayOrders);
                }

            } else if(FlowType.WALLET_TOPUP.equals(flowType)){
                long offerId = orderDetails.getUdfNotes().getOfferId();
                walletTopupHelper(orderDetails, user, offerId, juspayOrders, customer);


            } else if(FlowType.PAID_HELMET.equals(flowType)){
                paidHelmetHelper(orderDetails, customer, juspayOrders);

            } else {
                //todo refund amount.
                String remarks = String.format("flowType is not supported :%s", flowType);
                juspayService.serverFailedJuspayTxnsRefund(orderId, orderDetails.getAmount(), juspayOrders, remarks);
                juspayOrders.setOrderStatus("Failed");
                juspayOrders.setRefundStatus(true);
                this.juspayOrderRepository.save(juspayOrders);
            }

        } else if (ORDER_FAILED.equals(webhookRequestDto.getEventName())){
            juspayOrders.setJuspayOrderStatus(orderDetails.getTxnDetails().getJuspayOrderStatus());
            juspayOrders.setOrderStatus("Failed");
            this.juspayOrderRepository.save(juspayOrders);

        } else if (TXN_CREATED.equals(webhookRequestDto.getEventName())){
            //todo handle this case.
            juspayOrders = updateJuspayOrders(orderDetails, juspayOrders);
        }

    }

    @Transactional
    public JuspayOrders updateJuspayOrders(JuspayInternalOrderDto orderDetails, JuspayOrders juspayOrders) {
        juspayOrders.setJuspayOrderStatus(orderDetails.getTxnDetails().getJuspayOrderStatus());
        juspayOrders.setTxnId(orderDetails.getTxnDetails().getTxnId());
        juspayOrders.setOrderStatus("Pending");
        this.juspayOrderRepository.save(juspayOrders);
        return juspayOrders;
    }

    private void paidHelmetHelper(JuspayInternalOrderDto orderDetails, Customer customer, JuspayOrders juspayOrders) {
        PaidHelmet paidHelmet = this.paidHelmetRepository.findByUserId(customer.getId());
        if(paidHelmet == null){
            paidHelmet = LoggingUtil.insertIntoHelmet(paidHelmetRepository, customer.getId(), orderDetails.getAmount(), orderDetails.getUdfNotes().getPinCode(),
                    orderDetails.getUdfNotes().getAddress(), PostpaidStatus.not_linked);
        }
        if (paidHelmet != null) {
            if(paidHelmet.getPostpaidStatus() == null || PostpaidStatus.not_linked.equals(paidHelmet.getPostpaidStatus())){
                paidHelmet.setAmount(orderDetails.getAmount());
                paidHelmet.setAddress(orderDetails.getUdfNotes().getAddress());
                paidHelmet.setPincode(orderDetails.getUdfNotes().getPinCode());
                this.paidHelmetRepository.save(paidHelmet);
                if(BillingUtil.compareTo(globalConfigService.getHelmetPostpaidDepositAmount(), orderDetails.getAmount()) == 0){
                    paidHelmet.setPostpaidStatus(PostpaidStatus.linked);
                    paidHelmet.setPaidDate(new Date());
                    this.paidHelmetRepository.save(paidHelmet);
                }
                //todo paytm_transaction entry.
                long orderId = 2794;
                String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
                PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId, customer, "DEPOSIT", "postpaid_helmet", orderIdRand,
                        juspayOrders.getOrderId(), "juspay", orderDetails.getAmount(), true, null);
                juspayOrders.setOrderStatus("Success");
                this.juspayOrderRepository.save(juspayOrders);

            }else{
                //todo refund the amount.
                String remarks = String.format("failed as paid_helmet status is :%s", paidHelmet.getPostpaidStatus());
                juspayService.serverFailedJuspayTxnsRefund(orderDetails.getOrderId(), orderDetails.getAmount(), juspayOrders, remarks);
                juspayOrders.setOrderStatus("Failed");
                juspayOrders.setRefundStatus(true);
                this.juspayOrderRepository.save(juspayOrders);
            }
        } else {
            String remarks = String.format("failed to insert into PaidHelmet");
            juspayService.serverFailedJuspayTxnsRefund(orderDetails.getOrderId(), orderDetails.getAmount(), juspayOrders, remarks);
            juspayOrders.setOrderStatus("Failed");
            juspayOrders.setRefundStatus(true);
            this.juspayOrderRepository.save(juspayOrders);
        }
    }

    private void walletTopupHelper(JuspayInternalOrderDto orderDetails, User user, long offerId, JuspayOrders juspayOrders, Customer customer) {
        if(BillingUtil.compareTo(offerId, 0) > 0) {
            double walletValue = this.walletTopupService.getWalletTopupValue(orderDetails.getAmount(), offerId);
            if(BillingUtil.compareTo(walletValue, 0) !=0) {

                WalletTopup walletTopup = LoggingUtil.insertIntoWalletTopup(walletTopupRepository, user.getId(), orderDetails.getAmount(),
                        offerId, orderDetails.getOrderId() ,walletValue);
                String phpResponse = null;
                try {
                    phpResponse = bounceCashApi.triggerWalletTopupApi(walletValue, user.getMobileNumber(), juspayOrders.getOrderId(), user.getId());
                } catch (Exception e){
                    String remarks = "error triggerWalletTopupApi";
                    juspayService.serverFailedJuspayTxnsRefund(orderDetails.getOrderId(), orderDetails.getAmount(), juspayOrders, remarks);
                    juspayOrders.setOrderStatus("Failed");
                    juspayOrders.setRefundStatus(true);
                    this.juspayOrderRepository.save(juspayOrders);
                    return;
                }
                long orderId = 2794;
                String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
                PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId, customer, "DEPOSIT", "wallet_topup", orderIdRand,
                        juspayOrders.getOrderId(), "juspay", orderDetails.getAmount(), true, null);
                LoggingUtil.updateWalletTopup(walletTopupRepository, walletTopup, phpResponse);

                juspayOrders.setOrderStatus("Success");
                this.juspayOrderRepository.save(juspayOrders);

            } else {
                //todo refund amount
                String remarks = "wallet value is 0";
                juspayService.serverFailedJuspayTxnsRefund(orderDetails.getOrderId(), orderDetails.getAmount(), juspayOrders, remarks);
                juspayOrders.setOrderStatus("Failed");
                juspayOrders.setRefundStatus(true);
                this.juspayOrderRepository.save(juspayOrders);
            }

        } else {
            //todo refund
            String remarks = String.format("OfferId is negitive: %s", offerId);
            juspayService.serverFailedJuspayTxnsRefund(orderDetails.getOrderId(), orderDetails.getAmount(), juspayOrders, remarks);
            juspayOrders.setOrderStatus("Failed");
            juspayOrders.setRefundStatus(true);
            this.juspayOrderRepository.save(juspayOrders);
        }
    }


    private void clearDues(Customer customer, double bounceCashUsed, JuspayOrders juspayOrders, double juspayAmount, User user){
        long orderId = 2792;
        if (BillingUtil.compareTo(bounceCashUsed, 0) == 0 || clearDuesViaBounceCash(user.getToken(), bounceCashUsed, customer, orderId, user)){
            double totalAmountDue = customer.getTotalAmountDue();
            customer.setTotalAmountDue(BillingUtil.subtract(totalAmountDue, juspayAmount));
            customer.setDuesUpdatedOn(new Date());
            customerRepository.save(customer);

            String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
            PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId, customer, "WITHDRAW", "dues", orderIdRand,
                    juspayOrders.getOrderId(), "juspay", juspayAmount, true, null);

            LoggingUtil.logDuesLedger(duesLedgerRepository, customer.getId(), juspayAmount, customer.getTotalAmountDue(), "juspay", paytmTransaction.getId());
            juspayOrders.setOrderStatus("Success");
            this.juspayOrderRepository.save(juspayOrders);
        } else {
            //todo refund amount
            String remarks = "clearing dues through bouceCash failed";
            juspayService.serverFailedJuspayTxnsRefund(juspayOrders.getOrderId(), juspayAmount, juspayOrders, remarks);
            juspayOrders.setOrderStatus("Failed");
            juspayOrders.setRefundStatus(true);
            this.juspayOrderRepository.save(juspayOrders);
        }

    }


    private boolean clearDuesViaBounceCash(String token, double bounceCashUsed, Customer customer, long orderId, User user) {

        double remmitableAmount = 0;
        double dues = customer.getTotalAmountDue();
        try {
            BounceCashGetRemittableAmount bounceCashGetRemittableAmount = bounceCashApi.getRemittableAmount(user.getMobileNumber(), "D");
            remmitableAmount = bounceCashGetRemittableAmount.getResult().getData().getRemittableAmount();
        } catch (Exception e){
            log.info("error in getting remittable amount", e);
            return false;
        }
        double clearAmount = BillingUtil.min(bounceCashUsed, remmitableAmount);
        //todo  Note : if clearAmount = remmitableAmount then partially dues will be cleared.
        log.info("bounceCashUsed:{}", bounceCashUsed);

        if(clearAmount <= 0){
            return false;
        }
        try {
            BounceCashDeductionResponse bounceCashDeductionResponse = this.bounceCashApi
                    .deductNonPromotionalBalance(token, clearAmount);
            BounceCashDeductionInternalData bounceWallet = bounceCashDeductionResponse.getResult().getData()[0];
            log.info("result from php: {} ", bounceWallet);
            long paymentRefId = bounceWallet.getWalletTxnId();

            customer.setTotalAmountDue(BillingUtil.subtract(dues, clearAmount)); //todo check this statement
            customer.setDuesUpdatedOn(new Date());
            customerRepository.save(customer);

            String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
            PaytmTransaction paytmTransaction = LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId, customer, "WITHDRAW", "dues", orderIdRand,
                    String.valueOf(paymentRefId), "wallet", clearAmount, true, null);
            LoggingUtil.logDuesLedger(duesLedgerRepository, customer.getId(), clearAmount, customer.getTotalAmountDue(), "wallet", paytmTransaction.getId());
            return true;
        } catch (Exception e){
            log.info("error in deducting amount from bouncecash: ", e);
            return false;
        }
    }


    private boolean canClearDues(double amount, double bounceCashUsed, Customer customer, User user){

        double dues = customer.getTotalAmountDue();
        double walletBalance ;
        if(BillingUtil.compareTo(bounceCashUsed, 0) != 0){
            walletBalance = bounceCashApi.getBalance(user.getToken()).getResult().getData().getNonPromotionalBalance();
            if(BillingUtil.compareTo(walletBalance, dues) != -1){
                return false;
            }
        }
        double totalAmount = BillingUtil.add(amount, bounceCashUsed);
        if(BillingUtil.compareTo(dues, totalAmount) == 0){
            return true;
        } else {
            return false;
        }
    }

    private void linkPostpaid(Customer customer, double depositAmount, JuspayOrders juspayOrders){

        if (depositAmount >= globalConfigService.getPostpaidSecurityAmount()){
            customer.setPostpaidDepositAmount(depositAmount);
            customer.setPostpaidStatus(PostpaidStatus.linked);
            log.info("postpaidDepositAmount: {} in",depositAmount);
            this.customerRepository.save(customer);
        }
        //todo update in order table.
        long orderId = 2793;
        String orderIdRand = String.format("%s_%s", orderId, Generators.timeBasedGenerator().generate());
        try {
            LoggingUtil.inserIntoPaytmTransaction(paytmTransactionRepository, orderId, customer, "DEPOSIT", "postpaid", orderIdRand,
                    juspayOrders.getOrderId(), "juspay", depositAmount, true, null);
        } catch (Exception e){
            log.info("saving to transaction failed.");
        }
        juspayOrders.setOrderStatus("Success");
        this.juspayOrderRepository.save(juspayOrders);
    }


}
