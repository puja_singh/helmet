package com.bounce;

import com.bounce.entity.Booking;

public interface PaymentService {

    boolean canProcess(Booking booking, String userToken);
}
