package com.bounce.dtos.juspay;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class JuspayInternalContentDto {

    @JsonProperty("order")
    private JuspayInternalOrderDto order;
}
