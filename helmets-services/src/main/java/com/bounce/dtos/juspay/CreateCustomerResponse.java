package com.bounce.dtos.juspay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateCustomerResponse {

    @JsonProperty("object_reference_id")
    private String objectReferenceId;

    @JsonProperty("mobile_number")
    private String preauthId;

    @JsonProperty("id")
    private String id;

}
