package com.bounce.dtos.juspay;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPOJOBuilder
public class WebhookRequestDto {

    @JsonProperty("event_name")
    private String eventName;

    @JsonProperty("content")
    private JuspayInternalContentDto content;

}
