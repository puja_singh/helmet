package com.bounce.dtos.juspay;

import com.bounce.entity.enums.FlowType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class JuspayInternalUdfNotes {

    @JsonProperty("flow_type")
    private FlowType flowType;

    @JsonProperty("bounce_cash_used")
    private Double bounceCashUsed;

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("offer_id")
    private Long offerId;

    @JsonProperty("address")
    private String address;

    @JsonProperty("pincode")
    private Integer pinCode;


}
