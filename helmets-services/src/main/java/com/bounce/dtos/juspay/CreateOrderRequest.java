package com.bounce.dtos.juspay;

import com.bounce.entity.enums.FlowType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateOrderRequest {

    @JsonProperty("amount")
    private double amount;

    @JsonProperty("cust_id")
    private String custId;

    @JsonProperty("flow_type")
    private FlowType flowType;

    @JsonProperty("bounce_cash_used")
    private double bounceCashUsed;

    @JsonProperty("payment_method")
    private String paymentMethod;

    @JsonProperty("payment_method_type")
    private String paymentMethodType;

    @JsonProperty("offer_id")
    private Long offerId;

    @JsonProperty("address")
    private String address;

    @JsonProperty("pincode")
    private Long pinCode;

}
