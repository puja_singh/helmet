package com.bounce.dtos.juspay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class JuspayInternalTxnDetails {

    @JsonProperty("status")
    private String juspayOrderStatus;

    @JsonProperty("txn_id")
    private String txnId;

    @JsonProperty("txn_uuid")
    private String txnUUId;

    @JsonProperty("txn_amount")
    private String txnAmount; //todo check which amount will be used for taking the txn amount.

}
