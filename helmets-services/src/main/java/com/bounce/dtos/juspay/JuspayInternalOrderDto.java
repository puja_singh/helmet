package com.bounce.dtos.juspay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class JuspayInternalOrderDto {


    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("txn_detail")
    private JuspayInternalTxnDetails txnDetails;

    @JsonProperty("cust_id")
    private String custId;

    @JsonProperty("udf6")
    private String udf;

    private JuspayInternalUdfNotes udfNotes;

}
