package com.bounce.dtos;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.razorpay.Payment;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentMethodDetailsV2 extends PaymentMethodDetailsV1 {

    @JsonProperty("display")
    private PaymentDisplayDetails paymentDisplayDetails;

    @JsonProperty("balance")
    private Double balance;

    public PaymentMethodDetailsV2(PaymentMethodDetailsV1 paymentMethodDetailsV1,
                                  PaymentDisplayDetails paymentDisplayDetails, Double balance) {
        this.setLinkedStatus(paymentMethodDetailsV1.getLinkedStatus());
        this.setDepositAmount(paymentMethodDetailsV1.getDepositAmount());
        this.setPaymentMethod(paymentMethodDetailsV1.getPaymentMethod());
        this.paymentDisplayDetails = paymentDisplayDetails;
        this.balance = balance;
    }

}
