package com.bounce.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
public class PaymentDisplayDetails {

    @JsonProperty("title")
    private String title;

    @JsonProperty("main_string")
    private String mainString;

    @JsonProperty("substring1")
    private String subString1;

    @JsonProperty("substring2")
    private String subString2;

    @JsonProperty("cta_text")
    private String ctaText;

    @JsonProperty("highlighter")
    private String highlighter;
}