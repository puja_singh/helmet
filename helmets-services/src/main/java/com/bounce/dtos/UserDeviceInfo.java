package com.bounce.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserDeviceInfo {
    private String platform;
    private String appVersion;
}
