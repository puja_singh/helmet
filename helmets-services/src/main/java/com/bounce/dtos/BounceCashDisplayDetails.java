package com.bounce.dtos;

public class BounceCashDisplayDetails extends PaymentDisplayDetails {
    private static final String TITLE = "Bounce Cash";
    private static final String MAIN_STRING = "Recharge & get unlimited benefits";
    private static final String SUB_STRING_1 = "Quick & automatic payments";
    private static final String SUB_STRING_2 = "Hassle free payment & cashbacks";
    private static final String CTA_TEXT = "RECHARGE";
    private static final String HIGHLIGHTER = "Bounce Cash is automatically deducted";

    public BounceCashDisplayDetails(){
        super(TITLE, MAIN_STRING, SUB_STRING_1, SUB_STRING_2, CTA_TEXT, HIGHLIGHTER);
    }

}
