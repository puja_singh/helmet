package com.bounce.dtos;

public class PostpaidDisplayDetails extends PaymentDisplayDetails {
    private static final String TITLE = "Pay After Ride";
    private static final String MAIN_STRING = "Ride now and Pay later at ease";
    private static final String SUB_STRING_1 = "This is one time deposit";
    private static final String SUB_STRING_2 = "Withdraw it anytime";
    private static final String CTA_TEXT = "Unlock Now";
    private static final String HIGHLIGHTER = null;

    public PostpaidDisplayDetails(){
        super(TITLE, MAIN_STRING, SUB_STRING_1, SUB_STRING_2, CTA_TEXT, HIGHLIGHTER);
    }
}