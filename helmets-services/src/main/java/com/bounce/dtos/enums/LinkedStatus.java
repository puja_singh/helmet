package com.bounce.dtos.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LinkedStatus {
    linked("linked"), not_linked("not_linked"), refund_initiated("refund_initiated");

    private String code;
}

