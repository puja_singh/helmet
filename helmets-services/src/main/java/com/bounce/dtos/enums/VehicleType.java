package com.bounce.dtos.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public enum VehicleType {

    cycle, bike;

//    private String code;
}
