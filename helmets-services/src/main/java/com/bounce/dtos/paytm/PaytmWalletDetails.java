package com.bounce.dtos.paytm;

import com.bounce.entity.Customer;
import com.bounce.entity.User;
import com.bounce.entity.enums.PaymentMethod;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class PaytmWalletDetails {

    private Boolean isTokenValid;
    private PaymentMethod paymentMethod;
    private Customer customer;
}
