package com.bounce.dtos.paytm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class WalletBalanceResponse {

    @JsonProperty("response")
    private InternalResponse response;

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class InternalResponse {

        private Double paytmWalletBalance;
    }

}

