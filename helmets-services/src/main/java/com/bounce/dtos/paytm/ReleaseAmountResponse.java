package com.bounce.dtos.paytm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReleaseAmountResponse {

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("STATUSMESSAGE")
    private String message;
}
