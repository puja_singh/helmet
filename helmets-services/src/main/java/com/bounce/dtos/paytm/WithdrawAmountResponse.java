package com.bounce.dtos.paytm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.glassfish.jersey.server.JSONP;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class WithdrawAmountResponse {

    @JsonProperty("TxnId")
    private String paytmTransactionId;

    @JsonProperty("BankTxnId")
    private String bankTransactionId;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("ResponseCode")
    private String responseCode;

    @JsonProperty("ResponseMessage")
    private String responseMessage;
}
