package com.bounce.dtos;

import com.bounce.helper.GlobalConfigService;

import javax.inject.Inject;

public class PaytmDisplayDetails extends  PaymentDisplayDetails {
    private static final String TITLE = "Paytm";
    private static final String MAIN_STRING = "Rs.%s min balance is required";
    private static final String SUB_STRING_1 = null;
    private static final String SUB_STRING_2 = null;
    private static final String CTA_TEXT = "Link Account";
    private static final String HIGHLIGHTER = null;

    public PaytmDisplayDetails()
    {
        super(TITLE, MAIN_STRING, SUB_STRING_1, SUB_STRING_2, CTA_TEXT, HIGHLIGHTER);
    }

}
