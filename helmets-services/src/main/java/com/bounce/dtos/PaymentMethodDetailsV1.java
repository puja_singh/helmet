package com.bounce.dtos;

import com.bounce.dtos.enums.LinkedStatus;
import com.bounce.entity.Customer;
import com.bounce.entity.enums.PaymentMethod;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PaymentMethodDetailsV1 {

    @JsonProperty("link_status")
    private LinkedStatus linkedStatus;

    @JsonProperty("payment_method")
    private PaymentMethod paymentMethod;

    @JsonProperty("required_amount")
    private Double depositAmount;

    @JsonIgnore
    private Customer customer;
}
