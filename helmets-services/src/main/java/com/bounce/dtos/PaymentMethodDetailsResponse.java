package com.bounce.dtos;


import com.bounce.entity.enums.PaymentMethod;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
public class PaymentMethodDetailsResponse {

    @JsonProperty("default")
    private PaymentMethod defaultMethod;

    @JsonProperty("methods")
    private List<PaymentMethodDetailsV2> methods;

    @JsonProperty("reward_used_text")
    private String rewardUsedText;

    @JsonProperty("bounce_cash_text")
    private String bounceCashText;

    @JsonProperty("bounce_cash_sub_text")
    private String bounceCashSubText;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("strike_cost")
    private Double strike_cost;
}
