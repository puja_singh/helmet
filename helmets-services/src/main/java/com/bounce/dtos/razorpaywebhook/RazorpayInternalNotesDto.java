package com.bounce.dtos.razorpaywebhook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@JsonPOJOBuilder
public class RazorpayInternalNotesDto {

    @JsonProperty("contact")
    private String mobileNumber;

    @JsonProperty("due_amount")
    private Double dueAmount;

    @JsonProperty("dues_amount")
    private Double duesAmount;

    @JsonProperty("sec_deposit_amount")
    private Double securityDepositAmount;

    @JsonProperty("helmet_amount")
    private Double helmetAmount;

    @JsonProperty("wallet_topup_amount")
    private Double walletTopupAmount;
}