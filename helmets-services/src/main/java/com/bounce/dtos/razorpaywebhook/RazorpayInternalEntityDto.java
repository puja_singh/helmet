package com.bounce.dtos.razorpaywebhook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPOJOBuilder
public class RazorpayInternalEntityDto {

    @JsonProperty("status")
    private String status;

    @JsonProperty("notes")
    private RazorpayInternalNotesDto notes;

    @JsonProperty("id")
    private String paymentId;

    @JsonProperty("amount")
    private Double amount;

}