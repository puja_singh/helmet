package com.bounce.dtos.razorpaywebhook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPOJOBuilder
public class RazorpayInternalPaymentDto {

    @JsonProperty("entity")
    private RazorpayInternalEntityDto entity;
}