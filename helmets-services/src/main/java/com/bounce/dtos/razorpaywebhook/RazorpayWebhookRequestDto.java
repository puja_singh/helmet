package com.bounce.dtos.razorpaywebhook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPOJOBuilder
public class RazorpayWebhookRequestDto {

    @JsonProperty("event")
    private String event;

    @JsonProperty("created_at")
    private Long createdAt;

    @JsonProperty("payload")
    private RazorpayInternalPayloadDto payload;
}