package com.bounce.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@ToString
public class AnalyticsDumpDto {

    private String kingdom;

    private String phylum;

    private String family;

    private String source;

    private String className;

    private String order;

    private Double genusNum;

    private String species;

    private String tribe;

    private String valstring;

    private Double num1;

    private Double num2;

    private Double num3;

    private String str1;

    private String str2;

    private String str3;
}
