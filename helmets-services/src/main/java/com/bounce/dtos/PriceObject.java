package com.bounce.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PriceObject {

    private Double tripFare;

    private Double rewardPoints;

    private Double subTotal;

    private Double tax;

    private Double totalFare;

    private Double bounceWallet;

    private Double paytmDeductable;

    private Double mimBaseFare;

    private Double dues;
}
