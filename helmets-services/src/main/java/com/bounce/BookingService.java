package com.bounce;

import com.bounce.dtos.enums.VehicleType;
import com.bounce.entity.Booking;
import com.bounce.exceptions.CustomException;

import javax.transaction.Transactional;

public interface BookingService {

    void tripAfterCancellation(long bookingId);

    void releaseBlockedAmount(long bookingId) throws CustomException;

    void paytmConditionalLock(long bookingId, String userToken);
}
