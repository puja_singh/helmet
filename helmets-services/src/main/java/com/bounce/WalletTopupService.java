package com.bounce;

import com.bounce.entity.User;

import java.util.List;
import java.util.Map;

public interface WalletTopupService {



    void walletTopupHelper(User user, double amountInr, String paymentId, long offerId, String paymentMethod) throws Exception;

    double getWalletTopupValue(double amountInr, long offerId);

    List<Map<String, Object>> getActiveOffers(User user);
}
