package com.bounce;

public interface RazorpayService {

    String statusCheck(String paymentId);

    String capture(String paymentId, double amountInr, long userId);

    String issueFullRefund(String paymentId, double amountInr, long userId);


}
