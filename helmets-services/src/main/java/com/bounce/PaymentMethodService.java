package com.bounce;

import com.bounce.dtos.PaymentMethodDetailsResponse;
import com.bounce.dtos.PaymentMethodDetailsV1;
import com.bounce.entity.User;
import org.jooq.tools.json.JSONObject;

import java.util.List;

public interface PaymentMethodService {
    List<PaymentMethodDetailsV1> getPaymentMethodDetails(long customerId);

    PaymentMethodDetailsResponse RefreshPaymentDetails(User user, double estNetFare);
}
