package com.bounce.filters;

import com.bounce.entity.User;
import com.bounce.exceptions.CustomException;
import com.bounce.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.lang.reflect.Method;


@Aspect
@Component
@Slf4j
public class AuthFilterHelper {
    @Inject private UserRepository userRepository;
    private String fraudErrorMsg = "This account has been blocked due to a KYC issue. Please call Customer Care to understand the issue and get it unblocked.";

    @Before("@annotation(AuthFilter)")
    public User validateAspect(JoinPoint pjp) throws Throwable {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        String token = pjp.getArgs()[0].toString();
        Object getargs[] = pjp.getArgs();
        log.info("args is :{}",getargs);
        log.info("token :{}", token);
        try {
            if (StringUtils.isEmpty(token)) {
                throw new CustomException("No auth token provided", 401);
            }
            User user = this.userRepository.findFirstByTokenAndActive(token, true);
            log.info("user id: {}", user.getId());

            if (user == null) {
                throw new CustomException(fraudErrorMsg, 401);
            }
            return user;
        } catch (Exception e){
            throw new CustomException("User Authentication Failed", 401);
        }

    }
}
