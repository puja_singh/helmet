package com.bounce;


import com.bounce.entity.Customer;
import com.bounce.entity.RazorpayWebhooks;

public interface RazorpayAuthorizeFlowsService {

    void clearDues(Customer customer, String userToken, String paymentId, double amountInr, String mobileNumber,
                   RazorpayWebhooks razorpayWebhooks) throws Exception;

    void captureAndReleaseDubiousPayments(Customer customer, String paymentId, double amountInr, String callType,
                   String mobileNo, RazorpayWebhooks razorpayWebhooks) throws Exception;

    RazorpayWebhooks insertIntoRazorpayWebhooks(RazorpayWebhooks razorpayWebhooks);

}
