package com.bounce;

import com.bounce.dtos.request.PaidHelmetTrackingDto;
import com.bounce.entity.User;

import java.util.Map;

public interface PaidHelmetService {
    void HelmetPostpaidPayment(User user, double amountInr, String paymentId, String address, Integer pincode, String method) throws Exception;

    Map<String, Object> getPostpaidHelmetStatus(User user);

    void helmetPostpaidRefund(User user);

    Map<String, Object> getReturnDetails(User user);

    Map<String, Object> getTrackingDetails(User user);

    void setTrackingDetails(PaidHelmetTrackingDto paidHelmetTrackingDto);
}
