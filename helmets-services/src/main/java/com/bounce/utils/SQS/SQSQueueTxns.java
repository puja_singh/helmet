package com.bounce.utils.SQS;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.bounce.dtos.juspay.WebhookRequestDto;
import com.bounce.dtos.razorpaywebhook.RazorpayWebhookRequestDto;
import com.bounce.juspay.JuspayQueuePolling;
import com.bounce.utils.BillingUtil;
import com.bounce.utils.RealtimeData;
import com.bounce.utils.SQSCredentials;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.concurrent.TimeUnit;


@Slf4j
@Named
public class SQSQueueTxns {

    @Inject private JuspayQueuePolling juspayQueuePolling;

    private static final String MESSAGE_GROPUP_ID = "razorpay_webhooks";
    private static final String REGION = "ap-south-1";

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final int THREAD_COUNT= 20;

    private static AmazonSQS sqs = AmazonSQSClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(new SQSCredentials()))
            .withRegion(REGION).build();

    public static void SqsInsertion(JSONObject requestObject, String dedupId, String queueName) {
        final String queueUrl = sqs.getQueueUrl(queueName).getQueueUrl();
        final SendMessageRequest sendMessageRequest = new SendMessageRequest(
                queueUrl, requestObject.toString());
        sendMessageRequest.setMessageGroupId(MESSAGE_GROPUP_ID);
        sendMessageRequest.setMessageDeduplicationId(dedupId);
        SendMessageResult result = sqs.sendMessage(sendMessageRequest);
        log.info("SendMessage succeed with messageId {}, sequence number {}",
                result.getMessageId(), result.getSequenceNumber());
    }


    public void setupLongPolling(String queueName){

        final String queueUrl = sqs.getQueueUrl(queueName).getQueueUrl();
        final ReceiveMessageRequest smsReceiveRequest = new ReceiveMessageRequest()
                .withQueueUrl(queueUrl)
                .withWaitTimeSeconds(1);
        smsReceiveRequest.setMaxNumberOfMessages(10);
        for(int i =0; i < THREAD_COUNT; i ++) {
            RealtimeData.get().getScheduledExecutorService().schedule(() -> {
                while (true) {
                    queueMessageAsyncHelper(smsReceiveRequest, queueUrl);
                }
            }, 0, TimeUnit.SECONDS);
        }
    }

    private void queueMessageAsyncHelper(ReceiveMessageRequest smsReceiveRequest, String queueUrl) {
        try {
            final List<Message> messages = sqs.receiveMessage(smsReceiveRequest).getMessages();
            for (final Message message : messages) {
                log.info("Message {} {}", Thread.currentThread(), messages.size());
                log.info("Body {}", message.getBody());
                queueMessageAsyncBizHelper(message);
                String messageReceiptHandle = message.getReceiptHandle();
                sqs.deleteMessage(new DeleteMessageRequest(queueUrl, messageReceiptHandle));
            }
        } catch (Exception e) {
            log.error("error in queueMessageAsyncHelper : ",e);
        }
    }

    private void queueMessageAsyncBizHelper(Message message) {
        try {
            JSONObject jsonObject = new JSONObject(message.getBody());
            log.info("json[request] is {}",BillingUtil.getValue(jsonObject, "request"));
            //throws an error
            WebhookRequestDto webhookRequestDto = objectMapper.readValue(BillingUtil.getValue(jsonObject, "request"),
                    WebhookRequestDto.class);

            RealtimeData.get().getScheduledExecutorService().schedule(() -> {
                try {
                    juspayQueuePolling.processJuspayTransactions(webhookRequestDto);
                } catch (Exception e) {
                    log.error("error in juspayQueuePolling :", e);
                }
            }, 0, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("error in queueMessageAsyncBizHelper: ", e);
        }
    }


}
