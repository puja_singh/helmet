package com.bounce.utils.SQS;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.bounce.RazorpayAuthorizeFlowsService;
import com.bounce.UserDeviceService;
import com.bounce.dtos.razorpaywebhook.RazorpayInternalEntityDto;
import com.bounce.dtos.razorpaywebhook.RazorpayInternalNotesDto;
import com.bounce.dtos.razorpaywebhook.RazorpayWebhookRequestDto;
import com.bounce.entity.Customer;
import com.bounce.entity.RazorpayWebhooks;
import com.bounce.entity.User;
import com.bounce.razorpay.RazorpayApi;
import com.bounce.repository.CustomerRepository;
import com.bounce.repository.UserRepository;
import com.bounce.utils.BillingUtil;
import com.bounce.utils.SQSCredentials;
import com.bounce.utils.RealtimeData;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.concurrent.TimeUnit;


@Slf4j
@Named
public class SQSQueue {

    @Inject private RazorpayAuthorizeFlowsService razorpayAuthorizeFlowsService;
    @Inject private RazorpayApi razorpayApi;
    @Inject private UserRepository userRepository;
    @Inject private UserDeviceService userDeviceService;
    @Inject private CustomerRepository customerRepository;

    @Value("${razorpay.fifoqueue:razorpay_perf.fifo}")
    private String QUEUE_NAME;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final String MESSAGE_GROPUP_ID = "razorpay_webhooks";
    private static final String REGION = "ap-south-1";
    private final int THREAD_COUNT= 20;
    private final String AUTHORIZED = "authorized";
    private final String CAPTURED = "captured";

    private final String ANDROID = "android";

    private static AmazonSQS sqs = AmazonSQSClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(new SQSCredentials()))
            .withRegion(REGION).build();



    public void insertIntoQueue(RazorpayWebhookRequestDto requestDto) {

        RealtimeData.get().getScheduledExecutorService().schedule(() -> {
            try{
                insertIntoQueueHelper(requestDto);
            }catch (Exception e){
                log.error("Error while inserting into queue", e);
            }
        }, 0, TimeUnit.MILLISECONDS);
    }

    private void insertIntoQueueHelper(RazorpayWebhookRequestDto requestDto) throws Exception {

        JSONObject requestObject = new JSONObject();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.createObjectNode().pojoNode(requestDto);
        String jsonString = "";
        jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
        requestObject.put("request", jsonString);
        RazorpayInternalEntityDto entityDto = requestDto.getPayload().getPayment().getEntity();
        log.info("notes is : {}", entityDto.getNotes().toString());
        String dedupId = entityDto.getPaymentId();
        SQSQueueTxns.SqsInsertion(requestObject, dedupId, QUEUE_NAME);
    }



    public void setupLongPolling(){

        final String queueUrl = sqs.getQueueUrl(QUEUE_NAME).getQueueUrl();
        final ReceiveMessageRequest smsReceiveRequest = new ReceiveMessageRequest()
                .withQueueUrl(queueUrl)
                .withWaitTimeSeconds(1);
        smsReceiveRequest.setMaxNumberOfMessages(10);
        for(int i =0; i < THREAD_COUNT; i ++) {
            RealtimeData.get().getScheduledExecutorService().schedule(() -> {
                while (true) {
                    queueMessageAsyncHelper(smsReceiveRequest, queueUrl);
                }
            }, 0, TimeUnit.SECONDS);
        }
    }

    private void queueMessageAsyncHelper(ReceiveMessageRequest smsReceiveRequest, String queueUrl) {
        try {
            final List<Message> messages = sqs.receiveMessage(smsReceiveRequest).getMessages();
            for (final Message message : messages) {
                log.info("Message {} {}", Thread.currentThread(), messages.size());
                log.info("Body {}", message.getBody());
                queueMessageAsyncBizHelper(message);
                String messageReceiptHandle = message.getReceiptHandle();
                sqs.deleteMessage(new DeleteMessageRequest(queueUrl, messageReceiptHandle));
            }
        } catch (Exception e) {
            log.error("error in queueMessageAsyncHelper : ",e);
        }
    }

    private void queueMessageAsyncBizHelper(Message message) {
        try {
            JSONObject jsonObject = new JSONObject(message.getBody());
            log.info("json[request] is {}",BillingUtil.getValue(jsonObject, "request"));
            //throws an error
            RazorpayWebhookRequestDto requestDto = objectMapper.readValue(BillingUtil.getValue(jsonObject, "request"),
                    RazorpayWebhookRequestDto.class);
            log.info("request object event is: {} and amount is: {}", requestDto.getEvent(),
                    requestDto.getPayload().getPayment().getEntity().getAmount());
            RealtimeData.get().getScheduledExecutorService().schedule(() -> {
                try {
                    processCallbackFlow(requestDto);
                } catch (Exception e) {
                    log.error("error in processing the transaction for payment id: {}, {}", requestDto.getPayload().getPayment().getEntity().getPaymentId(), e);
                }
            }, 0, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("error in queueMessageAsyncBizHelper: ", e);
        }
    }

    private String processCallbackFlow(RazorpayWebhookRequestDto requestDto) throws Exception {
        RazorpayInternalEntityDto entity = requestDto.getPayload().getPayment().getEntity();
        String paymentId = entity.getPaymentId();
        log.info("razorpay webhook request processing for paymentId: {}", paymentId);

        String currentStatus = razorpayApi.statusCheck(paymentId);
        if(AUTHORIZED.equals(currentStatus)) {
            double amountInr = BillingUtil.paisaToInr(entity.getAmount());
            log.info("amount in inr is: {}", amountInr);

            User user = getUser(entity);
            Long userId = user.getId();
            String userToken = user.getToken();
            String mobileNumber = user.getMobileNumber();
            Customer customer = this.customerRepository
                    .findById(userId).orElseThrow(() -> new IllegalArgumentException("Invalid customer object passed"));
            String userDevicePlatform = this.userDeviceService.getUserDevicePlatform(customer.getId());
            String userAppVersion = this.userDeviceService.getUserDeviceAppVersion(customer.getId());
            Flow flow = figureOutFlow(entity.getNotes(), customer, amountInr, userDevicePlatform);
            RazorpayWebhooks razorpayWebhooks = RazorpayWebhooks.builder()
                    .amount(amountInr)
                    .appVersion(userAppVersion)
                    .callType(flow.toString())
                    .mobileApp(userDevicePlatform)
                    .paymentId(paymentId)
                    .mobileNumber(mobileNumber)
                    .userId(userId)
                    .captureStatus(AUTHORIZED)
                    .build();
            razorpayWebhooks = razorpayAuthorizeFlowsService.insertIntoRazorpayWebhooks(razorpayWebhooks);
            if(Flow.CLEAR_DUES.equals(flow)) {
                this.razorpayAuthorizeFlowsService.clearDues(customer, userToken, paymentId, amountInr, mobileNumber, razorpayWebhooks);
            } else if (Flow.GENERIC.equals(flow)){
                this.razorpayAuthorizeFlowsService.captureAndReleaseDubiousPayments(customer, paymentId, amountInr, String.valueOf(flow),
                        mobileNumber, razorpayWebhooks);
            } else {
                throw new RuntimeException(String.format("Please code more carefully!!!. Not supported: %s", flow));
            }
        }
        return "Sucess";
    }

    private User getUser(RazorpayInternalEntityDto entity) throws Exception {
        String mobileNumber = entity.getNotes().getMobileNumber();
        User user = this.userRepository
                .findFirstByMobileNumberAndActiveOrderByIdAsc(sanitizeMobileNumber(mobileNumber), true);
        if(StringUtils.isEmpty(user)) {
            throw new Exception("User not found in db");
        }
        log.info("token is: {} and userid: {}", user.getToken(), user.getId());
        return user;
    }

    private String sanitizeMobileNumber(String number) {
        int length = number.length();
        if(length <= 10) {
            return number;
        } else {
            int beginIndex = length - 10;
            return number.substring(beginIndex, length);
        }
    }

    private Flow figureOutFlow(RazorpayInternalNotesDto notesDto, Customer customer, double amountInr, String userDevicePlatform) {
        Flow flow = null;
        if(StringUtils.isEmpty(userDevicePlatform) || !ANDROID.equals(userDevicePlatform)) {
            //NOTE: for ios handle as generic flow
            flow = Flow.GENERIC;
        } else if (!StringUtils.isEmpty(notesDto.getDueAmount())
                || !StringUtils.isEmpty(notesDto.getDuesAmount())) {
            boolean isClearDuesEligible = checkClearDuesEligibility(customer, amountInr);
            if(isClearDuesEligible) {
                flow = Flow.CLEAR_DUES;
            } else {
                flow = Flow.GENERIC;
            }
        } else {
            flow = Flow.GENERIC;
        }
        return flow;
    }

    private boolean checkClearDuesEligibility(Customer customer, double amountInr) {
        double dues = customer.getTotalAmountDue();
        log.info("total dues for the customer {}", dues);
        if(dues >= amountInr) {
            return true;
        } else {
            return false;
        }
    }

    public enum Flow {
        CLEAR_DUES, PAID_HELMET, POSTPAID, WALLET_TOPUP, GENERIC
    }
}
