package com.bounce.utils;

import com.bounce.dtos.AnalyticsDumpDto;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class TrackingDumpUtil {

    public static void dumpIntoAnimalKingdom(AnalyticsDumpDto analyticsDumpDto) {

        Date ts = new Date();
        log.info("dumping into animal kingdom :{}", analyticsDumpDto.toString());
        AnalyticsUtils.getInstance().log(analyticsDumpDto.getKingdom(), analyticsDumpDto.getPhylum(), analyticsDumpDto.getSource(),
                ts.getTime(), analyticsDumpDto.getClassName(), analyticsDumpDto.getOrder(), analyticsDumpDto.getFamily(),
                analyticsDumpDto.getGenusNum(), analyticsDumpDto.getSpecies(), analyticsDumpDto.getTribe(),
                analyticsDumpDto.getValstring(), analyticsDumpDto.getNum1(), analyticsDumpDto.getNum2(), analyticsDumpDto.getNum3(),
                analyticsDumpDto.getStr1(), analyticsDumpDto.getStr2(), analyticsDumpDto.getStr3());
        log.info("inserted into animal kingdom");
    }
}