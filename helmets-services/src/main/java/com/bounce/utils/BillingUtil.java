package com.bounce.utils;

import com.bounce.helper.GlobalConfigService;
import org.json.JSONObject;

import java.math.BigDecimal;

public class BillingUtil {

    public static double roundDistance(double distance) {
        if(distance == 0) {
            return 0;
        }
        return Math.ceil(distance/ 100.0) /10.0;
    }

    public static long roundTime(double time) {
        if(time == 0) {
            return 0;
        }
        return (long) Math.ceil(time / 60.0);
    }

    public static boolean isBillingAmnestyApplicable(double distance, double oldCost) {
        GlobalConfigService globalConfigHelper = new GlobalConfigService();
        if(globalConfigHelper.isBillingAmnestyOn() &&
           distance <= globalConfigHelper.getBillingAmnestyDistance() &&
           oldCost <= globalConfigHelper.getBillingAmnestyAmount())   {
            return true;
        } else {
            return false;
        }
    }

    public static double add(double x, double y) {
        return BigDecimal.valueOf(x).add(BigDecimal.valueOf(y)).doubleValue();
    }

    public static double subtract(double x, double y) {
        return BigDecimal.valueOf(x).subtract(BigDecimal.valueOf(y)).doubleValue();
    }

    public static double multiply(double x, double y) {
        return BigDecimal.valueOf(x).multiply(BigDecimal.valueOf(y)).doubleValue();
    }

    public static double divide(double x, double y) {
        return BigDecimal.valueOf(x).divide(BigDecimal.valueOf(y)).doubleValue();
    }

    public static double max(double x, double y) {
        return BigDecimal.valueOf(x).max(BigDecimal.valueOf(y)).doubleValue();
    }

    public static double min(double x, double y) {
        return BigDecimal.valueOf(x).min(BigDecimal.valueOf(y)).doubleValue();
    }

    public static double compareTo(double x , double y) {
        return BigDecimal.valueOf(x).compareTo(BigDecimal.valueOf(y));
    }

    public static double paisaToInr(double x) {
        return divide(x, 100);
    }

    public static String getValue(JSONObject jsonObject, String key) {
        boolean hasKey = jsonObject.has(key);
        String value = null;
        if (hasKey) {
            value = jsonObject.get(key).toString();
        }
        return value;
    }
}
