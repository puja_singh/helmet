package com.bounce.utils;

import com.bounce.dtos.AnalyticsDumpDto;
import com.bounce.entity.*;
import com.bounce.entity.enums.PostpaidStatus;
import com.bounce.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.Date;

@Slf4j
public class LoggingUtil {

    @Transactional
    public static void logDuesLedger(DuesLedgerRepository duesLedgerRepository, long userId, double clearAmount, double totalAmountDue, String paymentMethod, Long paymentRefId){
        DuesLedger duesLedger = DuesLedger.builder()
                .userId(userId)
                .amount(clearAmount)
                .mode("Settle")
                .source("Clear Dues Flow")
                .paymentMethod(paymentMethod)
                .paymentRefId(paymentRefId)
                .totalDueAmount(totalAmountDue)
                .build();
        duesLedgerRepository.save(duesLedger);
    }

    @Transactional
    public static PaytmTransaction inserIntoPaytmTransaction(PaytmTransactionRepository paytmTransactionRepository, long bookingId, Customer customer, String type, String callType, String orderId,
                                               String paymentId, String method, double transAmount, boolean result, String apiResponse){
        PaytmTransaction paytmTransaction = PaytmTransaction.builder()
                .bookingId(bookingId)
                .customer(customer)
                .type(type)
                .callType(callType)
                .orderId(orderId)
                .paytmTransactionId(paymentId)
                .method(method)
                .transAmount(transAmount)
                .result(result)
                .build();
        paytmTransactionRepository.save(paytmTransaction);
        apiResonseIntoAnimalKingdom(customer, orderId, paymentId, apiResponse);
        return paytmTransaction;
    }

    private static void apiResonseIntoAnimalKingdom(Customer customer, String orderId, String paymentId, String apiResponse) {
        try{
            if(!StringUtils.isEmpty(apiResponse)) {
                log.info("api response: {}", apiResponse);
                AnalyticsDumpDto analyticsDumpDto = AnalyticsDumpDto.builder()
                        .kingdom("tuborg")
                        .phylum("payments")
                        .source("server")
                        .className(orderId)
                        .species(paymentId)
                        .genusNum(Double.valueOf(customer.getId()))
                        .tribe(apiResponse)
                        .build();
                TrackingDumpUtil.dumpIntoAnimalKingdom(analyticsDumpDto);
            }
        } catch (Exception e) {

            log.info("error in inserting in logging util", e);
        }
    }

    @Transactional
    public static WalletTopup insertIntoWalletTopup(WalletTopupRepository walletTopupRepository, Long userId, double amountInr, Long offerId, String paymentId, double walletTopupValue){

        WalletTopup walletTopup = WalletTopup.builder()
                .userId(userId)
                .amountInr(amountInr)
                .offerId(offerId)
                .paymentId(paymentId)
                .walletValue(walletTopupValue)
                .build();
        walletTopupRepository.save(walletTopup);
        return walletTopup;
    }

    @Transactional
    public static void updateWalletTopup(WalletTopupRepository walletTopupRepository, WalletTopup walletTopup, String phpResponse){

        walletTopup.setPhpResponse(phpResponse);
        walletTopupRepository.save(walletTopup);

    }


    @Transactional
    public static PaidHelmet insertIntoHelmet(PaidHelmetRepository paidHelmetRepository, long userId, double amountInr, Integer pincode, String address, PostpaidStatus postpaidStatus){

        PaidHelmet paidHelmet = PaidHelmet.builder()
                .userId(userId)
                .amount(amountInr)
                .address(address)
                .pincode(pincode)
                .postpaidStatus(postpaidStatus)
                .paidDate(new Date())
                .build();
        paidHelmetRepository.save(paidHelmet);
        return paidHelmet;
    }

    @Transactional
    public static WalletTopupTransaction insertIntoWalletTopupTransaction(WalletTopupTransactionRepository walletTopupTransactionRepository,
                                                                          double amount, String paymentId, long userId, String walletTxnId, boolean result){


        WalletTopupTransaction walletTopupTransaction = WalletTopupTransaction.builder()
                .amount(amount)
                .paymentId(paymentId)
                .userId(userId)
                .walletTxnId(walletTxnId)
                .result(result)
                .build();
        walletTopupTransactionRepository.save(walletTopupTransaction);
        return walletTopupTransaction;
    }

    @Transactional
    public static PaymentTransactionLog insertIntoPaymentTransactionLog(PaymentTrasactionLogRepository paymentTrasactionLogRepository,
                                                                        long userId, long bookingId, double amount,
                                                                        String paymentId, String paymentMethod, String callType, String type){
        PaymentTransactionLog paymentTransactionLog = PaymentTransactionLog.builder()
                .userId(userId)
                .bookingId(bookingId)
                .amount(amount)
                .paymentMethod(paymentMethod)
                .trackingId(paymentId)
                .callType(callType)
                .type(type)
                .build();
        paymentTransactionLog = paymentTrasactionLogRepository.save(paymentTransactionLog);
        return paymentTransactionLog;
    }

    @Transactional
    public static PostpaidRefunds insertIntoPostpaidRefunds(PostpaidRefundsRepository postpaidRefundsRepository, long userId, String paymentMethod, String paymentId, String apiResponse){

        PostpaidRefunds postpaidRefunds = PostpaidRefunds.builder()
                .userId(userId)
                .paymentId(paymentId)
                .paymentMethod(paymentMethod)
                .apiResponse(apiResponse)
                .build();
        postpaidRefunds = postpaidRefundsRepository.save(postpaidRefunds);
        return postpaidRefunds;
    }

}
