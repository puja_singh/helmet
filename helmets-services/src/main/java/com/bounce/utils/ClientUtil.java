package com.bounce.utils;

import org.glassfish.jersey.client.ClientProperties;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.concurrent.TimeUnit;

public class ClientUtil {
    private static Integer connectTimeout= (int) TimeUnit.SECONDS.toMillis(1); //MilliSeconds
    private static Integer readTimeout = (int) TimeUnit.SECONDS.toMillis(4); //MilliSeconds
    private static Integer readTimeoutUX = (int) TimeUnit.SECONDS.toMillis(1); //MilliSeconds

    public static Client getClient() {
        Client client = ClientBuilder.newClient();
        client.property(ClientProperties.CONNECT_TIMEOUT, connectTimeout);
        client.property(ClientProperties.READ_TIMEOUT, readTimeout);
        return client;
    }

    public static Client getClientUX() {
        Client client = ClientBuilder.newClient();
        client.property(ClientProperties.CONNECT_TIMEOUT, connectTimeout);
        client.property(ClientProperties.READ_TIMEOUT, readTimeoutUX);
        return client;
    }

    public static Client getClientBasedOnContext() {
        Client client = null;
        if (Thread.currentThread().getName() == "UX") {
            client = ClientUtil.getClientUX();
            Thread.currentThread().setName("default");
        } else {
            client = ClientUtil.getClient();
        }
        return client;
    }
}
