package com.bounce;

public interface EstimationService {

    double getEstimatedBaseFare(long bookingId, double distance, double time);
}
